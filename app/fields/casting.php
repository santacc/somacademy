<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$casting = new FieldsBuilder('campos_casting');

$casting
    ->setLocation('post_type', '==', 'casting');

$casting
->addImage('imagenCastingInterior', [
    'label' => 'Imagen para el casting Interior',
    ])
    ->addImage('imagenCasting', [
        'label' => 'Imagen para el casting',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addTextarea('descCasting', [
        'label' => 'Descripción del Casting',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addTextarea('descCorta', [
        'label' => 'Descripcion corta para grid',
    ])
    ->addTrueFalse('urlExterna', [
        'label' => 'Tiene la URL externa',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ])
    ->addUrl('urlCasting', [
        'label' => 'URL casting en web externa',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' =>'urlExterna',
            'operator' =>  '==',
            'value' =>'1'
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
    ])
    ->addText('textBotonExterno', [
        'label' => 'Texto para el boton enlace externo',
        'instructions' => '',
        'conditional_logic' => [
            'field' =>'urlExterna',
            'operator' =>  '==',
            'value' =>'1'
        ],
    ])
    ->addTrueFalse('urlDescarga', [
        'label' => 'Archivo para la inscripcion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ])
    ->addFile('archivoInscripcion', [
        'label' => 'Subir archivo para inscripcion',
        'instructions' => 'El tamaño del archivo tiene que ser inferior a 8MB',
        'required' => 0,
        'conditional_logic' => [
            'field' =>'urlDescarga',
            'operator' =>  '==',
            'value' =>'1'
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'library' => 'all',
        'min_size' => '',
        'max_size' => '8M',
        'mime_types' => '',
    ])
    ->addText('textBotonDescarga', [
        'label' => 'Texto para el boton enlace externo',
        'instructions' => '',
        'conditional_logic' => [
            'field' =>'urlDescarga',
            'operator' =>  '==',
            'value' =>'1'
        ],
    ])

    ->addTrueFalse('urlAutorizacion', [
        'label' => 'Archivo para la autorizacion',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ])
    ->addFile('archivoAutorizacion', [
        'label' => 'Subir archivo para autorizacion',
        'instructions' => 'El tamaño del archivo tiene que ser inferior a 2MB',
        'required' => 0,
        'conditional_logic' => [
            'field' =>'urlAutorizacion',
            'operator' =>  '==',
            'value' =>'1'
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'library' => 'all',
        'min_size' => '',
        'max_size' => '2Mb',
        'mime_types' => '',
    ])
    ->addText('textBotonAutorizacion', [
        'label' => 'Texto para el boton enlace externo',
        'instructions' => '',
        'conditional_logic' => [
            'field' =>'urlAutorizacion',
            'operator' =>  '==',
            'value' =>'1'
        ],
    ])

    ->addTrueFalse('paginaInterior', [
        'label' => 'El casting tiene su propia pagina interior en SOM produce',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ])
    ->addText('textBotonInterior', [
        'label' => 'Texto para el boton enlace interno',
        'instructions' => '',
        'conditional_logic' => [
            'field' =>'paginaInterior',
            'operator' =>  '==',
            'value' =>'1'
        ],
    ])
    ->addText('codigoFormulario', [
        'label' => 'Id del formulario',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
    ])
    ->addTextarea('styleAudicion', [
        'label' => 'CSS para formato de la audicion',
    
    ])

    ->addTextarea('codigoEstilo', [
        'label' => 'CSS para formulario la audicion',
    
    ])
;


return $casting;


