<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$alumno = new FieldsBuilder('campos_alumno');

$alumno
    ->setLocation('post_type', '==', 'alumno');

$alumno
    ->addText('nombre', [
        'label' => 'Nombre del alumno',
    ])
    ->addTextarea('comentarioAlumno', [
        'label' => 'Comentario del alumno',
    ]);;


return $alumno;
