<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

acf_add_options_page([
    'page_title' => get_bloginfo('name') . 'options practicas',
    'menu_title' => 'Configuracion Practicas',
    'menu_slug'  => 'theme-optionspracticas',
    'capability' => 'edit_theme_options',
    'position'   => '9999',
    'autoload'   => true,
    'icon_url'      => 'dashicons-edit-large'
]);

$optionspracticas = new FieldsBuilder('theme_optionpracticas');

$optionspracticas
    ->setLocation('options_page', '==', 'theme-optionspracticas');

$optionspracticas
->addTab('Accesos Usuarios', ['placement' => 'top'])
    ->addText('titSeccionAlumno', [
        'label' => 'Titulo de la seccion de alumnos',
    ])
    ->addFile('manualPracticas', [
        'label' => 'Manual de parcticas',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'library' => 'all',
        'min_size' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addText('txtManualPracticas', [
        'label' => 'Texto para el manual de practicas',
    ])
    ->addPageLink('urlAlumno', [
        'label' => 'URL para la pagina acceso del alumnos',
        'type' => 'page_link',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'post_type' => ['page'],
        'taxonomy' => [],
        'allow_null' => 0,
        'allow_archives' => 1,
        'multiple' => 0,
    ])
    ->addText('sortCodeVerEstudiante', [
        'label' => 'Shortcode para ver el perfil del estudiante',
        'wrapper' => [
            'width' => '50%',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addText('sortCodeEditarEstudiante', [
        'label' => 'Shortcode para editar el perfil del estudiante',
        'wrapper' => [
            'width' => '50%',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addText('sortCodePracticasEstudiante', [
        'label' => 'Shortcode para editar las practicas del estudiante',
        'wrapper' => [
            'width' => '50%',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addText('urlEmpresa', [
    'label' => 'URL para la pagina de la emmpresa',
    ])
->addTab('Accesos Empresas', ['placement' => 'top'])
    ->addText('titSeccionEmpresas', [
        'label' => 'Titulo de la seccion de empresa',
    ])
    ->addRepeater('proyectosFormativos', [
        'label' => 'Proyectos formativos por cursos',
        'button_label' => '',
    ])
        ->addFile('proyectoFormativo', [
            'label' => 'Proyecto formativo',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'library' => 'all',
            'min_size' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addText('txtProyectoFormativo', [
            'label' => 'Texto para el manual de practicas',
        ])
        ->addSelect('cursoProyecto', [
            'label' => 'Selecciona el curso al que pertenece el proyecto',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'choices' => [
                'tercero' => 'tercero',
                'cuarto' => 'cuarto',
            ],
            'default_value' => [],
            'allow_null' => 0,
            'multiple' => 0,
            'ui' => 0,
            'ajax' => 0,
            'return_format' => 'value',
            'placeholder' => '',
        ])
    ->endRepeater()
   
    ->addFile('procedimientoPaso', [
        'label' => 'Procedimiento paso a paso',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'library' => 'all',
        'min_size' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addText('txtProcedimientoPaso', [
        'label' => 'Texto para el procedimiento paso a paso',
    ])
    ->addPageLink('urlFormularioEmpresa', [
        'label' => 'URL para la pagina de formularios de la empresa',
        'type' => 'page_link',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'post_type' => ['page'],
        'taxonomy' => [],
        'allow_null' => 0,
        'allow_archives' => 1,
        'multiple' => 0,
    ])
    ->addText('sortCodeVerEmpresa', [
        'label' => 'Shortcode para ver el perfil de la empresa',
        'wrapper' => [
            'width' => '35%',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addText('sortCodeEmpresaEditar', [
        'label' => 'Shortcode para el formulario editar empresa',
        'wrapper' => [
            'width' => '35%',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addText('formularioSatisfaccion', [
        'label' => 'ID form Satisfacción',
        'wrapper' => [
            'width' => '15%',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addText('formularioPractica', [
        'label' => 'ID form practicas',
        'wrapper' => [
            'width' => '15%',
            'class' => '',
            'id' => '',
        ],
    ])
->addTab('Seguros y Seguridad Social', ['placement' => 'top'])
    ->addText('tituloSeguros', [
        'label' => 'Titulo seccion seguros ',
    ])
    ->addText('urlInfoPracticas', [
        'label' => 'URL Información practicas',
    ])
    ->addText('textoInfoPracticas', [
        'label' => 'Texto informacion Practicas',
    ])
    ->addText('tituloBases', [
        'label' => 'Titulo para las bases de cotización',
    ])
    ->addText('tituloPracticas', [
        'label' => 'Titulo para las practicas',
    ])
    ->addText('urlPracticas', [
        'label' => 'URL para la información de las practicas',
    ])
  
    ->addText('textoRealDecreto', [
        'label' => 'Texto para el real decreto',
    ])
    ->addText('urlRealDecreto', [
        'label' => 'URL del Real Decreto',
    ])
->addTab('Impresos y documentos', ['placement' => 'top'])
    ->addText('titSeccionImpreso', [
        'label' => 'Titulo Seccion Impresos y Documentos',
    ])
    ->addRepeater('listadoImpresos', [
        'label' => 'Listado de Impresos',
        'button_label' => 'Agregar Impresos',
    ])
        ->addFile('archivoImpreso', [
            'label' => 'Archivo del Impreso',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'library' => 'all',
            'min_size' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addText('textoImpreso', [
            'label' => 'Texto para el impreso',
        ])
    ->endRepeater()
->addTab('Cuestionario Satisfacción', ['placement' => 'top'])
->addText('titFormularioSatisfaccion', [
    'label' => 'Titulo para el formulario de satisfaccion',
])
    ->addText('idFormulario', [
        'label' => 'Id del formulario de custionario de satisfacción',
    ])

;



return $optionspracticas;

