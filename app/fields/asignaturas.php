<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$streaming_page = get_page_by_path('practicas', 'ARRAY_N');

$practicas = new FieldsBuilder('Campos Asignaturas');

$practicas
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-asignaturas.blade.php');

$practicas
    ->addTab('Campos Genericos', ['placement' => 'top'])
        ->addText('titTablaOficial', [
            'label' => 'Titulo para la tabla oficial',
        ])
        ->addText('titTablaPropia', [
            'label' => 'Titulo para la tabla propia',
        ])
        ->addText('encabezadoAsignaturas', [
            'label' => 'Encabezado para las asignaturas',
        ])
        ->addText('encabezadoCreditos', [
            'label' => 'Encabezado para los creditos',
        ])
    ->addTab('Primer Curso', ['placement' => 'top'])
        ->addText('nomCursoPrimero', [
            'label' => 'Nombre del curso',
        ])
        ->addRepeater('asignaturasPrimeroOficial', [
            'label' => 'Asignaturas para primero',
        ])
            ->addText('asignaturaPrimeroOficial', [
                'label' => 'Asignatura para primero oficial',
            ])
            ->addNumber('creditosPrimeroOficial', [
                'label' => 'Creditos Asignatura Primero Oficial',
                'min' => '1',
                'max' => '',
                'step' => '1',
            ])
            ->addFile('guiaDocente', [
                'label' => 'Guía Docente',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'return_format' => 'array',
                'library' => 'all',
                'min_size' => '',
                'max_size' => '',
                'mime_types' => '',
            ])
        ->endRepeater()
        ->addRepeater('asignaturasPrimeroPropio', [
            'label' => 'Asignaturas para primero Propio',
        ])
            ->addText('asignaturaPrimeroPropio', [
                'label' => 'Asignatura para primero Propio',
            ])
            ->addNumber('creditosPrimeroPropio', [
                'label' => 'Creditos Asignatura Primero Propio',
                'min' => '1',
                'max' => '',
                'step' => '1',
            ])
            ->addFile('guiaDocentePropia', [
        'label' => 'Guía Docente',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'library' => 'all',
        'min_size' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
        ->endRepeater()
        ->addText('creditosPrimero', [
            'label' => 'Creditos totales para Primero',
        ])
    ->addTab('Segundo Curso', ['placement' => 'top'])
        ->addText('nomCursoSegundo', [
            'label' => 'Nombre del curso',
        ])
        ->addRepeater('asignaturasSegundoOficial', [
            'label' => 'Asignaturas para segundo',
        ])
            ->addText('asignaturaSegundoOficial', [
                'label' => 'Asignatura para segundo oficial',
            ])
            ->addNumber('creditosSegundoOficial', [
            'label' => 'Creditos Asignatura Segundo Oficial',
            'min' => '1',
            'max' => '',
            'step' => '1',
            ])
            ->addFile('guiaDocenteSegundo', [
                'label' => 'Guía Docente',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'return_format' => 'array',
                'library' => 'all',
                'min_size' => '',
                'max_size' => '',
                'mime_types' => '',
            ])
        ->endRepeater()
        ->addRepeater('asignaturasSegundoPropio', [
            'label' => 'Asignaturas para segundo Propio',
        ])
            ->addText('asignaturaSegundoPropio', [
                'label' => 'Asignatura para segundo Propio',
            ])
            ->addNumber('creditosSegundoPropio', [
            'label' => 'Creditos Asignatura Segundo Propio',
            'min' => '1',
            'max' => '',
            'step' => '1',
        ])
        ->addFile('guiaDocenteSegundoPropio', [
            'label' => 'Guía Docente',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'library' => 'all',
            'min_size' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->endRepeater()
        ->addText('creditosSegundo', [
            'label' => 'Creditos totales para Segundo',
        ])
    ->addTab('Tercer Curso', ['placement' => 'top'])
        ->addText('nomCursoTercero', [
            'label' => 'Nombre del curso',
        ])
        ->addRepeater('asignaturasTerceroOficial', [
            'label' => 'Asignaturas para tercero',
        ])
            ->addText('asignaturaTerceroOficial', [
                'label' => 'Asignatura para tercero oficial',
            ])
            ->addNumber('creditosTerceroOficial', [
            'label' => 'Creditos Asignatura Tercero Oficial',
            'min' => '1',
            'max' => '',
            'step' => '1',
            ])
            ->addFile('guiaDocenteTercero', [
                'label' => 'Guía Docente',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'return_format' => 'array',
                'library' => 'all',
                'min_size' => '',
                'max_size' => '',
                'mime_types' => '',
            ])
        ->endRepeater()
        ->addRepeater('asignaturasTerceroPropio', [
            'label' => 'Asignaturas para Tercero Propio',
        ])
            ->addText('asignaturaTerceroPropio', [
                'label' => 'Asignatura para tercero Propio',
            ])
            ->addNumber('creditosTerceroPropio', [
            'label' => 'Creditos Asignatura Tercero Propio',
            'min' => '1',
            'max' => '',
            'step' => '1',
        ])
        ->addFile('guiaDocenteTerceroPropio', [
            'label' => 'Guía Docente',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'library' => 'all',
            'min_size' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->endRepeater()
        ->addText('creditosTercero', [
            'label' => 'Creditos totales para Tercero',
        ])
    ->addTab('Cuarto Curso', ['placement' => 'top'])
            ->addText('nomCursoCuarto', [
                'label' => 'Nombre del curso',
            ])
        ->addRepeater('asignaturasCuartoOficial', [
            'label' => 'Asignaturas para Cuarto',
        ])
            ->addText('asignaturaCuartoOficial', [
                'label' => 'Asignatura para Cuarto oficial',
            ])
            ->addNumber('creditosCuartoOficial', [
            'label' => 'Creditos Asignatura Cuarto Oficial',
            'min' => '1',
            'max' => '',
            'step' => '1',
        ])
        ->endRepeater()
        ->addRepeater('asignaturasCuartoPropio', [
            'label' => 'Asignaturas para Cuarto Propio',
        ])
            ->addText('asignaturaCuartoPropio', [
                'label' => 'Asignatura para Cuarto Propio',
            ])
            ->addNumber('creditosCuartoPropio', [
            'label' => 'Creditos Asignatura Cuarto Propio',
            'min' => '1',
            'max' => '',
            'step' => '1',
        ])
        ->endRepeater()
        ->addText('creditosCuarto', [
            'label' => 'Creditos totales para Cuarto',
        ])
;

return $practicas;

