<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$streaming_page = get_page_by_path('practicas', 'ARRAY_N');

$practicas = new FieldsBuilder('Campos Admision');

$practicas
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-admision.blade.php');

$practicas
    ->addTab('Fase 1', ['placement' => 'top'])
        ->addText('titFaseUno', [
            'label' => 'Titulo para Fase Uno',
        ])
        ->addText('textFaseUnoP', [
            'label' => 'Primer texto para Fase Uno',
        ])
        ->addText('textFaseUnoS', [
            'label' => 'Segundo texto para Fase Uno',
        ])
    ->addTab('Fase 2', ['placement' => 'top'])
        ->addText('titFaseDos', [
            'label' => 'Titulo para Fase Dos',
        ])
        ->addText('textFaseDosP', [
            'label' => 'Primer texto para Fase Dos',
        ])
        ->addText('textFaseDosS', [
            'label' => 'Segundo texto para Fase Dos',
        ])
    ->addTab('Fase 3', ['placement' => 'top'])
        ->addText('titFaseTres', [
            'label' => 'Titulo para Fase Tres',
        ])
        ->addText('textFaseTresP', [
            'label' => 'Primer texto para Fase Tres',
        ])
    ->addTab('Fase 4', ['placement' => 'top'])
        ->addText('titFaseCuatro', [
            'label' => 'Titulo para Fase Cuatro',
        ])
        ->addText('textFaseCuatroP', [
            'label' => 'Primer texto para Fase Cuatro',
        ])
;

return $practicas;
