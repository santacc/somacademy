<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$empresa = new FieldsBuilder('campos_empresa');

$empresa
    ->setLocation('post_type', '==', 'empresa');

$empresa
    ->addText('nombreEmpresa', [
        'label' => 'Nombre de la empresa',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])
    ->addText('actividadEmpresa', [
        'label' => 'Actividad de la empresa',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])
    ->addText('cifEmpresa', [
        'label' => 'CIF de la empresa',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])
    ->addText('sectorEmpresa', [
        'label' => 'Sector de la empresa',
        'wrapper' => [
            'width' => '100%',
            'class' => '',
            'id' => '',
            ],
    ])
    ->addText('personaFirma', [
        'label' => 'Persona que firma el convenio',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])
    ->addText('correoPersonaFirma', [
        'label' => 'Correo Persona que firma el convenio',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])
    ->addText('cargoPersonaFirma', [
        'label' => 'Cargo de la persona que firma el convenio',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])
    ->addText('direccionEmpresa', [
        'label' => 'Direccion de la empresa',
        'wrapper' => [
            'width' => '100%',
            'class' => '',
            'id' => '',
            ],
    ])
    ->addText('cpEmpresa', [
        'label' => 'Código postal de la empresa',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])
    ->addText('telefonoEmpresa', [
        'label' => 'Teléfono de la empresa',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])

    ->addText('localidadEmpresa', [
        'label' => 'Localidad de la empresa',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])

    ->addText('urlEmpresa', [
        'label' => 'Url de la web de la empresa',
        'wrapper' => [
            'width' => '33%',
            'class' => '',
            'id' => '',
            ],
    ])

    
    ;


return $empresa;