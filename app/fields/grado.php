<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$streaming_page = get_page_by_path('grado', 'ARRAY_N');

$grado = new FieldsBuilder('grado');

$grado
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-grado.blade.php');

$grado
    ->addTab('Horarios', ['placement' => 'top'])
    ->addText('titHorarios', [
        'label' => 'Titulo para la seccion de horarios',
    ])
        ->addRepeater('horario', [
            'label' => 'Creacion de los horarios',
            'layout' => 'row',
            'button_label' => 'Agregar un nuevo horario',
            'sub_fields' => [],
        ])
            ->addText('nombreHorario', [
                'label' => 'Agregar nombre al horario',
            ])
            ->addText('cicloHorario', [
                'label' => 'Agregar Ciclo para el que se crea el horario',
            ])
            ->addText('turnoHorario', [
                'label' => 'Turno del horario al horario',
            ])
            ->addText('horasHorario', [
                'label' => 'Asignar las horas del horario',
            ])
                ->addRepeater('diasLectivos', [
                    'label' => 'Creacion de los dias lectivos para de el horario',
                    'layout' => 'row',
                    'button_label' => 'Agregar un nuevo día',
                    'sub_fields' => [],
                    'min' => 1,
                    'max' => 7,
                ])
                    ->addText('nombreDia', [
                        'label' => 'Agregar nombre del dia para el horario',
                    ])
                        ->addRepeater('horasClases', [
                            'label' => 'Creacion de las horas lectivas para cada día del horario',
                            'layout' => 'table',
                            'button_label' => 'Agregar una nueva hora',
                            'sub_fields' => [],
                            'min' => 1,
                        ])
                            ->addText('horaClase', [
                                'label' => 'Agregar hora de la clase',
                                'placeholder' => '00:00 - 23:59',
                            ])
                            ->addText('nombreClase', [
                                'label' => 'Agregar nombre de la clase',
                                'placeholder' => 'Nombre de la clase',
                            ])
                        ->endRepeater()
                ->endRepeater()
        ->endRepeater()
    ->addTab('Formulario', ['placement' => 'top'])
        ->addText('tituloFormulario', [
            'label' => 'Titulo para la seccion del formulario',

        ])
        ->addText('titFormulario', [
        'label' => 'Titulo del formulario',
        ])
         ->addWysiwyg('textoBajoFormulario', [
            'label' => 'Texto que va debajo del formulario',
        ])
    ->addTab('Dossier', ['placement' => 'top'])
        ->addText('tituloDossier', [
            'label' => 'Titulo para la seccion del Dossier',
        ])
        ->addText('codeDossier', [
        'label' => 'ShortCode del Dossier',

    ])
    ->addFile('archivoDossier', [
        'label' => 'Subir archivo pdf',
    ])
    ->addTab('Equipo Docente', ['placement' => 'top'])
            ->addTrueFalse('activarEquipoDocente', [
                'label' => 'Si / No equipo docente',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'message' => '',
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ])
            ->addGroup('destacadoDocente', [
                'label' => 'Espacio para el docente principal',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'layout' => 'block',
                'sub_fields' => [],
            ])
                ->addImage('fotoDoecenteDestacado', [
                    'label' => 'Imagen supervisión',
                    'instructions' => '',
                ])
                ->addText('nombreProfeDestacado', [
                    'label' => 'Nombre del profesor a destacar',
                    'instructions' => '',
                ])
                ->addText('puestoProfeDestacado', [
                    'label' => 'Puesto del profesor a destacar',
                    'instructions' => '',
                ])
                ->addTextarea('descProfeDestacado', [
                    'label' => 'Descripción para profesor a destacar',
                ])
            ->endGroup()
    ->addRepeater('disciplinas', [
        'label' => 'Agregar Disciplinas',
        'layout' => 'row',
    ])
    ->addText('titDisciplina', [
        'label' => 'Titulo para la disciplina',
    ])
    ->addText('etiquetaJefeEstudios', [
        'label' => 'Etiqueta para la sección jefe de estudios',
    ])
        ->addRelationship('jefeEstudiosDisciplina', [
            'label' => 'Selecciona el profesor que sera Jefe de estudios',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['profesor'],
            'taxonomy' => [],
            'filters' => [
                0 => 'search',
                1 => 'post_type',
            ],
            'elements' => '',
            'min' => '',
            'max' => '2',
            'return_format' => 'object',
        ])

    ->addText('etiquetaProfesores', [
        'label' => 'Etiqueta para el resto de profesores',
    ])


        ->addRelationship('profesorGrado', [
            'label' => 'Selecciona el profesor que impartira el curso',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['profesor'],
            'taxonomy' => [],
            'filters' => [
                0 => 'search',
                1 => 'post_type',
            ],
            'elements' => '',
            'min' => '',
            'max' => '',
            'return_format' => 'object',
        ])
    ->endRepeater()
    ;

return $grado;

