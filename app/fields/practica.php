<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$practica = new FieldsBuilder('campos_practica');

$practica
    ->setLocation('post_type', '==', 'practica');

$practica

->addUser('idEmpresa', [
  'label' => 'ID de la empresa',
  'instructions' => '',
  'required' => 0,
  'conditional_logic' => [],
  'wrapper' => [
      'width' => '',
      'class' => '',
      'id' => '',
  ],
  'role' => 'empresa',
  'allow_null' => 0,
  'multiple' => 0,
])

->addUser('idEstudiante', [
  'label' => 'Asignar estudiantes',
  'instructions' => '',
  'required' => 0,
  'conditional_logic' => [],
  'wrapper' => [
      'width' => '',
      'class' => '',
      'id' => '',
  ],
  'role' => 'estudiante',
  'allow_null' => 0,
  'multiple' => 1,
])
->addTrueFalse('asignado', [
  'label' => 'Asignado si / no',
  'instructions' => '',
  'required' => 0,
  'conditional_logic' => [],
  'wrapper' => [
      'width' => '50%',
      'class' => '',
      'id' => '',
  ],
  'message' => '',
  'default_value' => 0,
  'ui' => 1,
  'ui_on_text' => '',
  'ui_off_text' => '',
])
->addText('nombrePractica', [
  'label' => 'Nombre de la practica',
  'wrapper' => [
      'width' => '100%',
      'class' => '',
      'id' => '',
  ],
])

->addText('nombreFormularioEmpresa', [
  'label' => 'Nombre de la empresa en el formulario',
  'wrapper' => [
      'width' => '50%',
      'class' => '',
      'id' => '',
  ],
])
    ->addNumber('numeroVacantes', [
      'label' => 'Número de vacantes',
      'wrapper' => [
          'width' => '50%',
          'class' => '',
          'id' => '',
      ],
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'min' => '',
      'max' => '',
      'step' => '',
    ])
    ->addTextarea('descripcionPractica', [
        'label' => 'Descripción de la practica',
        'wrapper' => [
            'width' => '100%',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addDateTimePicker('inicio', [
      'label' => 'Fecha de inicio',
      'wrapper' => [
          'width' => '33%',
          'class' => '',
          'id' => '',
      ],
      'display_format' => 'd/m/Y',
      'return_format' => 'd/m/Y',
    ])
    ->addDateTimePicker('final', [
      'label' => 'Fecha de finalización',
      'wrapper' => [
          'width' => '33%',
          'class' => '',
          'id' => '',
      ],
      'display_format' => 'd/m/Y',
      'return_format' => 'd/m/y',
    ])
    ->addTextarea('horario', [
      'label' => 'Horario de las practicas',
      'instructions' => 'Introducir si el horario es de mañana o de tarde',
      'wrapper' => [
          'width' => '33%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addText('numeroHorasJornada', [
      'label' => 'Número de horas jornada',
      'wrapper' => [
          'width' => '25%',
          'class' => '',
          'id' => '',
      ],
      
    ])
    ->addText('numeroDiasSemana', [
      'label' => 'Número dias a la semama',
      'wrapper' => [
          'width' => '25%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addText('duracionMeses', [
      'label' => 'Duracion en meses',
      'wrapper' => [
          'width' => '25%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addText('numeroTotalHoras', [
      'label' => 'Numero total horas',
      'wrapper' => [
          'width' => '25%',
          'class' => '',
          'id' => '',
      ],
      
    ])
    ->addText('direccionEmpresa', [
      'label' => 'Direccion de la empresa en la que se realiza la practica',
      'wrapper' => [
          'width' => '75%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addText('localidad', [
      'label' => 'Localidad de la empresa',
      'wrapper' => [
          'width' => '25%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addTextarea('detallePractica', [
      'label' => 'Funciones a realizar',
      'wrapper' => [
          'width' => '100%',
          'class' => '',
          'id' => '',
      ],
  ])
  
  ->addTab('Tutor empresa/entidad', ['placement' => 'top'])
    ->addText('nombreTutor', [
      'label' => 'Nombre del tutor',
      'wrapper' => [
          'width' => '33%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addText('primerApellidoTutor', [
      'label' => 'Primer apellido del tutor',
      'wrapper' => [
          'width' => '33%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addText('segundoApellidoTutor', [
      'label' => 'Segundo apellido del tutor',
      'wrapper' => [
          'width' => '33%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addText('cargoTutor', [
      'label' => 'Cargo del tutor en la empresa',
      'wrapper' => [
          'width' => '33%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addText('telefonoEmpresa', [
      'label' => 'Telefono de la empresa',
      'wrapper' => [
          'width' => '33%',
          'class' => '',
          'id' => '',
      ],
    ])
    ->addEmail('emailEmpresa', [
      'label' => 'Email de la empresa',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => [],
      'wrapper' => [
          'width' => '33%',
          'class' => '',
          'id' => '',
      ],
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
  ])
  ->addTab('Perfil de los estudiantes', ['placement' => 'top'])
  ->addTextarea('detallePerfil', [
    'label' => 'Detalle del perfil requerido',
    'wrapper' => [
        'width' => '100%',
        'class' => '',
        'id' => '',
    ],
])
    ;


return $practica;
