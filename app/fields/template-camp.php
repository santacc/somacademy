<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$streaming_page = get_page_by_path('practicas', 'ARRAY_N');

$practicas = new FieldsBuilder('practicas');

$practicas
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-camp.blade.php');

$practicas
    ->addTab('Texto Encabezado', ['placement' => 'top'])
    ->addTextarea('txtEncabezado', [
        'label' => 'Titulo para la seccion de horarios',
    ])
;

return $practicas;

