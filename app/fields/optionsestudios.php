<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

acf_add_options_page([
    'page_title' => get_bloginfo('name') . ' options home estudios',
    'menu_title' => 'Opciones Home Estudios',
    'menu_slug'  => 'theme-optionsestudios',
    'capability' => 'edit_theme_options',
    'position'   => '999',
    'autoload'   => true,
    'icon_url'      => 'dashicons-edit-large'
]);

$optionsestudios = new FieldsBuilder('theme_optionsestudios');

$optionsestudios
    ->setLocation('options_page', '==', 'theme-optionsestudios');

$optionsestudios
    ->addTab('Titulo de la página', ['placement' => 'left'])
        ->addText('titHomeEstudios', [
            'label' => 'Titulo para la página de estudios',
        ])
    ->addTab('Header Configuracion', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.sliderHomeEstudios'))
    ->addTab('Cajas texto superiores', ['placement' => 'left'])
        ->addRepeater('textoSuperiores', [
            'label' => 'Cajas de texto superiores',
            ])
            ->addText('tiTextoSuperiores', [
                'label' => 'Titulo para la los texto superiores',
            ])
            ->addTextarea('conTextoSuperiores', [
                'label' => 'Texto para los cuadros superiores',
            ])
        ->endRepeater()
    ->addTab('Grid Imagenes', ['placement' => 'left'])
        ->addRepeater('contenidoGrid', [
            'label' => 'Contenidos para grid',
            'max' => 5,
        ])
            ->addImage('imgenGrid', [
                    'label' => 'Imagen del grid',
                ])
            ->addText('tiGridImagenes', [
            'label' => 'Titulos para los Grid',
        ])
        ->endRepeater()
    ->addTab('Razones', ['placement' => 'left'])
        ->addRepeater('contenidoRazones', [
            'label' => 'Contenidos para listado Razones',
            'max' => 10,
        ])
            ->addText('numeroRazon', [
                'label' => 'Numero de la Razón',
            ])
            ->addTextarea('textRazon', [
                'label' => 'Texto de la razón',
            ])
        ->endRepeater()

    ->addTab('Profesores', ['placement' => 'left'])
        ->addRelationship('profesorListado', [
                'label' => 'Profesor para listado',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'post_type' => ['profesor'],
                'taxonomy' => [],
                'filters' => [
                ],
                'elements' => '',
                'min' => '',
                'max' => '',
                'return_format' => 'object',
            ])
    ->addTab('Alumnos', ['placement' => 'left'])
    ->addRelationship('alumnosListado', [
        'label' => 'Alumnos para listado',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'post_type' => ['alumno'],
        'taxonomy' => [],
        'filters' => [
        ],
        'elements' => '',
        'min' => '',
        'max' => '',
        'return_format' => 'object',
    ])
    ->addTab('Formulario', ['placement' => 'left'])
        ->addText('tituloSeccionForm', [
            'label' => 'Titulo de la seccion de formulario',
        ])
        ->addText('formCodigoFormulario', [
            'label' => 'Numero Id del formulario',
        ])
        ->addTextarea('textoBajoFormularioOfical', [
            'label' => 'Descripción del formulario',
        ])
        ->addTab('Horarios', ['placement' => 'top'])
    ->addTrueFalse('activarHorarios', [
        'label' => 'Si / No horarios',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ])
    ->addText('titHorarios', [
        'label' => 'Titulo para la seccion de horarios',
    ])
        ->addRepeater('horario', [
            'label' => 'Creacion de los horarios',
            'layout' => 'row',
            'button_label' => 'Agregar un nuevo horario',
            'sub_fields' => [],
        ])
            ->addText('nombreHorario', [
                'label' => 'Agregar nombre al horario',
            ])
            ->addText('turnoHorario', [
                'label' => 'Turno del horario al horario',
            ])
                ->addRepeater('diasLectivos', [
                    'label' => 'Creacion de los dias lectivos para de el horario',
                    'layout' => 'row',
                    'button_label' => 'Agregar un nuevo día',
                    'sub_fields' => [],
                    'min' => 1,
                    'max' => 7,
                ])
                    ->addText('nombreDia', [
                        'label' => 'Agregar nombre del dia para el horario',
                    ])
                        ->addRepeater('horasClases', [
                            'label' => 'Creacion de las horas lectivas para cada día del horario',
                            'layout' => 'table',
                            'button_label' => 'Agregar una nueva hora',
                            'sub_fields' => [],
                            'min' => 1,
                        ])
                            ->addText('horaClase', [
                                'label' => 'Agregar hora de la clase',
                                'placeholder' => '00:00 - 23:59',
                            ])
                            ->addText('nombreClase', [
                                'label' => 'Agregar nombre de la clase',
                                'placeholder' => 'Nombre de la clase',
                            ])
                        ->endRepeater()
                ->endRepeater()
        ->endRepeater()
        
        
;



return $optionsestudios;

