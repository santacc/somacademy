<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$contactoHome = new FieldsBuilder('contactoHome');

$contactoHome
    ->addTrueFalse('activeContacto', [
        'label' => 'Activar o desactivar la secion de Contacto',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => 'Activada la seccion de Contacto',
        'default_value' => 1,
        'ui' => 1,
        'ui_on_text' => 'Si',
        'ui_off_text' => 'No',
    ])
    ->addText('tituloContacto', [
        'label' => 'Titulo seccion contacto home',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])

    ->addText('formContHome', [
        'label' => 'Formulario para la seccion contactod e la home',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])



;

return $contactoHome;

