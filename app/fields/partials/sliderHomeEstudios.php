<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$sliderHomeEstudios = new FieldsBuilder('sliderHomeEstudios');

$sliderHomeEstudios

    ->addTrueFalse('verSliderHomeEstudios', [
        'label' => 'Activar el slider Home estudios',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => 'SI',
        'ui_off_text' => 'NO',
    ])
    ->addRepeater('sliderHeaderHomeEstudios', [
        'label' => 'Componentes para el slider de la home',
        'button_label' => 'Nuevo Slider',
        'conditional_logic' => [
            'field' => 'verSliderHomeEstudios',
            'operator' => '==',
            'value' => '1',
        ],

    ])
        ->addImage('imageSlideHomeEstudios', [
            'label' => 'Imagen para el slider',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addText('textoSliderHomeEstudios', [
            'label' => 'Texto para el encabezado del slider',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addUrl('linkSlideHomeEstudios', [
        'label' => 'Pagina o post al que apunta el slider',
        ])
    ->endRepeater()
    ->addNumber('velocidadTranscionHomeEstudios', [
        'label' => 'Configuarra la velocidad de la transicion del slider',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' => 'verSliderHomeEstudios',
            'operator' => '==',
            'value' => '1',
        ],

        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'min' => '500',
        'max' => '10000',
        'step' => '500',
    ])

;

return $sliderHomeEstudios;

