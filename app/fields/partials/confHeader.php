<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$confHeader = new FieldsBuilder('confHeader');

$confHeader

        ->addTrueFalse('verSlider', [
            'label' => 'Activar el slider',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'message' => '',
            'default_value' => 0,
            'ui' => 1,
            'ui_on_text' => 'SI',
            'ui_off_text' => 'NO',
        ])
    ->addText('shortcodeSlider', [
        'label' => 'Introducir el shortcode del slider del header',
        'instructions' => '',
        'conditional_logic' => [
            'field' =>'verSlider',
            'operator' => '==',
            'value' => '1'
        ],
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addRepeater('sliderHeader', [
        'label' => 'Componentes para el slider de la home',
        'button_label' => 'Nuevo Slider',

    ])
        ->addImage('imageSlide', [
            'label' => 'Imagen para el slider',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ])
        ->addText('textoSlider', [
            'label' => 'Texto para el encabezado del slider',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addPageLink('linkSlide', [
        'label' => 'Pagina o post al que apunta el slider',
        'type' => 'page_link',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'post_type' => [],
        'taxonomy' => [],
        'allow_null' => 0,
        'allow_archives' => 1,
        'multiple' => 0,
    ])
    ->endRepeater()
    ->addNumber('velocidadTranscion', [
        'label' => 'Configuarra la velocidad de la transicion del slider',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'min' => '500',
        'max' => '10000',
        'step' => '500',
    ])

    ;

return $confHeader;
