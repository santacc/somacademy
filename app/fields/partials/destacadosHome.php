<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$destacadosHome = new FieldsBuilder('destacadosHome');

$destacadosHome
    ->addTrueFalse('activeDestacados', [
        'label' => 'Activar o desactivar la secion de Destacados',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => 'Activada la seccion de Destacados',
        'default_value' => 1,
        'ui' => 1,
        'ui_on_text' => 'Si',
        'ui_off_text' => 'No',
    ])

    ->addRepeater('imgDestacadosHome', [
        'label' => 'Componentes para el slider de la home',
        'button_label' => 'Nuevo Slider',

    ])
        ->addImage('imageDestacado', [
            'label' => 'Imagen para el slider',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '800',
            'min_height' => '900',
            'min_size' => '',
            'max_width' => '1600',
            'max_height' => '1800',
            'max_size' => '300kb',
            'mime_types' => 'jpg',
        ])
        ->addText('titDestacado', [
            'label' => 'Titulo para el destacado',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ])
        ->addText('textoDestacado', [
            'label' => 'Texto para el encabezado del slider',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '100',
        ])
        ->addPageLink('linkDestacado', [
            'label' => 'Link para el destacdo',
            'type' => 'page_link',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'post_type' => ['taller','page'],
            'taxonomy' => [],
            'allow_null' => 1,
            'allow_archives' => 0,
            'multiple' => 0,
        ])
    ->endRepeater()

;

return $destacadosHome;

