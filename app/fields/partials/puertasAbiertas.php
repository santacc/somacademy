<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$puertasAbiertas = new FieldsBuilder('$puertasAbiertas');

$puertasAbiertas
    ->addTrueFalse('activePuertasAbiertas', [
        'label' => 'Activar o desactivar la secion de puertas abiertas',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => 'Activada la seccion de puertas abiertas',
        'default_value' => 1,
        'ui' => 1,
        'ui_on_text' => 'Si',
        'ui_off_text' => 'No',
    ])
    ->addText('tituloSeccion', [
        'label' => 'Titulo para la seccion de puertas abiertas',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addTextarea('introSeccion', [
        'label' => 'Introcuccion par la secciond e puertas abiertas',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addTextarea('avisoSeccion', [
        'label' => 'Avisos para la parte superior del formulario',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addText('formSeccion', [
        'label' => 'Formulario para la seccion de puertas abiertas',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])

;

return $puertasAbiertas;

