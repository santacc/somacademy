<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$bandaMusicales = new FieldsBuilder('bandaMusicales');

$bandaMusicales
    ->addTrueFalse('activeBanda', [
        'label' => 'Activar o desactivar la secion banda musicales',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => 'Activada la banda de los musicales',
        'default_value' => 1,
        'ui' => 1,
        'ui_on_text' => 'Si',
        'ui_off_text' => 'No',
    ])
    ->addTextarea('textoSuperior', [
        'label' => 'Texto para la parte superior de la banda de musicales',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])
    ->addImage('fondoSeccion', [
        'label' => 'Fondo para la seccion de Banda musicales',
    ])
    ->addRepeater('logosMusicales', [
        'label' => 'Logos para los musicales de la banda superior',
        'button_label' => 'Agregar un nuevo musical o espectaculo',
    ])
    ->addImage('imgLogos', [
        'label' => 'Image FIeld',
    ])
    ->addSelect('tipoCol', [
        'label' => 'Tipo de  columna',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'choices' => [
            'simple' => 'Simple',
            'doble' => 'Doble',
        ],
        'default_value' => [],
        'allow_null' => 0,
        'multiple' => 0,
        'ui' => 0,
        'ajax' => 0,
        'return_format' => 'value',
        'placeholder' => '',
    ])
    ->endRepeater()
    ->addTextarea('textoInferior', [
        'label' => 'Texto para la parte superior de la banda de musicales',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
    ])

;

return $bandaMusicales;
