<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$escuelaSegura = new FieldsBuilder('escuelaSegura');

$escuelaSegura
    ->addTrueFalse('activeMedidas', [
        'label' => 'Activar o desactivar la seccion de Escuela Segura',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => 'Activada la seccion de Escuela Segura',
        'default_value' => 1,
        'ui' => 1,
        'ui_on_text' => 'Si',
        'ui_off_text' => 'No',
    ])
    ->addImage('fondoEscuela', [
        'label' => 'Fondo lateral escuela segura',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addTextarea('textoescuela', [
        'label' => 'Texto escuela segura',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'maxlength' => '',
        'rows' => '',
        'new_lines' => '', // Possible values are 'wpautop', 'br', or ''.
    ])
    ->addImage('logoEscuelaSegura', [
        'label' => 'Logo escuela segura',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])




;

return $escuelaSegura;

