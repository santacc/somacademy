<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$top_bar = new FieldsBuilder('top_bar');

$top_bar
    ->addTrueFalse('top_bar', [
        'label' => 'Si o NO',
        'instructions' => 'Activar para que esta página aparezca en el grid de la home....',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => 'Si',
        'ui_off_text' => 'No',
    ])
    ->addText('telefonoHeader', [
        'label' => 'Introducir e telefono para la top bar',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' =>'top_bar',
            'operator' => '==',
            'value' => '1'
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
    ])
    ->addText('emailHeader', [
        'label' => 'Introducir la dirección email para la top bar',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' =>'top_bar',
            'operator' => '==',
            'value' => '1'
        ],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
    ])
    ->addRepeater('redesSociales', [
        'label' => 'Redes Sociales',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [
            'field' =>'top_bar',
            'operator' => '==',
            'value' => '1'
        ],
    ])
    ->addImage('iconoRedes', [
        'label' => 'Seleccionar el icono para las redes sociales',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'return_format' => 'array',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
    ])
    ->addUrl('urlRedes', [
        'label' => 'URL para las redes sociales',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
    ])

    ->endRepeater()
    ->addUrl('enlaceAcceso', [
        'label' => 'Enlace para el acceso a la gestión de los alumnos',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => 'https://www.url_acceso_alumnos.com',
    ])
    ->addText('tstBotonAcceso', [
        'label' => 'Texto para el boton de acceso a los usuarios',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
    ])
;


return $top_bar;


