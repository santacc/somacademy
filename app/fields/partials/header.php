<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$header = new FieldsBuilder('header');

$header
    ->addTab('Banner header', ['placement' => 'left'])
    ->addTrueFalse('activeBanner', [
        'label' => 'Activar o desactivar el banner de la home situado en el la parte superior',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => 'Activado el banner',
        'default_value' => 1,
        'ui' => 1,
        'ui_on_text' => 'Si',
        'ui_off_text' => 'No',
    ])
    ->addText('enlaceBanner', [
        'label' => 'Enlace para el banner',
    ])
    ->addText('textoUno', [
        'label' => 'Texto Uno',
    ])
    ->addText('textoDos', [
        'label' => 'Texto Dos ',
    ])
    ->addText('textoTres', [
        'label' => 'Texto Tres',
    ])
    ->addText('textoCuatro', [
        'label' => 'Text Cuatro',
    ])

    ->addText('textoSubCuatro', [
        'label' => 'Text debajo del Cuatro',
    ])
    ->addText('textoCinco', [
        'label' => 'Text Cinco',
    ])
    ->addText('textoSeis', [
        'label' => 'Text Seis',
    ])
    ->addText('textoSiete', [
        'label' => 'Text Siete',
    ])
    ->addTab('header', ['placement' => 'left'])
    ->addText('intro', ['label' => 'Introduction'])
        ->setInstructions('Introducir texto para cambiar el titulo.');


return $header;
