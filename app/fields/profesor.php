<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$profesor = new FieldsBuilder('campos_profesor');

$profesor
    ->setLocation('post_type', '==', 'profesor');

$profesor
    ->addText('cargo', [
        'label' => 'Cargo o funcion en la academia',
    ])
    ->addSelect('tipoFormacion', [
        'label' => 'Selecciona el tipo de formacion que imparte',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'choices' => [
            'formacionProfesional' => 'Formacion Profesional',
            'noProfesional' => 'Formación no profesional',
            'jefesEstudio' => 'Jefes de estudio',
        ],
        'default_value' => [],
        'allow_null' => 1,
        'multiple' => 1,
        'ui' => 0,
        'ajax' => 0,
        'return_format' => 'value',
        'placeholder' => '',
    ])
    ->addText('disciplina', [
        'label' => 'Disciplina o disciplinas que imparte el profesor ',
    ])
    ->addRelationship('cursos', [
        'label' => 'Cursos qeu imparte o participa este profesor',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'post_type' => ['taller'],
        'taxonomy' => [],
        'filters' => [
            0 => 'search',
            1 => 'post_type',
            2 => 'taxonomy',
        ],
        'elements' => '',
        'min' => '',
        'max' => '',
        'return_format' => 'object',
    ])
    ->addWysiwyg('estudios', [
        'label' => 'Estudios o formación del profesor',
        'tabs' => 'visual',
    ])
    ->addText('titSeccionCarta', [
        'label' => 'Text Field',
    ])
    ->addWysiwyg('cartaProfesor', [
        'label' => 'Descripcion de la asignatura de los profesores',
        'tabs' => 'visual',
    ])
    ->addWysiwyg('experiencia', [
        'label' => 'Experiencia o trayectoria del profesor',
        'tabs' => 'visual',
    ])
    ->addImage('imageProfesor', [
        'label' => 'Foto del profesor que imparte el taller',
    ])
    ->addWysiwyg('descripcion', [
        'label' => 'Texto del profesor sobre el y el curso',
        'tabs' => 'visual',
    ]);

return $profesor;
