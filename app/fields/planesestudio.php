<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$streaming_page = get_page_by_path('practicas', 'ARRAY_N');

$practicas = new FieldsBuilder('Campos Plan estudios');

$practicas
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-planestudios.blade.php');

$practicas
    ->addTab('Texto Intro', ['placement' => 'top'])
        ->addText('titPagina', [
            'label' => 'Titulo para la pagina',
        ])
        ->addWysiwyg('txtPrimerParrafo', [
            'label' => 'Texto para el primer parrafo',
        ])

        ->addImage('imagenEntreParrafos', [
            'label' => 'Imagen entre los parrafos',
        ])

        ->addWysiwyg('txtSegundoParrafo', [
            'label' => 'Texto para el segundo parrafo',
        ])
    ->addTab('Tabla Esquema', ['placement' => 'top'])
        ->addText('titEsquema', [
            'label' => 'Titulo para la el esquema o tabla',
        ])
    ->addText('subtitEsquema', [
            'label' => 'SubTitulo para la el esquema o tabla',
        ])
    ->addTab('Movimiento', ['placement' => 'top'])
        ->addText('titMovimiento', [
            'label' => 'Titulo para la el Movimiento',
        ])
        ->addWysiwyg('txtMovimiento', [
            'label' => 'Texto para Movimiento',
        ])
        ->addText('txtMovimientoUno', [
            'label' => 'Primer cuadrado',
        ])
        ->addText('txtMovimientoDos', [
            'label' => 'Segundo cuadrado',
        ])
        ->addText('txtMovimientoTres', [
            'label' => 'Tercer cuadrado',
        ])
        ->addText('txtMovimientoCuatro', [
            'label' => 'Cuarto cuadrado',
        ])
        ->addText('txtMovimientoCinco', [
            'label' => 'Quinto cuadrado',
        ])
        ->addText('txtMovimientoSeis', [
            'label' => 'Sexto cuadrado',
        ])
        ->addText('txtMovimientoSiete', [
            'label' => 'Septimo cuadrado',
        ])
        ->addText('txtMovimientoOcho', [
            'label' => 'Octavo cuadrado',
        ])


    ->addTab('Interpretación', ['placement' => 'top'])
        ->addText('titInterpretacion', [
            'label' => 'Titulo para la el Interpretación',
        ])
        ->addWysiwyg('txtInterpretacion', [
            'label' => 'Texto para Interpretación',
        ])
        ->addText('txtInterpretacionUno', [
            'label' => 'Primer cuadrado',
        ])
        ->addText('txtInterpretacionDos', [
            'label' => 'Segundo cuadrado',
        ])
        ->addText('txtInterpretacionTres', [
            'label' => 'Tercer cuadrado',
        ])
        ->addText('txtInterpretacionCuatro', [
            'label' => 'Cuarto cuadrado',
        ])
        ->addText('txtInterpretacionCinco', [
            'label' => 'Quinto cuadrado',
        ])
        ->addText('txtInterpretacionSeis', [
            'label' => 'Sexto cuadrado',
        ])
        ->addText('txtInterpretacionSiete', [
            'label' => 'Septimo cuadrado',
        ])
        ->addText('txtInterpretacionOcho', [
            'label' => 'Octavo cuadrado',
        ])
        ->addText('txtInterpretacionNueve', [
            'label' => 'Noveno cuadrado',
        ])

    ->addTab('Música, Canto y Voz', ['placement' => 'top'])
        ->addText('titMusica', [
            'label' => 'Titulo para la Música, Canto y Voz',
        ])
        ->addWysiwyg('txtMusica', [
            'label' => 'Texto para Música, Canto y Voz',
        ])
        ->addText('txtMusicaUno', [
            'label' => 'Primer cuadrado',
        ])
        ->addText('txtMusicaDos', [
            'label' => 'Segundo cuadrado',
        ])
        ->addText('txtMusicaTres', [
            'label' => 'Tercer cuadrado',
        ])
        ->addText('txtMusicaCuatro', [
            'label' => 'Cuarto cuadrado',
        ])
        ->addText('txtMusicaCinco', [
            'label' => 'Quinto cuadrado',
        ])
        ->addText('txtMusicaSeis', [
            'label' => 'Sexto cuadrado',
        ])
        ->addText('txtMusicaSiete', [
            'label' => 'Septimo cuadrado',
        ])
        ->addText('txtMusicaOcho', [
            'label' => 'Octavo cuadrado',
        ])
        ->addText('txtMusicaNueve', [
            'label' => 'Noveno cuadrado',
        ])

    ->addTab('Formación Básica', ['placement' => 'top'])
        ->addText('titBasica', [
            'label' => 'Titulo para la Formación Básica',
        ])
        ->addWysiwyg('txtBasica', [
            'label' => 'Texto para Formación Básica',
        ])
        ->addText('txtBasicaUno', [
            'label' => 'Primer cuadrado',
        ])
        ->addText('txtBasicaDos', [
            'label' => 'Segundo cuadrado',
        ])
        ->addText('txtBasicaTres', [
            'label' => 'Tercer cuadrado',
        ])
        ->addText('txtBasicaCuatro', [
            'label' => 'Cuarto cuadrado',
        ])
        ->addText('txtBasicaCinco', [
            'label' => 'Quinto cuadrado',
        ])
        ->addText('txtBasicaSeis', [
            'label' => 'Sexto cuadrado',
        ])
    ->addTab('Otras Formaciones', ['placement' => 'top'])
        ->addText('otrasFormacionesUno', [
            'label' => 'Texto otras formaciones Uno',
        ])
        ->addText('otrasFormacionesDos', [
            'label' => 'Texto otras formaciones Dos',
        ])

;

return $practicas;
