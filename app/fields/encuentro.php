<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$encuentro = new FieldsBuilder('campos_encuentro');

$encuentro
    ->setLocation('post_type', '==', 'encuentro');

$encuentro
->addWysiwyg('descripcionEncuentro', [
  'label' => 'Descripcion del encuentro',
  'instructions' => '',
  'required' => 0,
  'conditional_logic' => [],
  'wrapper' => [
      'width' => '',
      'class' => '',
      'id' => '',
  ],
  'default_value' => '',
  'tabs' => 'all',
  'toolbar' => 'full',
  'media_upload' => 1,
  'delay' => 0,
])

;


return $encuentro;
