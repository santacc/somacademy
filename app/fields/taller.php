<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$taller = new FieldsBuilder('campos_taller');

$taller
    ->setLocation('post_type', '==', 'taller');

$taller
    ->addTab('Datos Generales del Taller', ['placement' => 'left'])
    ->addDatePicker('fecha_comienzo', [
        'label' => 'Fecha para ordenar el evento',
        'instructions' => '',
        'required' => 0,
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'display_format' => 'd/m/Y',
        'return_format' => 'y-m-d',
    ])
        ->addSelect('tipoAlumnos', [
            'label' => 'Para quien es el taller',
            'instructions' => 'Selecciona a quien va dirigido el taller',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'choices' => [
                'seleccionar' => 'Selecciona tipo alumnos',
                'junior' => 'Junior',
                'profesionales' => 'Profesionales',
                'adultos' => 'Adultos'
            ],
            'default_value' => [],
            'allow_null' => 0,
            'multiple' => 0,
            'ui' => 1,
            'ajax' => 0,
            'return_format' => 'value',
            'placeholder' => '',
        ])
    ->addText('disciplinas', [
        'label' => 'Disciplinas del curso',
    ])
    ->addText('nivelesTaller', [
        'label' => 'Nivel para el taller ',
    ])
    ->addText('fechaGridTaller', [
        'label' => 'Fecha del taller ',
    ])
    ->addRelationship('profesoresTaller', [
        'label' => 'Relationship Field',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'post_type' => ['profesor'],
        'taxonomy' => [],
        'filters' => [
            0 => 'search',
            1 => 'post_type',
            2 => 'taxonomy',
        ],
        'elements' => '',
        'min' => '',
        'max' => '',
        'return_format' => 'object',
    ])
    ->addImage('imageTaller', [
        'label' => 'Foto para el taller que imparte el taller',
    ])
     ->addWysiwyg('descripcionTaller', [
        'label' => 'Descripcion del taller',
         'tabs' => 'visual',
    ])
    ->addTextarea('lugarTaller', [
        'label' => 'Lugar donde se imparte el taller',
    ])
    ->addTextarea('duracion', [
        'label' => 'Duración y horario del taller',
    ])
    ->addTextarea('perfil', [
        'label' => 'Perfil profesional requerido para acceso taller',
    ])
    ->addTextarea('oyentes', [
        'label' => 'Admite oyentes o no',
    ])
     ->addTextarea('precio', [
        'label' => 'Precio para alumno / oyente ',
    ])
    ->addTextarea('Otros', [
        'label' => 'Acalaraciones necesarias',
    ])

    ->addRepeater('botonesComprar', [
        'label' => 'Botones para poder comprar acceder a las diferentes ventas',
        'instructions' => '',
    ])
    ->addText('txtBtnComprar', [
        'label' => 'Texto para los difenetes botones de comprar',
        'button_label' => 'Agregar nuevo boton de venta',
    ])
        ->addText('urlBtnComparar', [
        'label' => 'URL para los difenetes botones de comprar',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
    ])
    ->endRepeater()
    ->addTab('Informacion para el calendario', ['placement' => 'left'])
    ->addRepeater('fechasEvento', [
        'label' => 'Dieferentes fechas del ecvento o taller',
        'instructions' => '',
    ])
        ->addDatePicker('diaTaller', [
            'label' => 'Selecciona la hora de comienzo del taller',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'display_format'=> 'yy-m-d',
            'return_format' => 'yy-m-d',

        ])
        ->addTimePicker('horaComienzo', [
            'label' => 'Hora comienzo taller',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'display_format' => 'H:i',
            'return_format' => 'H:i',
        ])
        ->addTimePicker('horaFin', [
            'label' => 'Hora finalizacion taller',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'display_format' => 'H:i',
            'return_format' => 'H:i',
        ])
        ->addTrueFalse('diaCompleto', [
        'label' => 'Selecciona si es dia completo',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ])
    ->endRepeater()
        ->addColorPicker('colorFondo', [
            'label' => 'Selecciona el color para el Taller',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
        ])
        ->addColorPicker('colorTexto', [
            'label' => 'Selecciona el color para el texto Taller',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'default_value' => '',
        ])
    ->addTab('Formulario para el taller', ['placement' => 'left'])
    ->addTrueFalse('verFormulario', [
        'label' => 'Ver / Ocultar el Formulario para e CV',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 1,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ])
    ->addText('formularioCV', [
        'label' => 'Introducir el id del Formulario',
    ])
    ->addTab('Estilos especiales', ['placement' => 'left'])
    ->addTextarea('estilos', [
        'label' => 'Intorcucir estilos especiales',
    ])
    ->addTextarea('codigos', [
        'label' => 'Intorcucir scripts especiales',
    ])
    ;

return $taller;

