<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('page');

$page
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '!=', 'views/template-interiorestudios.blade.php')
    ->and('page_template', '!=', 'views/template-homestudios.blade.php');

$page

    ->addFields(get_field_partial('partials.header'));

return $page;
