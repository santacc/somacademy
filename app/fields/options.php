<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

acf_add_options_page([
    'page_title' => get_bloginfo('name') . ' theme options',
    'menu_title' => 'Opciones Tema',
    'menu_slug'  => 'theme-options',
    'capability' => 'edit_theme_options',
    'position'   => '999',
    'autoload'   => true,
    'icon_url'      => 'dashicons-admin-appearance'


]);

$options = new FieldsBuilder('theme_options');

$options
    ->setLocation('options_page', '==', 'theme-options');

$options

    ->addTab('Configuarcion General', ['placement' => 'left'])
        ->addImage('logoHeader', [
            'label' => 'Logo para el header',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],

        ])
        ->addImage('botonRegistro', [
            'label' => 'Imagen para el boton de registro',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
        ])
    ->addUrl('linkRegistro', [
        'label' => 'Enlace para la landing de talleres',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'default_value' => '',
        'placeholder' => '',
    ])
    ->addTab('Banner header', ['placement' => 'left'])
        ->addTrueFalse('activeBanner', [
            'label' => 'Activar o desactivar el banner de la home situado en el la parte superior',
            'instructions' => '',
            'required' => 0,
            'wrapper' => [
                'width' => '',
                'class' => '',
                'id' => '',
            ],
            'message' => 'Activado el banner',
            'default_value' => 1,
            'ui' => 1,
            'ui_on_text' => 'Si',
            'ui_off_text' => 'No',
        ])
    ->addText('enlaceBanner', [
        'label' => 'Enlace para el banner',
    ])
    ->addText('textoUno', [
        'label' => 'Texto Uno',
    ])
    ->addText('textoDos', [
        'label' => 'Texto Dos ',
    ])
    ->addText('textoTres', [
        'label' => 'Texto Tres',
    ])
    ->addText('textoCuatro', [
        'label' => 'Text Cuatro',
    ])

    ->addText('textoSubCuatro', [
        'label' => 'Text debajo del Cuatro',
    ])
    ->addText('textoCinco', [
        'label' => 'Text Cinco',
    ])
    ->addText('textoSeis', [
        'label' => 'Text Seis',
    ])
    ->addText('textoSiete', [
        'label' => 'Text Siete',
    ])

    ->addTab('Top Bar', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.top_bar'))
    ->addTab('Header Configuracion', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.confHeader'))
    ->addTab('Seccion Banda musicales', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.bandaMusicales'))
    ->addTab('Seccion Puertas Abiertas', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.puertasAbiertas'))
    ->addTab('Seccion Destacados Home', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.destacadosHome'))
    ->addTab('Seccion situación', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.situacionHome'))
    ->addTab('Escuela Segura', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.escuelaSegura'))
    ->addTab('Contacto Home', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.contactoHome'))

;



return $options;
