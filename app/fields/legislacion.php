<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$streaming_page = get_page_by_path('practicas', 'ARRAY_N');

$practicas = new FieldsBuilder('Campos Legislación');

$practicas
    ->setLocation('post_type', '==', 'page')
    ->and('page_template', '==', 'views/template-legislacion.blade.php');

$practicas
    ->addTab('Botones Legislación', ['placement' => 'top'])
    ->addRepeater('botonesLegislacion', [
        'label' => 'Repeater Field',

        ])
        ->addText('textoBortones', [
            'label' => 'Texto para los botones',
        ])
        ->addFile('archivoSubido', [
            'label' => 'Archivos Subidos',
        ])
        ->addText('urlBotones', [
            'label' => 'Para enlace externo',
        ])
    ->endRepeater()
;

return $practicas;
