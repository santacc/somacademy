// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import calendario from './routes/calendario';
import templateHomestudios from './routes/template-homestudios';
import templateInteriorestudios from './routes/template-interiorestudios';
import templateProfesores from './routes/template-profesores';
import templateCasting from './routes/template-casting';
import templatePregrado from './routes/template-pregrado';
/** Populate Router instance with DOM routes */
const routes = new Router({
    // All pages
    common,
    // Home page
    home,
    // About Us page, note the change from about-us to aboutUs.
    aboutUs,

    calendario,

  templateHomestudios,

  templateInteriorestudios,

  templateProfesores,

  templateCasting,

  templatePregrado,

});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
