export default {
  init() {
    // JavaScript to be fired on all pages
    let contador = 0;
    $('.hamburger-dos').on('click',function () {
      $(this).toggleClass('is-active');
      $('.formularioFlotante').css('display', 'none');
      if(contador == 0 ) {
        $('.menuMovil').animate({
          'display': 'block',
          'left': '0',
        }, 500);
        contador = 1;
      } else {
        $('.menuMovil').animate({
          'display': 'block',
          'left': '-100vw',
        }, 500);
        $('.formularioFlotante').css('display', 'block');
        contador = 0;
      }
    });

    $('.hamburger-tres').on('click',function () {
      $(this).toggleClass('is-active');
    });

    $('.menuMovil .menu-item-94 a, .menuMovil .menu-item-95 a').on('click', function(){
      $('.menuMovil').animate({
        'display': 'block',
        'left': '-100vh',
      }, 500);
      $('.hamburger').toggleClass('is-active');
      contador = 0;
    })

    $( window ).resize(function() {
      let anchoCapaLateral = 0 - $('.btnAncla ').width();
      let varCerrado = 0;
      console.log(anchoCapaLateral);

      $( '.btnAncla' ).animate({
        right: anchoCapaLateral,
      }, {
        duration: 1000,
        complete: function() {
          varCerrado = 1;
        },
      });

      $('.btnAbrir').on('click', function(){
        if(varCerrado == 1) {
          $( '.btnAncla' ).animate({
            right: 0,
          }, {
            duration: 500,
            complete: function() {
              varCerrado = 0;
            },
          });
        } else {
          $( '.btnAncla' ).animate({
            right: anchoCapaLateral,
          }, {
            duration: 500,
            complete: function() {
              varCerrado = 1;
            },
          });
        }
      });
    });

    let anchoCapaLateral = 0 - $('.btnAncla').width();
    let varCerrado = 0;


    $( '.btnAncla' ).animate({
      right: anchoCapaLateral,
    }, {
      duration: 1000,
      complete: function() {
        varCerrado = 1;
      },
    });

    $('.btnAbrir').on('click', function(){
      if(varCerrado == 1) {
        $( '.btnAncla' ).animate({
          right: 0,
        }, {
          duration: 500,
          complete: function() {
            varCerrado = 0;
          },
        });
      } else {
        $( '.btnAncla' ).animate({
          right: anchoCapaLateral,
        }, {
          duration: 500,
          complete: function() {
            varCerrado = 1;
          },
        });
      }
    });


    $(document).ready(function(){
      var altura = $('.banner').offset().top;
      $(window).on('scroll', function(){
        if ( $(window).scrollTop() > altura ){
          $('.banner').addClass('headerFixed');
        } else {
          $('.banner').removeClass('headerFixed ');
        }
      });

     /* $('.menuAncla a').click(function() {
        let classDiv = $(this).attr('class');
        console.log(classDiv);
        var destino = $(this.hash);
        console.log(destino.offset())
        if (destino.length == 0) {
          destino = $('a[name="' + this.hash.substr(1) + '"]');
        }
        if (destino.length == 0) {
          destino = $('html');
        }
        $('html, body').animate({ scrollTop: destino.offset().top }, 500);
        return false;
      });*/
    });


    $('.dropdown-item').on('click', function(){
      let varId = $(this).attr('id');
      if(varId == 'ver-todo') {
        let varIdMay = 'Selecciona una disciplina';
        $('#dropdownMenuLink').html(varIdMay);
        $('.itemTaller').each(function () {
            $(this).addClass('visible');
        });
      } else {
        let varIdMay = varId.toUpperCase();
        $('#dropdownMenuLink').html(varIdMay);
        $('.itemTaller').each(function () {
          $(this).removeClass('visible');
          $(this).removeClass('oculto');
          let varCompruebo = $(this).hasClass(varId);
          if (varCompruebo == true) {
            $(this).addClass('visible');
          } else {
            $(this).addClass('oculto');
          }
        });
      }
    });

    let anchoFormularioFlotante = 0 - $('.formularioFlotante').outerWidth();
    let varFormCerrado = 0;


    $('.btnformularioFlotante').on('click', function(){
      if(varFormCerrado == 1) {
        $( '.formularioFlotante' ).animate({
          right: 0,
        }, {
          duration: 500,
          complete: function() {
            varFormCerrado = 0;
            $('.btnformularioFlotante').html('X');
            if($(window).width() < 768) {
              $('.btnformularioFlotante').css({
                left: 0,
              })
            }
          },
        });
      } else {
        $( '.formularioFlotante' ).animate({
          right: anchoFormularioFlotante,
        }, {
          duration: 500,
          complete: function() {
            varFormCerrado = 1;
            $('.btnformularioFlotante').html('descargar dossier');
            if($(window).width() < 768) {
              $('.btnformularioFlotante').css({
                left: '-36px',
              })
            }
          },
        });
      }
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
