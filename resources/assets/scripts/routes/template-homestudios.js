import Glide, {Autoplay, Breakpoints, Controls, Keyboard} from '@glidejs/glide/dist/glide.modular.esm';

export default {
  init() {
    // JavaScript to be fired on the about us page

    console.log('empieza el home estudios');

    let glide = new Glide('.glide', {
      bound: true,
      type: 'slider',
      rewind: false,
      perView: 3,
      gap: 30,
      breakpoints: {
        600: {
          perView: 1,
        },
        900: {
          perView: 2,
        },
        1200: {
          perView: 3,
        },
      },
    })
    glide.mount({ Controls, Autoplay, Keyboard, Breakpoints });


    /*let miVariableAltura = $('.navbar').innerHeight();
    $('#menuEspecial').css({
      'top': miVariableAltura,
    });*/

    $(document).ready(function(){
      var altura = $('.banner').offset().top;
      let miVariableAltura = $('.navbar').innerHeight();
      console.log(miVariableAltura);
      $(window).on('scroll', function(){
        if ( $(window).scrollTop() > altura ){
          $('#menuEspecial').css({
            'position': 'fixed',
            'top': miVariableAltura,
          });
        } else {
          $('#menuEspecial').css({
            'position': 'relative',
            'top': 0,
          });
        }
      });
    });

  },
};
