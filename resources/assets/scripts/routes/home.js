import Glide, { Controls, Autoplay, Keyboard, Breakpoints } from '@glidejs/glide/dist/glide.modular.esm'

export default {
  init() {
    // JavaScript to be fired on the home page
    let glide = new Glide('.glide', {
      bound: true,
      type: 'slider',
      rewind: false,
      perView: 4,
      breakpoints: {
        600: {
          perView: 1,
        },
        900: {
          perView: 2,
        },
        1200: {
          perView: 4,
        },
      },
    })
    glide.mount({ Controls, Autoplay, Keyboard, Breakpoints });


  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
