

export default {
  init() {
    // JavaScript to be fired on the about us page

    console.log('entramso en profesores');
    $('#todos').addClass('fondoRosas');
    $('#todos').on('click',function(){
      $('.itemTaller').css('display', 'block');
      $('#btnFormPro').removeClass('fondoRosas');
      $('#btnFormNoPro').removeClass('fondoRosas');
      $(this).addClass('fondoRosas');
      $('#btnJefesEstudio').removeClass('fondoRosas');

    });
    $('#btnFormPro').on('click',function(){
      $('.itemTaller').css('display', 'none');
      $('.formacionProfesional').css('display', 'block');
      $('#todos').removeClass('fondoRosas');
      $('#btnFormNoPro').removeClass('fondoRosas');
      $(this).addClass('fondoRosas');
      $('#btnJefesEstudio').removeClass('fondoRosas');
    });
    $('#btnFormNoPro').on('click',function(){
      $('.itemTaller').css('display', 'none');
      $('.noProfesional').css('display', 'block');
      $(this).addClass('fondoRosas');
      $('#btnFormPro').removeClass('fondoRosas');
      $('#todos').removeClass('fondoRosas');
      $('#btnJefesEstudio').removeClass('fondoRosas');
    });
    $('#btnJefesEstudio').on('click',function(){
      $('.itemTaller').css('display', 'none');
      $('.jefesEstudio').css('display', 'block');
      $(this).addClass('fondoRosas');
      $('#btnFormPro').removeClass('fondoRosas');
      $('#todos').removeClass('fondoRosas');
      $('#btnFormNoPro').removeClass('fondoRosas');
    });
  },
};
