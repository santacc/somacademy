export default {
  init() {
    // JavaScript to be fired on the about us page

    console.log('empieza el home casting');
    $('.fichaCasting').on('mouseover', function(e){
      e.stopPropagation();
      $(this).find('.capaTextoDestacadoSubir').addClass( 'agrandar');
      $(this).find('.capaBotones').addClass( 'mostrar');
      $(this).find('.descGridDestacado').addClass( 'mostrar' );

    });

    $('.fichaCasting').on('mouseleave', function(e){
      e.stopPropagation();
      $(this).find('.capaBotones').removeClass( 'mostrar');
      $(this).find('.capaTextoDestacadoSubir').removeClass( 'agrandar');
      $(this).find('.descGridDestacado').removeClass( 'mostrar' );
    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
