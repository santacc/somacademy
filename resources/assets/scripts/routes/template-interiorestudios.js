export default {
  init() {
    // JavaScript to be fired on the about us page
    console.log('empieza el home estudios');

    let miVariableAltura = $('#navPrincipal').height();
    $('#menuEspecial').css({
      'top': miVariableAltura,
    });

    $(document).ready(function(){
      var altura = $('.banner').offset().top;
      let miVariableAltura = $('.navbar').innerHeight();
      console.log(miVariableAltura);
      $(window).on('scroll', function(){
        if ( $(window).scrollTop() > altura ){
          $('#menuEspecial').css({
            'position': 'fixed',
            'top': miVariableAltura,
          });
        } else {
          $('#menuEspecial').css({
            'position': 'relative',
            'top': 0,
          });
        }
      });
    });

  },
};
