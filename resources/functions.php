<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);



    // Función para mostrar el perfil del usuario mediante un shortcode
function mostrar_perfil_estudiante_shortcode($atts) {
    ob_start(); // Inicia el almacenamiento en búfer de salida

    // Verifica si el usuario está autenticado
    if (is_user_logged_in()) {
        // Obtiene los datos del usuario actual
        $current_user = wp_get_current_user();
        $current_user2 = get_userdata( $current_user->ID );
        // Mostrar la imagen de perfil
        $profile_picture = get_avatar_url($current_user->ID, array('size' => 96));
        //echo '<p><img src="' . esc_url($profile_picture) . '" alt="Imagen de Perfil"></p>';

      
        $fullsize_path = wp_get_attachment_url( $current_user->subir_archivo ); 
        $filename_only = basename( get_attached_file( $current_user->subir_archivo ) );
        // Muestra los datos del usuario
        echo '<p><strong>Nombre:</strong> ' . esc_html($current_user->first_name) . '</p>';
        echo '<p><strong>Apellidos:</strong> ' . esc_html($current_user->last_name) . '</p>';
        echo '<p><strong>Rol:</strong> ' . esc_html(implode(', ', $current_user->roles)) . '</p>';
        echo '<p><strong>Página Web:</strong> ' . esc_url($current_user->user_url) . '</p>';
        echo '<p><strong>Email:</strong> ' . esc_html($current_user->user_email) . '</p>';
        echo '<p><strong>Disciplina:</strong> ' . esc_html($current_user->disciplina_field). '</p>';
        echo '<p><strong>Idioma:</strong> ' . esc_html($current_user->idioma ). '</p>';
        echo '<p><strong>Nivel Idioma:</strong> ' . esc_html($current_user->nivel_idioma ). '</p>';
        echo '<p><strong>CV:</strong> <a href="'.$fullsize_path.'">' . esc_html($filename_only ). '</a></p>';
        echo '<hr />';
        echo '<h4><strong>PRACTICAS SELECCIONADAS</strong></h4>';
        if($current_user->practica_primera != '') {
            $post1   = get_post( $current_user->practica_primera  );
            echo '<p>Opcion 1: ' . $post1->post_title . '</p>';
         
        }
        if($current_user->practica_segunda != '') {
            $post2   = get_post( $current_user->practica_segunda  );
            echo '<p>Opcion 2: ' . $post2->post_title . '</p>';
        }
        if($current_user->practica_tercera != '') {
           
            $post3   = get_post( $current_user->practica_tercera );
            echo '<p>Opcion 3: ' . $post3->post_title . '</p>';
        }
        
        

       // Full path
 // Just the file name


        // Otros campos que desees mostrar

    } else {
        // Si el usuario no está autenticado, muestra un mensaje de inicio de sesión
        echo '<p>Debes iniciar sesión para ver tu perfil.</p>';
    }

    return ob_get_clean(); // Devuelve el contenido almacenado en búfer de salida
}

// Registra el shortcode
add_shortcode('mostrar_perfil_estudiante', 'mostrar_perfil_estudiante_shortcode');

function mostrar_perfil_empresa_shortcode($atts) {
    ob_start(); // Inicia el almacenamiento en búfer de salida

    // Verifica si el usuario está autenticado
    if (is_user_logged_in()) {
        // Obtiene los datos del usuario actual
        $current_user = wp_get_current_user();
        $current_user2 = get_userdata( $current_user->ID );
        // Mostrar la imagen de perfil
        $profile_picture = get_avatar_url($current_user->ID, array('size' => 96));
        //echo '<p><img src="' . esc_url($profile_picture) . '" alt="Imagen de Perfil"></p>';

      
        $fullsize_path = wp_get_attachment_url( $current_user->subir_archivo ); 
        $filename_only = basename( get_attached_file( $current_user->subir_archivo ) );
        // Muestra los datos del usuario
        echo '<p><strong>Nombre Empresa:</strong> ' . esc_html($current_user->nombre_empresa) . '</p>';
        echo '<p><strong>CIF:</strong> ' . esc_html($current_user->cif_empresa) . '</p>';
        echo '<p><strong>Rol:</strong> ' . esc_html(implode(', ', $current_user->roles)) . '</p>';
        echo '<p><strong>Sector Empresa:</strong> ' . esc_url($current_user->sectro_empresa) . '</p>';
        echo '<p><strong>Dirección:</strong> ' . esc_html($current_user->direccion_empresa) . '</p>';
        echo '<p><strong>localidad:</strong> ' . esc_html($current_user->localidad). '</p>';
        echo '<p><strong>Codigo Postal:</strong> ' . esc_html($current_user->codigo_postal ). '</p>';
        echo '<p><strong>Persona de contacto:</strong> ' . esc_html($current_user->persona_contacto_empresa ). '</p>';
        echo '<p><strong>Email persona de contacto:</strong> ' . esc_html($current_user->email_persona_empresa ). '</p>';
        echo '<p><strong>Cargo persona de contacto:</strong> ' . esc_html($current_user->cargo_persona_empresa ). '</p>';
        
        
       // Full path
 // Just the file name


        // Otros campos que desees mostrar

    } else {
        // Si el usuario no está autenticado, muestra un mensaje de inicio de sesión
        echo '<p>Debes iniciar sesión para ver tu perfil.</p>';
    }

    return ob_get_clean(); // Devuelve el contenido almacenado en búfer de salida
}

// Registra el shortcode
add_shortcode('mostrar_perfil_empresa', 'mostrar_perfil_empresa_shortcode');
