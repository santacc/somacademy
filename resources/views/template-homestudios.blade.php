{{--
  Template Name: Plantilla home estudios
--}}
@extends('layouts.app')


@section('content')

  <!-- Seccion para el slider de la home -->

 <!-- <video width="100%" autoplay loop>
    <source src="/videos/stock-footage-film-production-lens-whacking-school-studio-set.webm">
  </video> -->

  <?php

    $titHomeEstudios = get_field('titHomeEstudios','options');

  ?>

  <section>
    <div class="container p-3">
      <div class="row">
        <div class="col-12 text-center">
          <h2><?php echo $titHomeEstudios; ?></h2>
        </div>
      </div>
    </div>
  </section>
  <section id="encabezadoHomeEstudios">
    @include('partials.home-estudios.sliderHeaderHomeEstudios')
  </section>


  <section class="my-5" id="textosSuperiores" >
    @include('partials.home-estudios.cuadrosTextoSup')
  </section>

  <section class="py-2" id="gridImagenes">
    @include('partials.home-estudios.gridImagenesEstudio')
  </section>



    @include('partials.home-estudios.razones')


  <section id="profesoresCont" class="pt-3 pb-5">
      <h3 class="py-5" style="text-align: center;">JEFES DE ESTUDIO</h3>
      @include('partials.home-estudios.gridProfesores')</div>
  </section>

  <section class="py-3" id="gridAlumnos">
    <div class="container">
      <h3 class="py-5" style="text-align: center;">CONOCE A NUESTROS ALUMNOS</h3>
      <div class="row">

        <div class="col-12"> @include('partials.home-estudios.gridAlumnos')</div>
      </div>
    </div>

  </section>
<section id="formularioOficial">
  <div class="contFormulario">
        <h3>
          <?php
          $tituloSeccionForm = get_field('tituloSeccionForm','option');
          $textoBajoFormularioOfical = get_field('textoBajoFormularioOfical','option');
         echo $tituloSeccionForm;
         ?>
        </h3>

        <?php
         $formCodigoFormulario  = get_field('formCodigoFormulario','option');
         echo '<hr class="wp-block-separator">';
          gravity_form( $formCodigoFormulario , $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $echo = true );
          echo $textoBajoFormularioOfical;
        ?>
     </div>
</section>
@endsection

