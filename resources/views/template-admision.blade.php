{{--
  Template Name: Plantilla Admisiones
--}}
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp


        @include('partials.page-header-estudios')
<?php

        $titFaseUno = get_field('titFaseUno');
        $textFaseUnoP = get_field('textFaseUnoP');
        $textFaseUnoS = get_field('textFaseUnoS');

        $titFaseDos = get_field('titFaseDos');
        $textFaseDosP = get_field('textFaseDosP');
        $textFaseDosS = get_field('textFaseDosS');

        $titFaseTres = get_field('titFaseTres');
        $textFaseTresP = get_field('textFaseTresP');

        $titFaseCuatro = get_field('titFaseCuatro');
        $textFaseCuatroP = get_field('textFaseCuatroP');
  ?>

  <div class="container">
    <div class="row mb-5">
      <div class="col-6 col-md-3 mb-2">
        <div class="fondoNegro">
          <div class="fondoNaranja"><?php echo $titFaseUno; ?></div>
          <p class="textoNaranja">
            <?php echo $textFaseUnoP; ?>
          </p>
          <p class="textoNaranja">
            <?php echo $textFaseUnoS; ?>
          </p>
        </div>
      </div>
      <div class="col-6 col-md-3 mb-2">
        <div class="fondoNegro">
          <div class="fondoAzulClaro"><?php echo $titFaseDos; ?></div>
          <p class="textoAzulOscuro"><?php echo $textFaseDosP; ?></p>
          <p class="textoAzulOscuro"><?php echo $textFaseDosS; ?></p>
        </div>
      </div>
      <div class="col-6 col-md-3 mb-2">
        <div class="fondoNegro">
          <div class="fondoGris"><?php echo $titFaseTres; ?></div>
            <p class="textoAzulOscuro"><?php echo $textFaseTresP; ?></p>
        </div>
      </div>
      <div class="col-6 col-md-3 mb-2">
        <div class="fondoNegro">
          <div class="fondoAzulOscuro">
            <?php echo $titFaseCuatro; ?>
          </div>
          <p class="textoAzulOscuro"><?php echo $textFaseCuatroP; ?></p>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-12 col-md-12">

        @include('partials.content-page')
      </div>
    </div>

  </div>


  @endwhile
@endsection

