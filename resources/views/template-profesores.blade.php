{{--
  Template Name: Tema grid profesores
--}}
@extends('layouts.app')

@section('content')
  @include('partials.page-header-profesores')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
  @php


    $args = array(
        'post_type'=>'profesor',
        'posts_per_page'=>'-1',
        'meta_query' => array(
          'tipo_formacion' => array(
            'key' => 'tipoFormacion',  
            'value' => 'jefesEstudio', 
            'compare' => 'NOT LIKE' 
          ),
        ),
        'orderby' => 'title',
	      'order'   => 'ASC',
	     );
    $loop = new WP_Query($args);

    $args2 = array(
        'post_type'=>'profesor',
        'posts_per_page'=>'-1',
        'meta_query' => array(
          'tipo_formacion' => array(
            'key' => 'tipoFormacion',  
            'value' => 'jefesEstudio', 
            'compare' => 'LIKE' 
          ),
        ),
	     );
    $loop2 = new WP_Query($args2);

  @endphp


  <div class="container">
    <div class="row">
      <div class="col-12 text-center mb-5 p-0">
        <div class="btn-group botonesFiltro" role="group" aria-label="Basic example">
          <button type="button" class="btn btn-outline-primary" id="todos">TODOS</button>
          <button type="button" class="btn btn-outline-primary" id="btnJefesEstudio">DIRECCIÓN ACADÉMICA</button>
          <button type="button" class="btn btn-outline-primary" id="btnFormPro">FORMACIÓN PROFESIONAL</button>
          <button type="button" class="btn btn-outline-primary" id="btnFormNoPro">FORMACIÓN NO PROFESIONAL</button>
        </div>
      </div>
    </div>
    <div class="row justify-content-center row-eq-height">
      @while ($loop2->have_posts()) @php $loop2->the_post() @endphp
      @include('partials.content-'.get_post_type())
      @endwhile
   
      @while ($loop->have_posts()) @php $loop->the_post() @endphp
      @include('partials.content-'.get_post_type())
      @endwhile
    </div>
  </div>
  {!! get_the_posts_navigation() !!}
@endsection
