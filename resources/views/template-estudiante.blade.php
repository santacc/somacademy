{{--
  Template Name: CUSTOM USUARIO
--}}

<?php

$sortCodeVerEstudiante = get_field('sortCodeVerEstudiante','option');
$sortCodeEditarEstudiante = get_field('sortCodeEditarEstudiante','option');
$sortCodePracticasEstudiante = get_field('sortCodePracticasEstudiante','option');

$args = array(
  'post_type'=>'empresa',
  'posts_per_page'=>'-1',

  );
$loop = new WP_Query($args);


  $contEmpresa = '';
while ($loop->have_posts()) {
  $loop->the_post();
  $imagenEmpresa = get_the_post_thumbnail_url(); 
  $contEmpresa .= '<div class="lista-empresa"><div class="txt-empresa"><h2 class="n-empresa">'. get_the_title() .'</h2><hr /></div>';

    $empresa = get_the_ID();



    $args2 = array(
      'post_type'=>'practica',
      'posts_per_page'=>'-1',
      'meta_query' => array(
        
        'empresa' => array(
          'key' => 'empresa',
          'value' => $empresa,
        ),
  ),

      );
    $loopPractica = new WP_Query($args2);
    $contPractica = '';
    $urlEmpresa = get_field('urlEmpresa');
    while ($loopPractica->have_posts()) {
      $loopPractica->the_post();
      

      $nombrePractica = get_field('nombrePractica');
      $nombreFormularioEmpresa = get_field('nombreFormularioEmpresa');  
      $numeroVacantes = get_field('numeroVacantes');
      $numeroTotalHoras = get_field('numeroTotalHoras');
      $descripcionPractica = get_field('descripcionPractica'); 
      $inicio = get_field('inicio');  
      $final = get_field('final');  
      $horario = get_field('horario');  
      $numeroHorasJornada = get_field('numeroHorasJornada');  
      $numeroDiasSemana = get_field('numeroDiasSemana');  
      $duracionMeses = get_field('duracionMeses');
      $direccionEmpresa = get_field('direccionEmpresa'); 
      $localidad = get_field('localidad'); 
      $detallePractica = get_field('detallePractica'); 

      $manualPracticas = get_field('manualPracticas','options');
      $txtManualPracticas = get_field('txtManualPracticas','options');

      $asignado = get_field('asignado');

      if($asignado == 1) {
        $clase = "listado-practica";
      } else {
        $clase = "listado-practica asignada";
      }

      if( $asignado == 1 ) {
       
        $clase = "listado-practica";
        $contPractica .= '<div class="'.$clase.'">
        <div class="item-practica nom-practica">'.$nombrePractica .'</div>
        <div class="item-practica num-vacantes"><strong>Numero vacantes</strong>'.$numeroVacantes.'</div>
        <div class="item-practica practica-asignada">NO ASIGNADA</div>
        <div class="item-practica descripcion-practica"><strong>Descripcion</strong>'.$descripcionPractica.'</div>
        <div class="item-practica f-inicio"><strong>Fecha Inicio</strong>'.$inicio.'</div>
        <div class="item-practica f-final"><strong>Fecha Final</strong>'.$final.'</div>
        <div class="item-practica num-horas"><strong>Numero Jornadas</strong>'.$numeroHorasJornada.'</div>
        <div class="item-practica dias-semana"><strong>Días Semana</strong>'.$numeroDiasSemana.'</div>
        <div class="item-practica dias-semana"><strong>Horas Totales</strong>'.$numeroTotalHoras.'</div>
        <div class="item-practica dracion-meses"><strong>Meses</strong>'.$duracionMeses.'</div>
        <div class="item-practica f-horario"><strong>Horario</strong>'.$horario.'</div>
        <div class="item-practica localidad"><strong>Direccion</strong>'.$direccionEmpresa.'</div>
        <div class="item-practica localidad"><strong>Localidad</strong>'.$localidad.'</div>
       
       
        </div>';

      } else {
        $clase = "listado-practica asignada";
        $contPractica .= '<div class="'.$clase.'">
        <div class="item-practica nom-practica">'.$nombrePractica .'</div>
        <div class="item-practica num-vacantes"><strong>Numero vacantes</strong>'.$numeroVacantes.'</div>
        <div class="item-practica practica-asignada">ASIGNADA</div>
        <div class="item-practica descripcion-practica"><strong>Descripcion</strong>'.$descripcionPractica.'</div>
        <div class="item-practica f-inicio"><strong>Fecha Inicio</strong>'.$inicio.'</div>
        <div class="item-practica f-final"><strong>Fecha Final</strong>'.$final.'</div>
        <div class="item-practica num-horas"><strong>Numero Jornadas</strong>'.$numeroHorasJornada.'</div>
        <div class="item-practica dias-semana"><strong>Días Semana</strong>'.$numeroDiasSemana.'</div>
        <div class="item-practica dias-semana"><strong>Horas Totales</strong>'.$numeroTotalHoras.'</div>
        <div class="item-practica dracion-meses"><strong>Meses</strong>'.$duracionMeses.'</div>
        <div class="item-practica f-horario"><strong>Horario</strong>'.$horario.'</div>
        <div class="item-practica localidad"><strong>Direccion</strong>'.$direccionEmpresa.'</div>
        <div class="item-practica localidad"><strong>Localidad</strong>'.$localidad.'</div>
       
       
        </div>';
      }

    }
    $contEmpresa .= $contPractica .'</div>';
  }  
   

   
?>

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @include('partials.page-header-estudios')
    @include('partials.content-page')

 

    <div class="contendor-perfil">
      <div class="menu">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
          <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">ACCESO</a>
         <?php echo do_shortcode('[wppb-restrict user_roles="estudiante" display_to="a" message=" "] <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">PERFIL</a>
          <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">LISTADO PRÁCTICAS</a>
          <a class="nav-link" id="v-pills-selecpracticas-tab" data-toggle="pill" href="#v-pills-selecpracticas" role="tab" aria-controls="v-pills-profile" aria-selected="false">SELECCIONAR PRÁCTICAS</a>
          [/wppb-restrict]'); ?>
         <?php echo do_shortcode('[wppb-restrict user_roles="estudiante" display_to="a" message=" "] <a href="'.$manualPracticas["url"].'" class="btn btn-descarga"><i class="fa-solid fa-file-pdf"></i>'.$txtManualPracticas.'</a>[/wppb-restrict]'); ?>
        </div>
      </div>
      <div class="contenido">
        <div class="tab-content" id="v-pills-tabContent">
          <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab"><?php echo do_shortcode('[wppb-login]'); ?><?php echo do_shortcode(''.$sortCodeVerEstudiante.''); ?> 
            <?php echo do_shortcode('[wppb-restrict user_roles="estudiante" display_to="a" message=" "][/wppb-restrict]'); ?></div>
          <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab"><?php echo do_shortcode(''.$sortCodeEditarEstudiante.''); ?></div>
            
          <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
            <?php echo do_shortcode('[wppb-restrict user_roles="estudiante" display_to="a" message=" "] <div id="listado-empresas">
              <h3 class="titulo-listado-empresas">LISTADO EMPRESAS / ENTIDADES </h3>
             <div class="contenedor-practicas">'. $contEmpresa .'</div>
             
            </div>[/wppb-restrict]'); ?>
    </div>
    
    <div class="tab-pane fade" id="v-pills-selecpracticas" role="tabpanel" aria-labelledby="v-pills-selecpracticas-tab"><?php echo do_shortcode(''.$sortCodePracticasEstudiante.''); ?></div>
        </div>
      </div>
    </div>
    <style>
      li#wppb-form-element-16 {
        margin-top: 50px
      }
      .wppb-description-delimiter {
        margin: 2% 0 6%; 
      }
      .wppb-form-field label, #wppb-login-wrap .login-username label, #wppb-login-wrap .login-password label, #wppb-login-wrap .login-auth label {
    width: 30%;
    float: left;
    font-weight: bolder !important;
    min-height: 1px;
    font-family: 'sansationbold';
      }
    </style>
  @endwhile
@endsection
