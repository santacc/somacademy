{{--
Template Name: Tema grid talleres junior
--}}
@extends('layouts.app')
@section('content')
@include('partials.page-header-talleres')
<!-- botton call to action!! -->
<?php
$mostrarBanner = get_field('activeBanner');

if($mostrarBanner == 1) {

?>
<div class="btnAncla">
  <a href="<?php the_field('enlaceBanner') ?>" class="enlacePrincipal">
    <?php
    $txtUno = get_field('textoUno');
    if( $txtUno != '' ) {
    // Do something.
    ?>
    <div class="txtUno"><?php echo $txtUno; ?></div>
    <?php } ?>
    <?php
    $txtDos = get_field('textoDos');
    if( $txtDos != '' ) {
    // Do something.
    ?>
    <div class="txtDos"><?php echo $txtDos; ?></div>
    <?php } ?>
    <?php
    $txtTres = get_field('textoTres');
    if( $txtTres != '' ) {
    // Do something.
    ?>
    <div class="txtTres"><?php echo $txtTres; ?></div>
    <?php } ?>
    <?php
    $txtCuatro = get_field('textoCuatro');
    if( $txtCuatro != '' ) {
    // Do something.
    ?>
    <div class="txtCuatro"><?php echo $txtCuatro; ?></div>
    <?php } ?>
    <?php
    $txtSubCuatro = get_field('textoSubCuatro');
    if( $txtSubCuatro != '' ) {
    // Do something.
    ?>
    <div class="txtSubCuatro"><?php echo $txtSubCuatro; ?></div>
    <?php } ?>
    <?php
    $txtCinco = get_field('textoCinco');
    if( $txtCinco != '' ) {
    // Do something.
    ?>
    <div class="txtCinco"><?php echo $txtCinco; ?></div>
    <?php } ?>
    <?php
    $txtSeis = get_field('textoSeis');
    if( $txtSeis != '' ) {
    // Do something.
    ?>
    <div class="txtCinco"><?php echo $txtSeis; ?></div>
    <?php } ?>
    <?php
    $txtSiete = get_field('textoSiete');
    if( $txtSiete != '' ) {
    // Do something.
    ?>
    <div class="txtCinco"><?php echo $txtSiete; ?></div>
    <?php } ?>

  </a>
</div>
<?php } ?>

<?php
// Check rows exists.
if( have_rows('logosMusicales') ):

  $logoMusicales = '';

  // Loop through rows.
  while( have_rows('logosMusicales') ) : the_row();

    // Load sub field value.
    $subImagen = get_sub_field('imgLogos');
    // Do something...
    $logoMusicales .= '<div class="col-4 col-md-4 col-lg-1 align-self-center p-4"><img src="'. $subImagen['url'] .'" width="100%" alt="'. $subImagen['name'] .'"></div>';

    // End loop.
  endwhile;

// No value.
else :
  // Do something...
endif;

?>

<section id="bandaMusicales">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="txtDestacadoHome">

          SOM Academy, creada por SOM Produce, los productores de
        </h2>
      </div>
    </div>
  </div>
  <?php 
  $activeBanda = get_field('activeBanda');
  if($activeBanda == 1) { ?>
  <div class="container">
    <div class="row justify-content-center">
      <?php echo $logoMusicales; ?>
    </div>
  </div>
  <?php } ?>
</section>
@if (!have_posts())
<div class="alert alert-warning">
  {{ __('Sorry, no results were found.', 'sage') }}
</div>
{!! get_search_form(false) !!}
@endif
@php
$args = array(
    'post_type'=>'taller',
    'posts_per_page'=>'-1',
    'meta_query' => array(
        'relation' => 'AND',
        'tipo_alumno' => array(
          'key' => 'tipoAlumnos',
          'value' => 'junior',
        ),
        'fecha_com' => array(
          'key' => 'fecha_comienzo',
        ),
      ),
      'orderby' => array(
        'fecha_com' => 'DESC',
      ),
    );
$loop = new WP_Query($args);
@endphp
<div class="container">
  <div class="row">
    <div class="col-12">
      <?php the_content(); ?>
    </div>
  </div>
</div>
<div class="container">
  <div class="row justify-content-center row-eq-height">
    @while ($loop->have_posts()) @php $loop->the_post() @endphp
    @include('partials.content-'.get_post_type())
    @endwhile
  </div>
</div>
{!! get_the_posts_navigation() !!}
@endsection
