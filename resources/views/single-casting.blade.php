<?php
global $post;
$codigoEstilo = get_field('codigoEstilo');
$styleAudicion = get_field('styleAudicion');
?>
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

  @include('partials.content-single-'.get_post_type())
  @endwhile
@endsection
<style>
  <?php echo $styleAudicion; ?>
<?php echo $codigoEstilo; ?>
</style>
