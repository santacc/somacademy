{{--
Template Name: Tema Grados
--}}
<?php
$mostrarBanner = get_field('activeBanner','option');

if($mostrarBanner == 1) {

?>
<div class="btnAncla menuAncla"><div class="btnAbrir">+ Información</div>
  <a href="<?php the_field('enlaceBanner','option') ?>" class="enlacePrincipal">
    <?php
    $txtUno = get_field('textoUno','option');
    if( $txtUno != '' ) {
    // Do something.
    ?>
    <div class="txtUno"><?php echo $txtUno; ?></div>
    <?php } ?>
    <?php
    $txtDos = get_field('textoDos','option');
    if( $txtDos != '' ) {
    // Do something.
    ?>
    <div class="txtDos"><?php echo $txtDos; ?></div>
    <?php } ?>
    <?php
    $txtTres = get_field('textoTres','option');
    if( $txtTres != '' ) {
    // Do something.
    ?>
    <div class="txtTres"><?php echo $txtTres; ?></div>
    <?php } ?>
    <?php
    $txtCuatro = get_field('textoCuatro','option');
    if( $txtCuatro != '' ) {
    // Do something.
    ?>
    <div class="txtCuatro"><?php echo $txtCuatro; ?></div>
    <?php } ?>
    <?php
    $txtSubCuatro = get_field('textoSubCuatro','option');
    if( $txtSubCuatro != '' ) {
    // Do something.
    ?>
    <div class="txtSubCuatro"><?php echo $txtSubCuatro; ?></div>
    <?php } ?>
    <?php
    $txtCinco = get_field('textoCinco','option');
    if( $txtCinco != '' ) {
    // Do something.
    ?>
    <div class="txtCinco"><?php echo $txtCinco; ?></div>
    <?php } ?>
    <?php
    $txtSeis = get_field('textoSeis','option');
    if( $txtSeis != '' ) {
    // Do something.
    ?>
    <div class="txtCinco"><?php echo $txtSeis; ?></div>
    <?php } ?>
    <?php
    $txtSiete = get_field('textoSiete','option');
    if( $txtSiete != '' ) {
    // Do something.
    ?>
    <div class="txtCinco"><?php echo $txtSiete; ?></div>
    <?php } ?>

  </a>
</div>
<?php } ?>
@extends('layouts.app')
@section('content')
  @include('partials.page-header-talleres')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php the_content(); ?>
      </div>
    </div>
  </div>

  <?php
      $activarEquipoDocente = get_field('activarEquipoDocente');

      if($activarEquipoDocente == 1) {
      $contDestacadoDoecente = '<div class="container"><div class="row">';
        if( have_rows('destacadoDocente') ):
            while( have_rows('destacadoDocente') ): the_row();
                // Get sub field values.
                $image = get_sub_field('fotoDoecenteDestacado');
              $nomDocenteDestacado = get_sub_field('nombreProfeDestacado');
              $titDocenteDestacado = get_sub_field('puestoProfeDestacado');
              $descProfeDestacado = get_sub_field('descProfeDestacado');
                //$link = get_sub_field('link');
              $contDestacadoDoecente .= '<div class="col-3">
                                            <img src="'. $image["url"]  .'" alt="'. $image["alt"]  .'" width="100%"/>
                                            </div>';
              $contDestacadoDoecente .= '<div class="col-9" style="align-content: center; align-self: center; align-items: center"><h3 style="width: fit-content">'.$nomDocenteDestacado.' <hr class="wp-block-separator"></h3><h4>'.$titDocenteDestacado.'</h4><p>'.$descProfeDestacado.'</p></div>';
         endwhile;
       endif;
      $contDestacadoDoecente .= '</div></div>';


  $contenedorGenralDocentes = '';
  if( have_rows('disciplinas') ):
    $contProfesores = '';
    $contProfesoresInterior = '';
    $contJefeEstudios = '';
    $contJefeEstudiosInterior = '';
    while( have_rows('disciplinas') ) : the_row();
      $titDisciplina = get_sub_field('titDisciplina');
      $etiquetaProfesores = get_sub_field('etiquetaProfesores');
      $profesores = get_sub_field('profesorGrado');
      $etiquetaJefeEstudios = get_sub_field('etiquetaJefeEstudios');
      $jefeEstudiosDisciplina = get_sub_field('jefeEstudiosDisciplina');
      $contenedorGenralDocentes .= '<div class="container mt-5">
                                        <div class="row">
                                        <div class="col-12">
                                        <h3>'. $titDisciplina .'</h3>
                                        <hr class="wp-block-separator"></div></div>';
      if( $jefeEstudiosDisciplina ):
        $contJefeEstudios .='<div class="row"><div class="col-12 col-md-6">  <h4 style="width: fit-content">'. $etiquetaJefeEstudios .'<hr class="wp-block-separator"></h4>
                                      </div></div><div class="row">';
                                      foreach( $jefeEstudiosDisciplina as $jefeEstudio ):
                                        $idJefe = $jefeEstudio->ID;
                                        $fotoProfesor = get_field('imageProfesor', $idJefe);
                                        setup_postdata($jefeEstudio);
                                        $contJefeEstudiosInterior.= '<div class="col-3 mb-4 fichaProf">

                                                                                                <a href="'. $jefeEstudio->guid .'" class="linkFichaProf">
                                                                                                  <img src="'. $fotoProfesor["url"] .'" alt="'. $fotoProfesor["alt"] .'" width="100%" class="imgProfe">
                                                                                                    <h3 class="titProf"> '.  $jefeEstudio->post_title.'</h3>
                                                                                                </a>
                                                                                            </div>';
                                      endforeach;
                                      $contJefeEstudios .= $contJefeEstudiosInterior. '</div>';
                                      wp_reset_postdata();
      endif;
      $contenedorGenralDocentes .= $contJefeEstudios;
      $contJefeEstudiosInterior = '';
      $contJefeEstudios= '';


        $etiquetaProfesores = get_sub_field('etiquetaProfesores');
        $profesores = get_sub_field('profesorGrado');
        if( $profesores ):
          $contProfesores .='<div class="row">
                                    <div class="col-12 col-md-6">
                                      <h4  style="width: fit-content">'. $etiquetaProfesores .'<hr class="wp-block-separator"></h4>
                                      </div></div><div class="row">';
                                     foreach( $profesores as $profesor ):
                                    $idProf = $profesor->ID;
                                    $fotoProfesor = get_field('imageProfesor', $idProf);
                                    setup_postdata($profesor);
                                    $contProfesoresInterior.= '<div class="col-3 mb-4 fichaProf">
                                                                  <a href="'. $profesor->guid .'" class="linkFichaProf">
                                                                    <img src="'. $fotoProfesor["url"] .'" alt="'. $fotoProfesor["alt"] .'" width="100%" class="imgProfe">
                                                                      <h3 class="titProf"> '.  $profesor->post_title.'</h3>
                                                                  </a>
                                                              </div>';
                                    endforeach;
                                  $contProfesores .= $contProfesoresInterior. '</div></div>';
                                  wp_reset_postdata();
                                  endif;
          $contenedorGenralDocentes .= $contProfesores;
          $contProfesoresInterior = '';
      $contProfesores = '';



    endwhile;
  else:

  endif;
  $contenedorGenralDocentes .= '</div></div>';

   echo  $contDestacadoDoecente;
    echo $contenedorGenralDocentes;
}
  $mostrarDossier = '';
  $tituloDosier = get_field('tituloDossier');
  $codigoDossier = get_field('codeDossier');
  $descargarDossier = get_field('archivoDossier');

  if($tituloDosier != '') {
    $mostrarDossier .= '<a name="dossier"></a><div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <h3>'.$tituloDosier.'</h3>
                                        <hr class="wp-block-separator">
                                        '.do_shortcode( $codigoDossier ).'
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12" style="text-align: center">
                                    <a href="'. $descargarDossier['url'] .'"  class="btnDescargaDossier">Descargar el Dossier</a>
                                </div>
                                </div>
                            </div>';
    echo $mostrarDossier;
  }


  //echo do_shortcode( $codigoDossier );

  ?>
      <?php

          $contenidoMostrar = '';
          $tituloHorarios = get_field('titHorarios');
          $contenidoMostrar = '<div class="container"><div class="row"><div class="col-12"><h3>'. $tituloHorarios .'</h3></div></div></div>';

      if( have_rows('horario') ):

        while( have_rows('horario') ) : the_row();
          $contenidoMostrar .=  '<div class="container horarioClase"><div class="row"><div class="col-12"><div class="tablaTitulos">';
          $nomHorario = get_sub_field('nombreHorario');
          $cicHorario = get_sub_field('cicloHorario');
          $turnHorario = get_sub_field('turnoHorario');
          $horasHorario = get_sub_field('horasHorario');
         // $contenidoMostrar .= '<div class="titulosHorario nomHorario">'.$nomHorario.'</div>';
         // $contenidoMostrar .= '<div class="titulosHorario cicloHorario">'.$cicHorario.'</div>';
          $contenidoMostrar .= '<div class="titulosHorario turnoHorario">'. $turnHorario .'  '. $horasHorario .'</div>';
          $contenidoMostrar .= '</div></div></div>';
          if( have_rows('diasLectivos') ):
            $contenidoMostrar .= ' <div class="row"><div class="col-12"><div class="tablaHorarios">';
            while( have_rows('diasLectivos') ) : the_row();
             $nomDia = get_sub_field('nombreDia');
              $contenidoMostrar .=  '<div class="columnaDia">';
              $contenidoMostrar .= '<div class="diaSemana">'. $nomDia .'</div>';
              if( have_rows('horasClases') ):
                while( have_rows('horasClases') ) : the_row();
                  $horaClase = get_sub_field('horaClase');
                  $nomClase = get_sub_field('nombreClase');
                  $contenidoMostrar .= '<div class="claseHora">'.  $horaClase . ' ' .  $nomClase . '</div>';
                endwhile;
                $contenidoMostrar .= '</div>';
              else :
              endif;
            endwhile;
            $contenidoMostrar .= '</div></div></div></div>';
          else :
          endif;
        endwhile;
        $contenidoMostrar .= '</div></div></div>';
      else :
      endif;
      echo $contenidoMostrar;
      ?>






  <div class="container">
    <div class="row">
      <div class="col-12">
        <a name="formulario"></a>
        <h3>
          <?php
          $titFormulario = get_field('tituloFormulario');
          $textoBajoFormulario = get_field('textoBajoFormulario');
         echo $titFormulario;
         ?>
        </h3>

        <?php
         $formCodigo  = get_field('titFormulario');
         echo '<hr class="wp-block-separator">';
          gravity_form( $formCodigo , $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $echo = true );
       echo $textoBajoFormulario;
        ?>




      </div>
    </div>
  </div>


@endsection

