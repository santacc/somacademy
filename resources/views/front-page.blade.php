
@extends('layouts.app')


@section('content')
  <!-- botton call to action!! -->
  <?php
  $mostrarBanner = get_field('activeBanner','option');

  if($mostrarBanner == 1) {

  ?>

  <div class="btnAncla menuAncla"><div class="btnAbrir">+ Información</div>
    <a href="<?php the_field('enlaceBanner','option') ?>" class="enlacePrincipal">
     <?php
      $txtUno = get_field('textoUno','option');
      if( $txtUno != '' ) {
      // Do something.
     ?>
      <div class="txtUno"><?php echo $txtUno; ?></div>
    <?php } ?>
       <?php
       $txtDos = get_field('textoDos','option');
       if( $txtDos != '' ) {
       // Do something.
       ?>
       <div class="txtDos"><?php echo $txtDos; ?></div>
       <?php } ?>
       <?php
       $txtTres = get_field('textoTres','option');
       if( $txtTres != '' ) {
       // Do something.
       ?>
       <div class="txtTres"><?php echo $txtTres; ?></div>
       <?php } ?>
       <?php
       $txtCuatro = get_field('textoCuatro','option');
       if( $txtCuatro != '' ) {
       // Do something.
       ?>
       <div class="txtCuatro"><?php echo $txtCuatro; ?></div>
       <?php } ?>
       <?php
       $txtSubCuatro = get_field('textoSubCuatro','option');
       if( $txtSubCuatro != '' ) {
       // Do something.
       ?>
       <div class="txtSubCuatro"><?php echo $txtSubCuatro; ?></div>
       <?php } ?>
       <?php
       $txtCinco = get_field('textoCinco','option');
       if( $txtCinco != '' ) {
       // Do something.
       ?>
       <div class="txtCinco"><?php echo $txtCinco; ?></div>
       <?php } ?>
       <?php
       $txtSeis = get_field('textoSeis','option');
       if( $txtSeis != '' ) {
       // Do something.
       ?>
       <div class="txtCinco"><?php echo $txtSeis; ?></div>
       <?php } ?>
       <?php
       $txtSiete = get_field('textoSiete','option');
       if( $txtSiete != '' ) {
       // Do something.
       ?>
       <div class="txtCinco"><?php echo $txtSiete; ?></div>
       <?php } ?>

    </a>

  </div>
  <?php } ?>
  <!-- <?php // add_revslider('mi-slider-1'); ?> -->
  <!-- Seccion para el slider de la home -->
  <section id="encabezadoHome">
    @include('partials.front-page.sliderHeader')
  </section>
  <!-- Seccion para el texto superior la home
  <section id="textoPrimero">
    <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="txtDestacadoHome">
          SOM Academy, la primera escuela de musicales en España, creada por SOM Produce, la mayor productora española de musicales y miembro de la Broadway League
          <br /> Ven a SOM Academy.
        </h2>
      </div>
    </div>
  </div>
  </section> -->
  <!-- Seccion par la banda de los musicales -->

  <?php
  $mostrarBanda = get_field('activeBanda','option');

  if($mostrarBanda == 1) {
      $imgFondoSeccion = get_field('fondoSeccion','option');
  ?>
  <section id="bandaMusicales" style="background-image: url(<?php echo $imgFondoSeccion['url']; ?>)">
    @include('partials.front-page.bandaMusicales')
  </section>
  <?php } ?>
  <!-- Seccion para los destacados -->
  <?php
  $mostrarDestacados = get_field('activeDestacados','option');
  if($mostrarDestacados == 1) {
    ?>
  <section id="destacadosHome">
    @include('partials.front-page.seccionDestacadoHome')
  </section>
  <?php } ?>
  <!-- Seccion para situacion -->
  <?php
  $mostrarSituacion = get_field('activeSituacion','option');
  if($mostrarSituacion == 1) {
    ?>
  <section id="situacion">
    @include('partials.front-page.seccionSituacion')
  </section>
  <!-- Seccion para el formulario de puertas abiertas -->
  <?php
  $mostrarPuertas = get_field('activePuertasAbiertas','option');
  if($mostrarPuertas == 1) {
  ?>
  <section id="puertasAbiertas">
    @include('partials.front-page.seccionPuertasAbiertas')
  </section>
  <?php } ?>
  <!-- Seccion para medidas covid -->
  <?php } ?>
  <?php
  $mostrarMedidas = get_field('activeMedidas','option');
  if($mostrarMedidas == 1) {
    ?>
  <section id="medidas">
    @include('partials.front-page.seccionMedidas')
  </section>
  <?php } ?>
  <!-- Seccion para el formulariod e contacto -->
  <?php
  $mostrarContacto = get_field('activeContacto','option');
  if($mostrarContacto == 1) {
    ?>
  <section id="formContacto">
    @include('partials.front-page.seccionContacto')
  </section>
  <?php } ?>
@endsection

