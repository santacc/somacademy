{{--
  Template Name: Tema Castings
--}}

@extends('layouts.app')
<?php

$args = [
  'post_type' => 'casting',
  'posts_per_page' => -1,
];
$loop = new WP_Query( $args );
?>

@section('content')
  @include('partials.page-header-casting')
  <div class="container">
    <div class="row justify-content-center">
      @if (!$loop->have_posts())
        <div class="col-12 p-5 text-center">
          <h3>Actualmente no tenemos ningún casting activo. </h3>
        </div>
      @endif

      @while($loop->have_posts()) @php $loop->the_post() @endphp
      @include('partials.content-'.get_post_type())
      @endwhile
    </div>
  </div>

@endsection
