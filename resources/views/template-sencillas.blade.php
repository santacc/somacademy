{{--
  Template Name: Tema sencillo
--}}
<?php
$imagenHeader = get_the_post_thumbnail_url();
?>
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

  @include('partials.page-header-sencillas')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10 col-md-8">

       @include('partials.content-page')
      </div>
    </div>
  </div>
  @endwhile
@endsection

