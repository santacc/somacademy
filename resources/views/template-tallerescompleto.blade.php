{{--
  Template Name: Tema grid talleres completos
--}}
@extends('layouts.app')

@section('content')
  @include('partials.page-header-talleres')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
  @php
    $args = array(
        'post_type'=>'taller',
        'posts_per_page'=>'-1',
       'meta_query' => array(
        'relation' => 'AND',
        'tipo_alumno' => array(
          'key' => 'tipoAlumnos',
          'value' => 'junior',
        ),
        'fecha_com' => array(
          'key' => 'fecha_comienzo',
        ),
      ),
      'orderby' => array(
        'fecha_com' => 'ASC',
      ),
        );
    $loop = new WP_Query($args);
  @endphp
  <div class="container my-4">
    <div class="row botonesSup">
      <div class="col-6 col-md-6"><a href="/grado-profesional/pregrado-en-teatro-musical/#formulario" class="btndifCompras">Pregrado en <br />teatro musical</a></div>
      <div class="col-6 col-md-6"><a href="/grado-profesional/grado-profesional/#formulario" class="btndifCompras">Grado en <br />teatro musical</a></div>
      <!-- <div class="col-6 col-md-4"><a href="/grado-profesional/postgrado-en-teatro-musical/#formulario" class="btndifCompras">Postgrado en <br />teatro musical</a></div>
     <div class="col-6 col-md-3"><a href="/summer-practicas/#formulario" class="btndifCompras">Summer <br />practicas</a></div> -->
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>SOM Junior Academy</h2>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row justify-content-center row-eq-height">
      @while ($loop->have_posts()) @php $loop->the_post() @endphp
      @include('partials.content-tallerescompleto')
      @endwhile
    </div>
  </div>
  <?php
    $args = array(
      'post_type'=>'taller',
      'posts_per_page'=>'-1',
      'meta_query' => array(
        'relation' => 'AND',
        'tipo_alumno' => array(
          'key' => 'tipoAlumnos',
          'value' => 'profesionales',
        ),
        'fecha_com' => array(
          'key' => 'fecha_comienzo',
        ),
      ),
      'orderby' => array(
        'fecha_com' => 'ASC',
      ),

      );
    $loop = new WP_Query($args);


  //listado de taxonomias
  $taxonomy = 'disciplina';
  $tax_terms = get_terms(
    $taxonomy, array(
      'hide_empty' => true,
      )
  );
  $menuDrop = '';
    foreach ($tax_terms as $tax_term) {
      $menuDrop .= '<div class="dropdown-item" id="'. $tax_term->slug .'">' .  $tax_term->name.'</div>';
    }
    $menuDrop .= '<div class="dropdown-item" id="ver-todo">Ver Todos</div>';

 ?>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Talleres Adultos</h2>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12" style="text-align: center">
        <div class="dropdown show" style="text-transform: uppercase">
          <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Selecciona una disciplina
          </a>

          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <?php echo $menuDrop ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-center row-eq-height">
      @while ($loop->have_posts()) @php $loop->the_post() @endphp
      @include('partials.content-tallerescompleto')
      @endwhile
    </div>
  </div>
  {!! get_the_posts_navigation() !!}
@endsection
