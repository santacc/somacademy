{{--
  Template Name: PERFIL EMPRESA
--}}

<?php

$formularioSatisfaccion = get_field('formularioSatisfaccion','option');
$formularioPractica = get_field('formularioPractica','option');
$sortCodeVerEmpresa = get_field('sortCodeVerEmpresa','option');
$sortCodeEmpresaEditar = get_field('sortCodeEmpresaEditar','option');
$current_user = wp_get_current_user();
$current_user2 = get_userdata( $current_user->ID );
$procedimientoPaso = get_field('procedimientoPaso','options');
$txtProcedimientoPaso = get_field('txtProcedimientoPaso','options');
    $args = array(
      'post_type'=>'practica',
      'posts_per_page'=>'-1',
      'meta_query' => array(
        
        'id_empresa' => array(
          'key' => 'idEmpresa',
          'value' => $current_user->ID,
        ),
  ),

      );
    $loopPractica = new WP_Query($args);
    $contPractica = '';
    $asignado == 0;
    while ($loopPractica->have_posts()) {
      $loopPractica->the_post();
      

      $nombrePractica = get_field('nombrePractica');
      $nombreFormularioEmpresa = get_field('nombreFormularioEmpresa');  
      $numeroVacantes = get_field('numeroVacantes');
      $numeroTotalHoras = get_field('numeroTotalHoras');
      $descripcionPractica = get_field('descripcionPractica'); 
      $inicio = get_field('inicio');  
      $final = get_field('final');  
      $horario = get_field('horario');  
      $numeroHorasJornada = get_field('numeroHorasJornada');  
      $numeroDiasSemana = get_field('numeroDiasSemana');  
      $duracionMeses = get_field('duracionMeses');
      $direccionEmpresa = get_field('direccionEmpresa'); 
      $localidad = get_field('localidad'); 
      $detallePractica = get_field('detallePractica'); 

      $manualPracticas = get_field('manualPracticas','options');
      $txtManualPracticas = get_field('txtManualPracticas','options');

      $asignado = get_field('asignado');


      $users = get_field('idEstudiante');

      $contUser = '';
      if( $users ) {
            $listadoAlumnos = '<div class="listado-alumnos"><h4 class="titulo-seccion">ALUMNO ASIGNADOS A ESTA PRÁCTICA</h4>';
            foreach( $users as $user ){
                $Objeto = (object)$user;
                $contUser .= '<div class="item-alumno"><div class="nom-alumno">'. $Objeto->user_firstname .'</div>';
                $contUser .= '<div class="apellido-alumno">'. $Objeto->user_lastname .'</div></div>';
               
            }
        
            $listadoAlumnos  .= $contUser .'</div>';
    
    }
    

      if( $asignado == 1 ) {
        
        $clase = "listado-practica";
        $contPractica .= '<div class="'.$clase.'">
        <div class="item-practica nom-practica">'.$nombrePractica .'</div>
        <div class="item-practica num-vacantes"><strong>Numero vacantes</strong>'.$numeroVacantes.'</div>
        <div class="item-practica practica-asignada">NO ASIGNADA</div>
        <div class="item-practica descripcion-practica"><strong>Descripcion</strong>'.$descripcionPractica.'</div>
        <div class="item-practica f-inicio"><strong>Fecha Inicio</strong>'.$inicio.'</div>
        <div class="item-practica f-final"><strong>Fecha Final</strong>'.$final.'</div>
        <div class="item-practica num-horas"><strong>Numero Jornadas</strong>'.$numeroHorasJornada.'</div>
        <div class="item-practica dias-semana"><strong>Días Semana</strong>'.$numeroDiasSemana.'</div>
        <div class="item-practica dias-semana"><strong>Horas Totales</strong>'.$numeroTotalHoras.'</div>
        <div class="item-practica dracion-meses"><strong>Meses</strong>'.$duracionMeses.'</div>
        <div class="item-practica f-horario"><strong>Horario</strong>'.$horario.'</div>
        <div class="item-practica f-horario"><strong>Funciones a realizar</strong>'.$detallePractica.'</div>
        <div class="item-practica localidad"><strong>Direccion</strong>'.$direccionEmpresa.'</div>
        <div class="item-practica localidad"><strong>Localidad</strong>'.$localidad.'</div>

       
        </div>';

      } else {
        $clase = "listado-practica asignada";
        $contPractica .= '<div class="'.$clase.'">
        <div class="item-practica nom-practica">'.$nombrePractica .'</div>
        <div class="item-practica num-vacantes"><strong>Numero vacantes</strong>'.$numeroVacantes.'</div>
        <div class="item-practica practica-asignada">ASIGNADA</div>
        <div class="item-practica descripcion-practica"><strong>Descripcion</strong>'.$descripcionPractica.'</div>
        <div class="item-practica f-inicio"><strong>Fecha Inicio</strong>'.$inicio.'</div>
        <div class="item-practica f-final"><strong>Fecha Final</strong>'.$final.'</div>
        <div class="item-practica num-horas"><strong>Numero Jornadas</strong>'.$numeroHorasJornada.'</div>
        <div class="item-practica dias-semana"><strong>Días Semana</strong>'.$numeroDiasSemana.'</div>
        <div class="item-practica dias-semana"><strong>Horas Totales</strong>'.$numeroTotalHoras.'</div>
        <div class="item-practica dracion-meses"><strong>Meses</strong>'.$duracionMeses.'</div>
        <div class="item-practica f-horario"><strong>Horario</strong>'.$horario.'</div>
        <div class="item-practica f-horario"><strong>Funciones a realizar</strong>'.$detallePractica.'</div>
        <div class="item-practica localidad"><strong>Direccion</strong>'.$direccionEmpresa.'</div>
        <div class="item-practica localidad"><strong>Localidad</strong>'.$localidad.'</div>
        '.$listadoAlumnos.'</div>';
      }


    }
    $contEmpresa .= $contPractica .'</div>';

   

   
?>

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @include('partials.page-header-estudios')
    @include('partials.content-page')

 

    <div class="contendor-perfil">
      
      <div class="menu">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
          <?php echo do_shortcode('[wppb-restrict display_to="not_logged_in" message=" "]<a class="nav-link active" id="v-pills-formulario-tab" data-toggle="pill" href="#v-pills-formulario" role="tab" aria-controls="v-pills-formulario" aria-selected="true">INSCRIPCIÓN EMPRESA</a>[/wppb-restrict]'); ?>
          <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">ACCESO</a>
           <?php echo do_shortcode('[wppb-restrict user_roles="empresa" display_to="a" message=" "] <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">PERFIL</a>

          <a class="nav-link" id="v-pills-practica-tab" data-toggle="pill" href="#v-pills-practica" role="tab" aria-controls="v-pills-practica" aria-selected="false">AGREGAR PRACTICA</a>
          <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">LISTADO PRÁCTICAS</a>
            <a class="nav-link" id="v-pills-cuestionario-tab" data-toggle="pill" href="#v-pills-cuestionario" role="tab" aria-controls="v-pills-cuestionario" aria-selected="false">CUESTIONARIO DE SATISFACIÓN</a>
          [/wppb-restrict]'); ?>
        </div>
        <?php echo do_shortcode('[wppb-restrict user_roles="empresa"  message=" "]        <a href="'.$procedimientoPaso["url"].'" class="btn btn-descarga"><i class="fa-solid fa-file-pdf"></i>'.$txtProcedimientoPaso.'</a>[/wppb-restrict]'); ?>

      </div>
      <div class="contenido">
        <div class="tab-content" id="v-pills-tabContent">
          <?php echo do_shortcode('[wppb-restrict display_to="not_logged_in" message=" "]<div class="tab-pane fade formulario show active" id="v-pills-formulario" role="tabpanel" aria-labelledby="v-pills-formulario-tab">[/wppb-restrict]'); ?>
            <?php echo do_shortcode('[wppb-restrict user_roles="empresa"  message=" "]<div class="tab-pane fade formulario" id="v-pills-formulario" role="tabpanel" aria-labelledby="v-pills-formulario-tab">[/wppb-restrict]'); ?>
            <section class="form-satisfaccion">
              <div class="cont-interior-form">
                <h3 class="titulo-impresos">¿Tienes una empresa y estás en búsqueda de talento?</h3>
                <div class="">Rellena este formulario y nos pondremos en contacto contigo</div>
                <div class="">Consulta nuestro procedimiento paso a paso <a href="<?php echo $procedimientoPaso["url"]; ?>">aquí</a></div>
                <?php $idFormulario = get_field('idFormulario','option'); 
            
                  echo do_shortcode('[gravityform id="'.$idFormulario.'" title="false"]'); ?>
              </div>
            
            </section>
          </div>
          <?php echo do_shortcode('[wppb-restrict display_to="not_logged_in" message=" "]<div class="tab-pane fade" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">[/wppb-restrict]'); ?>
            <?php echo do_shortcode('[wppb-restrict user_roles="empresa"  message=" "]<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">[/wppb-restrict]'); ?>
            <?php echo do_shortcode('[wppb-login redirect_url="/perfil-empresa/"]'); ?><?php echo do_shortcode(''.$sortCodeVerEmpresa.''); ?> 
            </div>
            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
              <?php  echo do_shortcode('[wppb-edit-profile form_name="perfil-empresa"]'); ?>
            </div>
            <div class="tab-pane fade" id="v-pills-cuestionario" role="tabpanel" aria-labelledby="v-pills-cuestionario-tab">
              
              <div class="cont-formulario">
               
              
                <?php  echo do_shortcode('[gravityform id="'.$formularioSatisfaccion.'" title="false"]'); ?>
              </div>
            </div>
          <div class="tab-pane fade" id="v-pills-practica" role="tabpanel" aria-labelledby="v-pills-practica-tab">
            <div class="cont-formulario">
              <h3 class="titulo-impresos">ENVIAR PROPUESTA PRÁCTICA</h3>
              <div class="">Rellena este formulario y envianos tu propuesta de práctica</div>
              <?php  echo do_shortcode('[gravityform id="'.$formularioPractica.'" title="false"]'); ?>
            </div>
          </div>
         <div class="tab-pane fade practicas-list" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
           <div class="cont-listado-practicas"> <h3 class="titulo-impresos">PRÁCTICAS OFERTADAS</h3><?php echo $contEmpresa; ?></div>
        </div>
      
        </div>
      </div>
    </div>
    <style>
      li#wppb-form-element-16 {
        margin-top: 50px
      }
      .wppb-form-field label, #wppb-login-wrap .login-username label, #wppb-login-wrap .login-password label, #wppb-login-wrap .login-auth label {
    width: 30%;
    float: left;
    font-weight: bolder !important;
    min-height: 1px;
    font-family: 'sansationbold';
      }
    </style>
  @endwhile
@endsection
