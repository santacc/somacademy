{{--
  Template Name: Plantilla Plan Estudios
--}}
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp


        @include('partials.page-header-estudios')
<?php

        $titPagina = get_field('titPagina');
        $txtPrimerParrafo = get_field('txtPrimerParrafo');

        $imagenEntreParrafos = get_field('imagenEntreParrafos');

        $txtSegundoParrafo = get_field('txtSegundoParrafo');

        $titEsquema = get_field('titEsquema');
        $subtitEsquema = get_field('subtitEsquema');

        $titMovimiento = get_field('titMovimiento');
        $txtMovimiento = get_field('txtMovimiento');
        $txtMovimientoUno = get_field('txtMovimientoUno');
        $txtMovimientoDos = get_field('txtMovimientoDos');
        $txtMovimientoTres = get_field('txtMovimientoTres');
        $txtMovimientoCuatro = get_field('txtMovimientoCuatro');
        $txtMovimientoCinco = get_field('txtMovimientoCinco');
        $txtMovimientoSeis = get_field('txtMovimientoSeis');
        $txtMovimientoSiete = get_field('txtMovimientoSiete');
        $txtMovimientoOcho = get_field('txtMovimientoOcho');



        $titInterpretacion = get_field('titInterpretacion');
        $txtInterpretacion = get_field('txtInterpretacion');
        $txtInterpretacionUno = get_field('txtInterpretacionUno');
        $txtInterpretacionDos = get_field('txtInterpretacionDos');
        $txtInterpretacionTres = get_field('txtInterpretacionTres');
        $txtInterpretacionCuatro = get_field('txtInterpretacionCuatro');
        $txtInterpretacionCinco = get_field('txtInterpretacionCinco');
        $txtInterpretacionSeis = get_field('txtInterpretacionSeis');
        $txtInterpretacionSiete = get_field('txtInterpretacionSiete');
        $txtInterpretacionOcho = get_field('txtInterpretacionOcho');
        $txtInterpretacionNueve = get_field('txtInterpretacionNueve');


        $titMusica = get_field('titMusica');
        $txtMusica = get_field('txtMusica');
        $txtMusicaUno = get_field('txtMusicaUno');
        $txtMusicaDos = get_field('txtMusicaDos');
        $txtMusicaTres = get_field('txtMusicaTres');
        $txtMusicaCuatro = get_field('txtMusicaCuatro');
        $txtMusicaCinco = get_field('txtMusicaCinco');
        $txtMusicaSeis = get_field('txtMusicaSeis');
        $txtMusicaSiete = get_field('txtMusicaSiete');
        $txtMusicaOcho = get_field('txtMusicaOcho');
        $txtMusicaNueve = get_field('txtMusicaNueve');


        $titBasica = get_field('titBasica');
        $txtBasica = get_field('txtBasica');
        $txtBasicaUno = get_field('txtBasicaUno');
        $txtBasicaDos = get_field('txtBasicaDos');
        $txtBasicaTres = get_field('txtBasicaTres');
        $txtBasicaCuatro = get_field('txtBasicaCuatro');
        $txtBasicaCinco = get_field('txtBasicaCinco');
        $txtBasicaSeis = get_field('txtBasicaSeis');

        $otrasFormacionesUno = get_field('otrasFormacionesUno');
        $otrasFormacionesDos = get_field('otrasFormacionesDos');

?>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10 col-md-8">

        @include('partials.content-page')
      </div>
    </div>

  </div>
  <div class="container mb-5">
    <div class="row justify-content-center">
      <div class="col-10">
        <h3 class="tituloSeccionEsquema"><?php echo $titPagina; ?></h3>
        <p class="mt-3"><?php echo $txtPrimerParrafo ; ?></p>
        <img src="<?php echo $imagenEntreParrafos["url"]; ?>" width="100%" class="my-3">
        <p><?php echo $txtSegundoParrafo; ?></p>
      </div>
    </div>
  </div>
    <div class="container">
      <div class="row">
        <div class="col-12 p-1">
          <div class="capaSupTitulo">
            <h2 class="tituloPlanEstudios"><?php echo $titEsquema; ?></h2>
            <div class="subTituloPlanEstudios"><?php echo $subtitEsquema; ?></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-6 col-lg-3 p-1">
          <div class="capaAzulOscura">
            <div class="encabezado"><?php echo $titMovimiento;  ?></div>
            <p> <?php echo $txtMovimiento;  ?></p>
          </div>
          <div class="capaAzulClara"><?php echo $txtMovimientoUno; ?></div>
          <div class="capaAzulClara"><?php echo $txtMovimientoDos; ?></div>
          <div class="capaAzulClara"><?php echo $txtMovimientoTres; ?></div>
          <div class="capaNaranja"><?php echo $txtMovimientoCuatro; ?></div>
          <div class="capaNaranja"><?php echo $txtMovimientoCinco; ?></div>
          <div class="capaNaranja">
            <?php echo $txtMovimientoSeis; ?>
            <ul class="listadoHorizontal"><li><?php echo $txtMovimientoSiete; ?></li><li> <?php echo $txtMovimientoOcho; ?></li></ul>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3 p-1">
          <div class="capaAzulOscura">
            <div class="encabezado"> <?php echo $titInterpretacion; ?></div>
            <p><?php echo $txtInterpretacion; ?></p>
          </div>
            <div class="capaAzulClara"><?php echo $txtInterpretacionUno; ?>
              <ul class="listadoHorizontal">
                <li><?php echo $txtInterpretacionDos; ?></li>
                <li><?php echo $txtInterpretacionTres; ?></li>
              </ul>
            </div>
            <div class="capaAzulClara" style="padding: 3% 0 0 0; margin-bottom: 0"><?php echo $txtInterpretacionCuatro; ?>
              <ul class="listadoHorizontal" style="margin-bottom: 1%">
                <li><?php echo $txtInterpretacionCinco; ?> </li></ul>
          <div class="capaNaranja" style="padding: 0 3% 1% 0;">
              <ul class="listadoHorizontal" style="margin-bottom: 0">
                <li><?php echo $txtInterpretacionSeis; ?></li></ul></div></div>
            <div class="capaAzulClara"><?php echo $txtInterpretacionSiete; ?></div>
            <div class="capaAzulClara"><?php echo $txtInterpretacionOcho; ?></div>
            <div class="capaAzulClara"><?php echo $txtInterpretacionNueve; ?></div>
        </div>
        <div class="col-12 col-md-6 col-lg-3 p-1">
          <div class="capaAzulOscura">
            <div class="encabezado"><?php echo $titMusica; ?></div>
              <p><?php echo $txtMusica; ?>
              </p>
          </div>
          <div class="capaAzulClara" style="margin-bottom: 0; padding-bottom: 0;"><?php echo $txtMusicaUno; ?>
          <ul class="listadoHorizontal" style="margin-bottom: 0">
            <li><?php echo $txtMusicaDos; ?></li>
            <li><?php echo $txtMusicaTres; ?></li>
          </ul>
          </div>
          <div class="capaNaranja" style="padding-top: 0;">
            <ul class="listadoHorizontal">
              <li><?php echo $txtMusicaCuatro; ?>
                <div class="capaAzulClara">
                  <ul class="listadoHorizontal" style="padding-inline-start: 10px;">
                    <li><?php echo $txtMusicaCinco; ?></li>
                  </ul>
                </div>
              </li>
              <li><?php echo $txtMusicaSeis; ?></li>
            </ul></div>
          <div class="capaAzulClara" style="margin-bottom: 0"><?php echo $txtMusicaSiete; ?></div>
          <div class="capaNaranja" style="margin-bottom: 0; padding-bottom: 0; padding-top:0">
            <ul class="listadoHorizontal" style="margin-bottom: 0">
              <li><?php echo $txtMusicaOcho; ?></li>
            </ul>
          </div>
          <div class="capaAzulClara" style="padding-top: 0">
            <ul class="listadoHorizontal">
              <li><?php echo $txtMusicaNueve; ?></li>
            </ul>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3 p-1">
          <div class="capaAzulOscura">
            <div class="encabezado"><?php echo $titBasica; ?></div>
            <p><?php echo $txtBasica; ?></p>
          </div>
          <div class="capaAzulClara"><?php echo $txtBasicaUno; ?></div>
          <div class="capaAzulClara"><?php echo $txtBasicaDos; ?></div>
          <div class="capaAzulClara"><?php echo $txtBasicaTres; ?></div>
          <div class="capaAzulClara"><?php echo $txtBasicaCuatro; ?></div>
          <div class="capaAzulClara"><?php echo $txtBasicaCinco; ?></div>
          <div class="capaAzulClara"><?php echo $txtBasicaSeis; ?></div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 p-1">
          <div class="capAmarilla"><?php echo $otrasFormacionesUno; ?></div>
          <div class="capAmarilla"><?php echo $otrasFormacionesDos; ?></div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="leyendaColores">
            <div class="asignaturasOficiales"></div> Asignaturas oficiales
            <div class="asignaturasPropias"></div> Asignaturas propias
          </div>
        </div>
      </div>
    </div>
  @endwhile
@endsection

