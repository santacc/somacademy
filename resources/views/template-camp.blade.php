{{--
  Template Name: Tema summer camp
--}}
<?php
$imagenHeader = get_the_post_thumbnail_url();
$textoFranja = get_field('txtEncabezado');
?>
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp

  @include('partials.page-header-sencillas')
  <section id="bandaMusicales" style="background-image: url(/wp-content/uploads/2020/08/fondo_bandamusicales.jpg); background-size: cover;" >
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="txtDestacadoHome" style="text-align: center">

            La primera escuela de musicales en España, <br /> creada por SOM Produce, productora de:
          </h2>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-4 col-md-4 col-lg-2 align-self-center p-4">
          <img src="/wp-content/uploads/2020/08/logo_Billy.png" width="100%">
        </div>
        <div class="col-4 col-md-4 col-lg-2 align-self-center p-4">
          <img src="/wp-content/uploads/2020/08/logo_MAtilda.png" width="100%">
        </div>
        <div class="col-4 col-md-4 col-lg-2 align-self-center p-4">
          <img src="/wp-content/uploads/2020/08/logo_Grease.png" width="100%">
        </div>
        <div class="col-4 col-md-4 col-lg-2 align-self-center p-4">
          <img src="/wp-content/uploads/2020/08/logo_Cabaret.png" width="100%">
        </div>
        <div class="col-4 col-md-4 col-lg-2 align-self-center p-4">
          <img src="/wp-content/uploads/2020/08/logo_WSS.png" width="100%">
        </div>
        <!-- <div class="col-4 col-md-4 col-lg-1 align-self-center p-4">
          <img src="/wp-content/uploads/2020/09/SonrisasHorizontal.png" width="100%">
        </div>
        <div class="col-4 col-md-4 col-lg-1 align-self-center p-4">
          <img src="/wp-content/uploads/2020/09/Logo-priscilla.png" width="100%">
        </div> -->
      </div>
    </div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-6 align-self-center">
          <img src="/wp-content/uploads/2021/03/LogoSummerpracticas.png" width="100%">
        </div>
      </div>
    </div>
  </section>
  <section class="textoDestacado">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-8 align-self-center">
         <?php echo $textoFranja; ?>
        </div>
      </div>
    </div>
  </section>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-11 col-md-10">

        @include('partials.content-page')
      </div>
    </div>
  </div>
  @endwhile
@endsection
