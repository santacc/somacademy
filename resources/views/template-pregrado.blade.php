{{--
  Template Name: Tema pregrado
--}}
@include('partials.pregrado.banner_superior')

@extends('layouts.app')
@section('content')
  
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
  
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
  <?php
  $activarEquipoDocente = get_field('activarEquipoDocente');
  $activarHeader = get_field('activarHeader');
  $activarPrograma = get_field('activarPrograma');
  $activarHorarios = get_field('activarHorarios');
  $activarInfoPractica = get_field('activarInfoPractica');
  $activarFormulario = get_field('activarFormulario');
  $activarDossier = get_field('activarDossier');
  $activarEquipoDocente = get_field('activarEquipoDocente');

?>



    <?php if($activarHeader == 1) { ?>
      @include('partials.pregrado.header_pregrado')
    <?php } ?>
    <?php if($activarPrograma == 1) { ?>
      @include('partials.pregrado.programa_pregrado')
    <?php } ?>
    <?php if($activarHorarios == 1) { ?>
      @include('partials.pregrado.horarios_pregrado')
    <?php } ?>
    <?php if($activarInfoPractica == 1) { ?>
      @include('partials.pregrado.infopractica_pregrado')
    <?php } ?>
    <?php if($activarFormulario == 1) { ?>
      @include('partials.pregrado.formulario_pregrado')
    <?php } ?>
    <?php if($activarDossier == 1) { ?>
      @include('partials.pregrado.dossier_pregrado')
    <?php } ?>
    <?php if($activarEquipoDocente == 1) { ?>
      @include('partials.pregrado.profesores_pregrado')
    <?php } ?>


@endsection

