{{--
  Template Name: Plantilla Difusion Practicas
--}}

<?php

$urlAlumno = get_field('urlAlumno','options');
$urlEmpresa = get_field('urlFormularioEmpresa','options');
$titSeccionAlumno = get_field('titSeccionAlumno','options');

$titSeccionEmpresas = get_field('titSeccionEmpresas','options');

/* Seccion empresa */
$proyectoFormativo = get_field('proyectoFormativo','options');
$txtProyectoFormativo = get_field('txtProyectoFormativo','options');


/* Seccion seguros y seguridad */
$tituloSeguro = get_field('tituloSeguro','options');
$urlInfoPracticas = get_field('urlInfoPracticas','options');
$textoInfoPracticas = get_field('textoInfoPracticas','options');
$tituloBases = get_field('tituloBases','options');
$tituloPracticas = get_field('tituloPracticas','options');
$urlPracticas = get_field('urlPracticas','options');
$textoRealDecreto = get_field('textoRealDecreto','options');
$urlRealDecreto = get_field('urlRealDecreto','options');

/* Documentos */
$titSeccionImpreso = get_field('titSeccionImpreso','options');

/* Cuestionario de satisfaccion */

$titFormularioSatisfaccion = get_field('titFormularioSatisfaccion','option');



if( have_rows('listadoImpresos','options') ) {
  while( have_rows('listadoImpresos','options') ) { 
    the_row();       
    $archivoImpreso = get_sub_field('archivoImpreso');
    $textoImpreso = get_sub_field('textoImpreso');
    $contArchivos .= '<a href="'.$archivoImpreso["url"].'" target="_blank"><i class="fa-solid fa-file-pdf"></i>'.$textoImpreso.'</a>';
  }
}

$contProyectosTercero = '';
$contProyectosCuarto = '';

if( have_rows('proyectosFormativos','options') ) {
  while( have_rows('proyectosFormativos','options') ) { 
    the_row();       
    $proyectoFormativo = get_sub_field('proyectoFormativo');
    $txtProyectoFormativo = get_sub_field('txtProyectoFormativo');
    $cursoProyecto = get_sub_field('cursoProyecto');
    

    if($cursoProyecto == 'tercero') {
      $contProyectosTercero .= '<div class="item-proyecto"><a href="'.$proyectoFormativo["url"].'" target="_blank" class="enlace-item"><i class="fa-solid fa-file-pdf"></i>'. $txtProyectoFormativo.'</a></div>';
    } else {
      $contProyectosCuarto .= '<div class="item-proyecto"><a href="'.$proyectoFormativo["url"].'" target="_blank" class="enlace-item"><i class="fa-solid fa-file-pdf"></i>'. $txtProyectoFormativo.'</a></div>';
    }

  }
}


?>


@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp


        @include('partials.page-header-estudios')

        <section class="seccion-superior">
          <div class="cont-departamentos">
            <div class="acceso-departamento">
              <a href="<?php echo $urlAlumno; ?>" class="btn btn-acceso"><?php echo $titSeccionAlumno; ?></a>
            </div>
            <div class="acceso-departamento">
              <a href="<?php echo $urlEmpresa; ?>" class="btn btn-acceso"><?php echo $titSeccionEmpresas; ?></a>
              </div>
          </div>
        </section>
        <section id="proyectos-formativo">
<h3 class="tit-proyectos">PROYECTOS FORMATIVOS</h3>
          <div class="cont-proyectos">
            <?php if($contProyectosTercero != '') { ?> 
              <div class="cont-año">
                <div class="curso">Tercer Curso</div>
                <div class="item-proyectos"><?php echo $contProyectosTercero; ?></div>
              </div>
            <?php } ?>
            <?php if($contProyectosCuarto != '') { ?>
            <div class="cont-año">
              <div class="curso">Cuarto Curso</div>
              <div class="item-proyectos"><?php echo $contProyectosCuarto; ?></div>
            </div>
            <?php } ?>

          </div>

        </section>
        <section class="documentos">
          <h3 class="tit-proyectos" style="text-align: center">DOCUMENTOS</h3>
          <div class="cont-impresos">
            <div class="seccion-seguros">
              <h3 class="titulo-impresos">ENLACES DE INTERÉS SEGURIDAD SOCIAL</h3>
              
              <a href="<?php echo $urlInfoPracticas; ?>"><i class="fa-solid fa-link"></i><?php echo $textoInfoPracticas; ?></a>
              <br /><br/>
              <?php echo $tituloBases; ?> <br/><br/>
              <a href="<?php echo $urlPracticas; ?>"><i class="fa-solid fa-link"></i><?php echo $tituloPracticas; ?></a>
              <a href="<?php echo $urlRealDecreto; ?>"><i class="fa-solid fa-link"></i><?php echo $textoRealDecreto; ?> </a>
            </div>
            <div class="seccion-seguros">
              <h3 class="titulo-impresos"><?php echo $titSeccionImpreso; ?></h3>
              <?php echo $contArchivos; ?>
            </div>
          </div>
         
         
        </section>

        <section id="listado-empresas">
          <h3 class="titulo-listado-empresas">LISTADO EMPRESAS / ENTIDADES COLABORADORAS</h3>
          @php
          $args = array(
            'post_type'=>'empresa',
            'posts_per_page'=>'-1',
      
            );
          $loop = new WP_Query($args);
        @endphp
<div class="cont-empresas">
  @while ($loop->have_posts()) @php $loop->the_post() @endphp
  @include('partials.content-'.get_post_type())
  @endwhile
</div>
         
        </section>

        
  
  @endwhile
@endsection

