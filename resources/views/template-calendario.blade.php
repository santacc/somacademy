{{--
  Template Name: Calendario
--}}

@extends('layouts.app')
@section('content')
  <?php
    $args = array(
        'post_type'=>'taller',
        'posts_per_page'=>'-1'
        );
    $loop = new WP_Query($args);
    $arrayCalendario = Array();
    $i = 0;
    ?>
  @include('partials.page-header')
  @while($loop->have_posts()) @php $loop->the_post() @endphp

    <?php

      $tituloTMP = get_the_title();
      $titulo = substr($tituloTMP , 0, 32); //Esta es la cadena que se quiere acortar

      $colorTaler = get_field('colorFondo');
      $txtColorTaller = get_field('colorTexto');

      $imagenTaller = get_field('imageTaller');
      $urlTaller = get_the_permalink();
      $descTaller = get_field('descripcionTaller');

      $mostrarTooltip = '<div class="contTooltip"><div class="contImgTooltip"><img src="'.$imagenTaller["url"].'" class="imgTooltip"></div><div class="titTooltip" >'. $titulo .'</div><div class="txtTooltip">'.$descTaller.' <a href="'. $urlTaller .'">ir al taller</a></div>';


      // Check rows exists.
      if( have_rows('fechasEvento') ):

          // Loop through rows.
          while( have_rows('fechasEvento') ) : the_row();

              // Load sub field value.
            $diaTaller = get_sub_field('diaTaller');
            $comTaller = get_sub_field('horaComienzo');
            $finTaller = get_sub_field('horaFin');
            $tallerDiaCompletoTMP = get_sub_field('diaCompleto');
            if($tallerDiaCompletoTMP == 0 ) {
              $tallerDiaCompleto = false;
            } else {
              $tallerDiaCompleto = true;
            }
              // Do something...
            $arrayCalendario[$i] = array(
              #Aquí está la respuesta, usa [] luego del nombre del array.
              'title' => $titulo,
              'start' => $diaTaller.'T'.$comTaller,
              'end' =>  $diaTaller.'T'.$finTaller,
              'color' => $colorTaler,
              'textColor' => $txtColorTaller,
              'allDay' => $tallerDiaCompleto,
              'url' => $urlTaller,
              'description' => $mostrarTooltip,
              /* groupId => 'blueEvents',
              daysOfWeek => [ '4', '3' ], */
            );
              $i++;
          // End loop.
          endwhile;

      // No value.
      else :
          // Do something...
      endif;




    ?>


  @endwhile
  <div class="container"><div class="row"><div class="col-12"><div id='calendar'></div></div></div></div>
@endsection

<link href='https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.1/css/all.css' rel='stylesheet'>
<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/main.css' rel='stylesheet' />
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/main.js'></script>
<script src='https://unpkg.com/popper.js/dist/umd/popper.min.js'></script>
<script src='https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.4.0/locales/es.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script>
  var post_info;
  document.addEventListener('DOMContentLoaded', function() {

    post_info = <?php echo json_encode($arrayCalendario); ?>;

    console.log(post_info);

    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
      initialView: 'dayGridMonth',
      themeSystem: 'bootstrap',
      timeZone: 'UTC',
      firstDay: 1,
      locale: 'es',
      eventLimit: true, // allow "more" link when too many events
      titleFormat: {
        year: 'numeric',
        month: 'long',
      },
      eventTimeFormat: { // like '14:30:00'
        hour: '2-digit',
        minute: '2-digit',
        meridiem: false
      },
      navLinks: true,
      selectable: true,
      selectMirror: true,
      selectAllow: true,
      headerToolbar: {
        center: 'dayGridMonth,timeGridWeek,dayGridDay,listWeek',
        end: 'today prev,next',
      }, // buttons for switching between views
        // any other sources...
      views: {
        timeGrid: {
          dayMaxEventRows: 4 // adjust to 6 only for timeGridWeek/timeGridDay
        }
      },
      eventDidMount: function(info) {
        var tooltip = new Tooltip(info.el, {
          title: info.event.extendedProps.description,
          placement: 'top',
          trigger: 'hover',
          container: 'body',
          html: true,
        });
        console.log(tooltip);
      },
    });
    calendar.render();
    calendar.addEventSource(post_info);



  });

</script>

<style>
  .tooltip {
    opacity: 1 !important;
  }
</style>


