{{--
  Template Name: Plantilla interior estudios
--}}
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp


        @include('partials.page-header-estudios')


  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10 col-md-8">

        @include('partials.content-page')
      </div>
    </div>
  </div>
  @endwhile
@endsection

