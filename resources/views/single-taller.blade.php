
@extends('layouts.app')

@section('content')
  @include('partials.page-header-talleres')
  <div class="container">
    <div class="row">
      @while(have_posts()) @php the_post() @endphp
      @include('partials.content-single-'.get_post_type())
      @endwhile
    </div>
  </div>
<?php 

$estilos = get_field('estilos');
$codigos = get_field('codigos');

if($estilos != '') {
  echo '<style>'.$estilos.'</style>';
}

if($codigos != '') {
  echo '<script>'.$codigos.'</script>';
}
?>
@endsection
