{{--
  Template Name: Plantilla Asignaturas
--}}
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp


        @include('partials.page-header-estudios')

  <?php


        $titTablaOficial = get_field('titTablaOficial');
        $titTablaPropia = get_field('titTablaPropia');
        $encabezadoAsignaturas = get_field('encabezadoAsignaturas');
        $encabezadoCreditos = get_field('encabezadoCreditos');

        //PRIMERO
        $nomCursoPrimero = get_field('nomCursoPrimero');
        $creditosPrimero = get_field('creditosPrimero');

        $contenedorPrimeroOficial = '';
        if( have_rows('asignaturasPrimeroOficial') ) {
          while( have_rows('asignaturasPrimeroOficial')) {
            the_row();
            $asignaturaPrimeroOficial = get_sub_field('asignaturaPrimeroOficial');
            $creditosPrimeroOficial = get_sub_field('creditosPrimeroOficial');
            $guiaDocente = get_sub_field('guiaDocente');
            $contenedorPrimeroOficial .= '<div class="asignaturasOficiales">
                    <div class="colDerechaOficial"><a href="'.$guiaDocente['url'].'" target="_blank" class="enlaceGuia">'.$asignaturaPrimeroOficial.'</a></div>
                    <div class="colIzquierdaOficial">'.$creditosPrimeroOficial.'</div>
                  </div>';
          }
        } else {

        }

        $contenedorPrimeroPropio = '';
        if( have_rows('asignaturasPrimeroPropio') ) {
          while( have_rows('asignaturasPrimeroPropio')) {
            the_row();
            $asignaturaPrimeroPropio = get_sub_field('asignaturaPrimeroPropio');
            $creditosPrimeroPropio = get_sub_field('creditosPrimeroPropio');
            $guiaDocentePropia = get_sub_field('guiaDocentePropia');
            $contenedorPrimeroPropio .= '<div class="asignaturasPropias">
                      <div class="colDerechaPropia"><a href="'.$guiaDocentePropia['url'].'" target="_blank" class="enlaceGuiaPropia">'.$asignaturaPrimeroPropio.'</a></div>
                      <div class="coIzquierdaPropia">'.$creditosPrimeroPropio.'</div>
                    </div>';
          }
        }else {

        }

        //SEGUNDO
        $nomCursoSegundo = get_field('nomCursoSegundo');
        $creditosSegundo = get_field('creditosSegundo');
        $contenedorSegundoOficial = '';
        if( have_rows('asignaturasSegundoOficial') ) {
          while( have_rows('asignaturasSegundoOficial')) {
            the_row();
            $asignaturaSegundoOficial = get_sub_field('asignaturaSegundoOficial');
            $creditosSegundoOficial = get_sub_field('creditosSegundoOficial');
            $guiaDocenteSegundo = get_sub_field('guiaDocenteSegundo');
            $contenedorSegundoOficial .= '<div class="asignaturasOficiales">
                    <div class="colDerechaOficial"><a href="'.$guiaDocenteSegundo['url'].'" target="_blank" class="enlaceGuia">'.$asignaturaSegundoOficial.'</a></div>
                    <div class="colIzquierdaOficial">'.$creditosSegundoOficial.'</div>
                  </div>';
          }
        }else {

        }
        $contenedorSegundoPropio = '';
        if( have_rows('asignaturasSegundoPropio') ) {
          while( have_rows('asignaturasSegundoPropio')) {
            the_row();
            $asignaturaSegundoPropio = get_sub_field('asignaturaSegundoPropio');
            $creditosSegundoPropio = get_sub_field('creditosSegundoPropio');
            $guiaDocenteSegundoPropio = get_sub_field('guiaDocenteSegundoPropio');
            $contenedorSegundoPropio .= '<div class="asignaturasPropias">
                      <div class="colDerechaPropia"><a href="'.$guiaDocenteSegundoPropio['url'].'" target="_blank" class="enlaceGuiaPropia">'.$asignaturaSegundoPropio.'</a></div>
                      <div class="coIzquierdaPropia">'.$creditosSegundoPropio.'</div>
                    </div>';
          }
        }else {

        }

        //TERCERO
        $nomCursoTercero = get_field('nomCursoTercero');
        $creditosTercero = get_field('creditosTercero');
        $contenedorTerceroOficial = '';
        if( have_rows('asignaturasTerceroOficial') ) {
          while( have_rows('asignaturasTerceroOficial')) {
            the_row();
            $asignaturaTerceroOficial = get_sub_field('asignaturaTerceroOficial');
            $creditosTerceroOficial = get_sub_field('creditosTerceroOficial');
            $guiaDocenteTercero = get_sub_field('guiaDocenteTercero');
            $contenedorTerceroOficial .= '<div class="asignaturasOficiales">
                                          <div class="colDerechaOficial"><a href="'.$guiaDocenteTercero['url'].'" target="_blank" class="enlaceGuia">'.$asignaturaTerceroOficial.'</a></div>
                                          <div class="colIzquierdaOficial">'.$creditosTerceroOficial.'</div>
                                        </div>';
          }
        }else {

        }
        $contenedorTerceroPropio = '';
        if( have_rows('asignaturasTerceroPropio') ) {
          while( have_rows('asignaturasTerceroPropio')) {
            the_row();
            $asignaturaTerceroPropio = get_sub_field('asignaturaTerceroPropio');
            $creditosTerceroPropio = get_sub_field('creditosTerceroPropio');
            $guiaDocenteTerceroPropio = get_sub_field('guiaDocenteTerceroPropio');
            $contenedorTerceroPropio .= '<div class="asignaturasPropias">
                      <div class="colDerechaPropia"><a href="'.$guiaDocenteTerceroPropio['url'].'" target="_blank" class="enlaceGuiaPropia">'.$asignaturaTerceroPropio.'</a></div>
                      <div class="coIzquierdaPropia">'.$creditosTerceroPropio.'</div>
                    </div>';
          }
        }else {

        }
        //CUARTO
        $nomCursoCuarto = get_field('nomCursoCuarto');
        $creditosCuarto = get_field('creditosCuarto');
        $contenedorCuartoOficial = '';
            //REPEATER
          if( have_rows('asignaturasCuartoOficial') ) {
            while( have_rows('asignaturasCuartoOficial')) {
              the_row();
              $asignaturaCuartoOficial = get_sub_field('asignaturaCuartoOficial');
              $creditosCuartoOficial = get_sub_field('creditosCuartoOficial');
              $contenedorCuartoOficial .= '<div class="asignaturasOficiales">
                    <div class="colDerechaOficial">'.$asignaturaCuartoOficial.'</div>
                    <div class="colIzquierdaOficial">'.$creditosCuartoOficial.'</div>
                  </div>';
            }
          }else {

          }
        $contenedorCuartoPropio = '';
        if( have_rows('asignaturasCuartoPropio') ) {
          while( have_rows('asignaturasCuartoPropio')) {
            the_row();
            $asignaturaCuartoPropio = get_sub_field('asignaturaCuartoPropio');
            $creditosCuartoPropio = get_sub_field('creditosCuartoPropio');
            $contenedorCuartoPropio .= '<div class="asignaturasPropias">
                      <div class="colDerechaPropia">'.$asignaturaCuartoPropio.'</div>
                      <div class="coIzquierdaPropia">'.$creditosCuartoPropio.'</div>
                    </div>';
          }
        }else {

        }


        //REPEATER





  ?>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-10 col-md-8">

        @include('partials.content-page')
      </div>
    </div>
    <div class="accordion" id="accordionExample">
      <div class="card">
        <div class="card-header" id="headingOne">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              <?php echo $nomCursoPrimero; ?>
            </button>
          </h2>
        </div>
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">
            <div class="contenedorTabla">
              <div class="titulOficial">
                <h3 class="encabezadoOfical"><?php echo $titTablaOficial; ?></h3>
                <div class="tablaAsignaturasOficiales">
                  <div class="tituloOficiales">
                    <div class="colDerechaOficial"><?php echo $encabezadoAsignaturas; ?></div>
                    <div class="colIzquierdaOficial"><?php echo $encabezadoCreditos; ?></div>
                  </div>
                  <?php echo $contenedorPrimeroOficial; ?>
                </div>
              </div>
              <div class="tituloPropio">
                <h3 class="encabezadoPropio"><?php echo $titTablaPropia; ?></h3>
                <div class="tablaAsignaturasPropias">
                  <div class="tituloPropios">
                      <div class="colDerechaPropia"><?php echo $encabezadoAsignaturas; ?></div>
                      <div class="coIzquierdaPropia"><?php echo $encabezadoCreditos; ?></div>
                    </div>
                  <?php echo $contenedorPrimeroPropio; ?>

                </div>
              </div>
            </div>
            <div class="barraTotal"> <?php echo $creditosPrimero ; ?></div>
            <small>* Pincha en cada asignatura para ver su guia docente</small>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingTwo">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              <?php echo $nomCursoSegundo; ?>
            </button>
          </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
          <div class="card-body">
            <div class="contenedorTabla">
              <div class="titulOficial">
                <h3 class="encabezadoOfical"><?php echo $titTablaOficial; ?></h3>
                <div class="tablaAsignaturasOficiales">
                    <div class="tituloOficiales">
                      <div class="colDerechaOficial"><?php echo $encabezadoAsignaturas; ?></div>
                      <div class="colIzquierdaOficial"><?php echo $encabezadoCreditos; ?></div>
                    </div>
                  <?php echo $contenedorSegundoOficial; ?>
                </div>
              </div>
              <div class="tituloPropio">
                <h3 class="encabezadoPropio"><?php echo $titTablaPropia; ?></h3>
                <div class="tablaAsignaturasPropias">
                  <div class="tituloPropios">
                    <div class="colDerechaPropia"><?php echo $encabezadoAsignaturas; ?></div>
                    <div class="coIzquierdaPropia"><?php echo $encabezadoCreditos; ?></div>
                  </div>
                  <?php echo $contenedorSegundoPropio; ?>
                </div>
              </div>
            </div>
            <div class="barraTotal"> <?php echo $creditosSegundo ; ?></div>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingThree">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              <?php echo $nomCursoTercero; ?>
            </button>
          </h2>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
          <div class="card-body">
            <div class="contenedorTabla">
              <div class="titulOficial">
                <h3 class="encabezadoOfical"><?php echo $titTablaOficial; ?></h3>
                <div class="tablaAsignaturasOficiales">
                  <div class="tituloOficiales">
                    <div class="colDerechaOficial"><?php echo $encabezadoAsignaturas; ?></div>
                    <div class="colIzquierdaOficial"><?php echo $encabezadoCreditos; ?></div>
                  </div>
                  <?php echo $contenedorTerceroOficial; ?>
                </div>
              </div>
              <div class="tituloPropio">
                <h3 class="encabezadoPropio"><?php echo $titTablaPropia; ?></h3>
                <div class="tablaAsignaturasPropias">
                  <div class="tituloPropios">
                    <div class="colDerechaPropia"><?php echo $encabezadoAsignaturas; ?></div>
                    <div class="coIzquierdaPropia"><?php echo $encabezadoCreditos; ?></div>
                  </div>
                  <?php echo $contenedorTerceroPropio; ?>
                </div>
              </div>
            </div>
            <div class="barraTotal"> <?php echo $creditosTercero; ?></div>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingFour">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
              <?php echo $nomCursoCuarto; ?>
            </button>
          </h2>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
          <div class="card-body">
            <div class="contenedorTabla">
              <div class="titulOficial">
                <h3 class="encabezadoOfical"><?php echo $titTablaOficial; ?></h3>
                <div class="tablaAsignaturasOficiales">
                  <div class="tituloOficiales">
                    <div class="colDerechaOficial"><?php echo $encabezadoAsignaturas; ?></div>
                    <div class="colIzquierdaOficial"><?php echo $encabezadoCreditos; ?></div>
                  </div>
                  <?php echo $contenedorCuartoOficial; ?>
                </div>
              </div>
              <div class="tituloPropio">
                <h3 class="encabezadoPropio"><?php echo $titTablaPropia; ?></h3>
                <div class="tablaAsignaturasPropias">
                  <div class="tituloPropios">
                    <div class="colDerechaPropia"><?php echo $encabezadoAsignaturas; ?></div>
                    <div class="coIzquierdaPropia"><?php echo $encabezadoCreditos; ?></div>
                  </div>
                  <?php echo $contenedorCuartoPropio; ?>
                </div>
              </div>
            </div>
            <div class="barraTotal"> <?php echo $creditosCuarto ; ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endwhile
@endsection

