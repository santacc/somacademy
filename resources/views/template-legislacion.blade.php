{{--
  Template Name: Plantilla Legislación
--}}
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp


        @include('partials.page-header-estudios')

<?php
  $contBotones = '';
  if( have_rows('botonesLegislacion') ) {
    while( have_rows('botonesLegislacion')) {
      the_row();
      $urlBotones = get_sub_field('urlBotones');
      $textoBortones = get_sub_field('textoBortones');
      $archivoSubido = get_sub_field('archivoSubido');
      if($urlBotones != '') {
        $contBotones .= '<div class="col-6 col-lg-3 mb-3"><a href="'.$urlBotones.'" target="_blank" class="btn btn-outline-naranjaEstudios" style="display:block">'. $textoBortones .'</a></div>';
      } else {
        $contBotones .= '<div class="col-6 col-lg-3 mb-3"><a href="'.$archivoSubido["url"].'" target="_blank" class="btn btn-outline-naranjaEstudios" style="display:block">'. $textoBortones .'</a></div>';
      }
      
    }
  }else {

  }
?>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md-12">

        @include('partials.content-page')
      </div>
    </div>
    <div class="row">
      <?php echo $contBotones; ?>
    </div>
  </div>

  @endwhile
@endsection

