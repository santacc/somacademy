@extends('layouts.app')

@section('content')
  @include('partials.page-header-profesores')
  <div class="container">
    <div class="row">
      @while(have_posts()) @php the_post() @endphp
      @include('partials.content-single-'.get_post_type())
      @endwhile
    </div>
  </div>

@endsection
