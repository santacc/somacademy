<?php

$imagenTaller = get_field('imageTaller');


$botonParaMostrar = '';

// Check rows exists.
if( have_rows('botonesComprar') ):

  // Loop through rows.
  while( have_rows('botonesComprar') ) : the_row();
    // Load sub field value.
    $textoBoton = get_sub_field('txtBtnComprar');
    $linkBoton = get_sub_field('urlBtnComparar');
    // Do something...
    $botonParaMostrar .= '<a href='. $linkBoton .' target="_blank" class="btndifCompras">'. $textoBoton .'</a>';
    // End loop.
  endwhile;
// No value.
else :
  // Do something...
endif;


  global $post;
  $imagenTaller = get_field('imageTaller');
  $strDataTaller = get_field('disciplinas');


$varIDPOST = $post->ID;
$listadoTaxonomis = '';
$terms = get_the_terms( $post->ID,'disciplina', '', ' ');
foreach ( $terms as $term ) {
  $pasoAminusculas = $term->slug;
  $listadoTaxonomis  .= $pasoAminusculas;
  $listadoTaxonomis .= ' ';
}



?>

<?php
    $tipoTaller = get_field('tipoAlumnos');
    if($tipoTaller == 'profesionales') {
    ?>
      <div href="<?php the_permalink(); ?>" class="col-10 col-md-5 itemTaller <?php  echo $listadoTaxonomis; ?>" >
<?php } else { ?>
  <div href="<?php the_permalink(); ?>" class="col-10 col-md-5 itemTallerjunior" >
<?php } ?>

  <div class="row">

    <div class="col-5 imgItemTaller">
      <img src="<?php echo $imagenTaller['url']; ?>" width="100%">
    </div>
    <div class="col-7 txtItemTaller">
      <h2><?php the_title() ?></h2>
<div style="margin-bottom: 20px;">
      <?php
      $featured_posts = get_field('profesoresTaller');
      if( $featured_posts ): ?>

        <?php foreach( $featured_posts as $featured_post ):
          $title = get_the_title( $featured_post->ID );
          ?>

          <?php echo esc_html( $title ); echo '<br />'; ?>
        <?php endforeach; ?>
      <?php endif; ?><br />
      <?php the_field('disciplinas') ?> <br />

</div>
      <div style="margin-top: 20px; position: relative; bottom: 5px; ">
        <a href="<?php the_permalink(); ?>"  class="btnVermas">+ info</a>
        <?php echo $botonParaMostrar; ?>
      </div>
    </div>

  </div>
</div>
