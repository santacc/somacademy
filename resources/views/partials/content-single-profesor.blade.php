<?php
  $cargo = get_field('cargo');
  $email = get_field('email');
  $disciplinas = get_field('disciplina');
  $cursos = '';
  $estudios = get_field('estudios');
  $cartaProfesor = get_field('cartaProfesor');
  $titCartaProfesor = get_field('titSeccionCarta');

  $experiencia= get_field('experiencia');
  $imageProfesor= get_field('imageProfesor');
  $descripcion = get_field('descripcion');

    $featured_posts = get_field('cursos');
    if( $featured_posts ):
      foreach( $featured_posts as $featured_post ):
        $permalink = get_permalink( $featured_post->ID );
        $title = get_the_title( $featured_post->ID );

        $cursos .= '<a href="'. esc_url( $permalink ) .'">'. esc_html( $title ) .'</a>';
        $cursos .= '<br />';
      endforeach;
    endif;

    ?>

<div class="col-12" style="text-align: end"><div class='volver' onclick="history.back()">VOLVER</div></div>
<div class="col-12 col-md-7 py-3">

  <?php if($cargo != '') { ?>
  <div class="cargoProfesores txtContenido"><?php echo $cargo; ?></div>
  <?php } ?>

  <?php if($disciplinas != '') { ?>
      <h4>Disciplinas</h4>
      <div class="txtContenido"><?php echo $disciplinas; ?></div>
  <?php } ?>

  <?php if($estudios != '') { ?>
  <h4>Estudios / Formación</h4>
  <div class="txtContenido"> <?php echo $estudios; ?></div>
  <?php } ?>
    <?php if($cartaProfesor != '') { ?>
    <h4><?php echo $titCartaProfesor; ?></h4>
    <div class="txtContenido"> <?php echo $cartaProfesor; ?></div>
    <?php } ?>
  <?php if($experiencia != '') { ?>
  <h4>Experiencia / Trayectoria</h4>
  <div class="txtContenido"> <?php echo $experiencia; ?></div>
  <?php } ?>

  <?php if($cursos != '') { ?>
  <h4>Cursos que imparte</h4>
    <div class="txtContenido"> <?php echo $cursos; ?></div>
  <?php } ?>
</div>
<div class="col-12 col-md-5 py-3">
  <img src="{!! $imageProfesor['url'] !!}" width="100%" alt="{!! $imageProfesor['alt'] !!}">
</div>
