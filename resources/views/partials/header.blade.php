<?php
$telefono = get_field('telefonoHeader', 'option');
$emailHeader = get_field('emailHeader', 'option');
// Check rows exists.
  $listRedes = '';
  if( have_rows('redesSociales','option') ){
   $listRedes .= '<ul>';
     // Loop through rows.
     while( have_rows('redesSociales' , 'option') ) {

       the_row();

        // Load sub field value.
        $iconoRedes = get_sub_field('iconoRedes');
        $linkRedes = get_sub_field('urlRedes');

        $listRedes .= '<li class="redesTopBar">';
        $listRedes .= '<a href="'. $linkRedes .'" target="_blank"><img src=' .$iconoRedes['url']. ' alt="'.$iconoRedes['name'].'"></a>';
        $listRedes .= '</li>';

        // Do something...

    // End loop.
    }
    $listRedes .= '</ul>';
// No value.
  }else{
    // Do something...
  }
$emailHeaderFinal = '';
  if($emailHeader != '') {
    $emailHeaderFinal .= '<div class="mailHeader">'. $emailHeader .'</div>';
  } else {
    $emailHeaderFinal = '';
  }

  $telefonoFinal = '';
  if($telefono != '') {
    $telefonoFinal .= '<div class="telefonoHeader">'. $telefono .'</div>';
  } else {
    $telefonoFinal = '';
  }

  $botonAcceso = '';
  $link_boton = get_field('enlaceAcceso','option');
  $textoBoton = get_field('tstBotonAcceso', 'option');
  $botonAcceso = '<a href="'. $link_boton .'" class="btnAccesoAlumnos" target="_blank" rel=“nofollow”>'. $textoBoton .'</a>';
  $botonAccesoCodex = '<a href="https://www.codex.pro/codexpro/somacademy.do" class="btnAccesoAlumnos" target="_blank" rel=“nofollow”>Codex</a>';



if( get_field('top_bar', 'option') == 1 ) {
  ?>
    <div class="container-fluid" id="topBar">
      <div class="row align-items-center">
       <div class="col-1 col-md-4 col-lg-3 col-xl-3">{!! $telefonoFinal !!}  {!! $emailHeaderFinal !!} </div>
        <div class="col-0 col-lg-3 col-xl-6"> </div>
        <div class="col-6 col-md-2 col-lg-2 col-xl-1">{!!   $botonAccesoCodex !!}</div>
        <div class="col-6 col-md-3 col-lg-3 col-xl-2">{!! $botonAcceso !!}</div>
      </div>
    </div>
  <?php
  }
?>
<header class="banner" id="navPrincipal">
  <div class="container-fluid">
    <nav class="navbar navbar-expand-md mr-auto animated fadeInUp" role="navigation" >
      <?php
        $imagenLogo = get_field('logoHeader','option');
        $imagenRegistro = get_field('botonRegistro','option');
        $enlaceRegistro = get_field('linkRegistro','option');
      ?>
      <a class="brand mr-auto" href="{{ home_url('/') }}"><img src="{{ $imagenLogo['url'] }}" class="logoHeader" alt="Logo SOM Academy"></a>
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu($primarymenu) !!}
        @endif
        <a href="{!! $enlaceRegistro !!}" class="btnRegistro">inscríbete<!--<img src="{!! $imagenRegistro['url'] !!}" class="inscribete" alt="Boton Acceso SOM Academy"> --></a>
          <button class="hamburger hamburger-dos hamburger--spin" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
      </nav>
    <?php

    if ( is_page_template( 'views/template-homestudios.blade.php' )||is_page_template( 'views/template-interiorestudios.blade.php' ) || is_page_template( 'views/template-asignaturas.blade.php' ) || is_page_template( 'views/template-planestudios.blade.php' ) || is_page_template( 'views/template-admision.blade.php' ) || is_page_template( 'views/template-legislacion.blade.php')  ) { ?>
      <section class="navEspecial" id="menuEspecial">
        <nav class="navbar navbar-expand-md mr-auto animated fadeInUp justify-content-center" role="navigation" >
          <button class="hamburger hamburger-tres hamburger--spin navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">menú del grado<span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <?php
          $conf = [
            'menu' =>  'Menu Estudios',
            'menu_id' =>  'menu-estudios', // <ul id="navMenu">
            'menu_class' => 'nav navbar-nav1', // <ul class="navMenu">
            'container' => 'nav', // <nav></nav>
            'container_class' => 'menu', // <nav id="navMenu">
            'container_id' => '',    // <nav class="navMenu">
            'theme_location' =>  '', // este sera el nombre del menu que le tengamos asignado en functions.php usando register_nav_menu()
            'echo' => true,
            'fallback_cb' => 'wp_page_menu', // en caso de que el menu no exista cargar wp_page_menu
            'before' => '', // texto antes del texto del enlace.
            'after' => '', // texto despues del texto del enlace.
            'link_before' => '', // <a href=""><span> ....
            'link_after' => '', // </span></a>
            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          
            'item_spacing' => '', // preserve / discard
            'depth' => 3, // numero de niveles que serán mostrados
            'walker' => new \App\wp_bootstrap4_navwalker(),
          ];
          wp_nav_menu($conf);
          ?>
          </div>
        </nav>
      </section>
      @include('partials.formulario-flotante')
  <?php }  ?>
  <?php

  if ( is_page_template( 'views/template-practicas.blade.php' ) || is_page_template( 'views/template-estudiante.blade.php' ) ||is_page_template( 'views/template-empresa.blade.php' )) { ?>
    <section class="navEspecial" id="menuEspecial">
      <nav class="navbar navbar-expand-md mr-auto animated fadeInUp justify-content-center" role="navigation" >
        <button class="hamburger hamburger-tres hamburger--spin navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">menú del grado<span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
      <?php
        $conf = [
          'menu' =>  'Menu Estudios',
          'menu_id' =>  'menu-estudios', // <ul id="navMenu">
          'menu_class' => 'nav navbar-nav1', // <ul class="navMenu">
          'container' => 'nav', // <nav></nav>
          'container_class' => 'menu', // <nav id="navMenu">
          'container_id' => '',    // <nav class="navMenu">
          'theme_location' =>  '', // este sera el nombre del menu que le tengamos asignado en functions.php usando register_nav_menu()
          'echo' => true,
          'fallback_cb' => 'wp_page_menu', // en caso de que el menu no exista cargar wp_page_menu
          'before' => '', // texto antes del texto del enlace.
          'after' => '', // texto despues del texto del enlace.
          'link_before' => '', // <a href=""><span> ....
          'link_after' => '', // </span></a>
          'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        
          'item_spacing' => '', // preserve / discard
          'depth' => 3, // numero de niveles que serán mostrados
          'walker' => new \App\wp_bootstrap4_navwalker(),
        ];
        wp_nav_menu($conf);
        ?>
        </div>
      </nav>
    </section>
   
<?php }  ?>

</header>

<div class="menuMovil">
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu($primarymenu) !!}
  @endif
</div>


<style>
  .gform_required_legend {
    display: none;
  }
</style>
