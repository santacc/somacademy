<?php

  $imagenCasting = get_field('imagenCasting');
  $descCasting = get_field('descCasting');
  $descCorta = get_field('descCorta');

  /* para URL externa */
  $urlExterna = get_field('urlExterna');
  $urlCasting = get_field('urlCasting');
  $textBotonExterno = get_field('textBotonExterno');

  /* para agregar alguna descarga */
  $urlDescarga = get_field('urlDescarga');
  $archivoInscripcion = get_field('archivoInscripcion');
  $textBotonDescarga = get_field('textBotonDescarga');

  /* si tiene una ficha interior */
  $paginaInterior = get_field('paginaInterior');
  $textBotonInterior = get_field('textBotonInterior');
  $codigoFormulario = get_field('codigoFormulario');

  /* dfescargar autorizaciones */
  $urlAutorizacion = get_field('urlAutorizacion');
  $archivoAutorizacion = get_field('archivoAutorizacion');
  $textBotonAutorizacion = get_field('textBotonAutorizacion');

  $tituloCAsting = get_the_title();
?>
<div class="col-12 col-lg-5 mb-3 p-3 ">
  <div class="capaCasting fichaCasting">
    <img src="<?php echo $imagenCasting["url"]; ?>" width="100%" style="background-color: white">
    <div class="capaTextoDestacado"></div>
    <div class="capaTextoDestacadoSubir">
      <div class="titGridDestacado" ><?php echo $tituloCAsting; ?></div>
      <div class="descGridDestacado">
        <p ><?php echo $descCorta; ?></p></div>

      <div class="container capaBotones">
        <div class="row px-2 justify-content-center">
          <?php if($paginaInterior == 1) {?>
          <div class="col-6 p-1 mb-1"><a class="btn btn-primary" href="<?php the_permalink(); ?>" style="display: block; text-transform: uppercase; border-radius: 0"><?php echo $textBotonInterior; ?></a></div>
          <?php } ?>
          <?php if($urlExterna == 1) { ?>
          <div class="col-6 p-1 mb-1"><a class="btn btn-primary" href="<?php echo $urlCasting; ?>" target="_blank" style="display: block; text-transform: uppercase; border-radius: 0"><?php echo $textBotonExterno; ?></a></div>
          <?php } ?>
          <?php if($urlDescarga == 1) { ?>
          <div class="col-6 p-1 mb-1"><a class="btn btn-primary" href="<?php echo $archivoInscripcion["url"]; ?>" target="_blank" style="display: block; text-transform: uppercase; border-radius: 0"><?php echo $textBotonDescarga; ?></a></div>
          <?php } ?>
        </div>
      </div>

  </div>
  </div>
</div>

