
<?php
  if( have_rows('imgDestacadosHome','options') ) {
    $logoMusicales = '';
    while( have_rows('imgDestacadosHome','options') ) { the_row();
      $subDestacados = get_sub_field('imageDestacado');
      $tituloDest = get_sub_field('titDestacado');
      $textoDesc = get_sub_field('textoDestacado');
      $enlaceDec = get_sub_field('linkDestacado');

      $logoMusicales .= '<div class="item-destacado">';
            if($enlaceDec!= ''){
              $logoMusicales .= '<div class="infoDestacado">
                            <h3 class="titulo-destcado">'. $tituloDest .'</h3>
                            <p class="parrafo-destacado">'. $textoDesc.'</p>
                              <a href="'. $enlaceDec .'" class="btn-destacado" >Leer más..</a>
                              
                            </div>';
            }
      $logoMusicales .= '<img src="'. $subDestacados['url'] .'" width="100%" alt="'. $subDestacados['name'] .'">
                     </div>';


    }
  }

 /* if( have_rows('imgDestacadosHome','options') ) {
    $logoMusicales = '';
    while( have_rows('imgDestacadosHome','options') ) { the_row();
      $subDestacados = get_sub_field('imageDestacado');
      $tituloDest = get_sub_field('titDestacado');
      $textoDesc = get_sub_field('textoDestacado');
      $enlaceDec = get_sub_field('linkDestacado');

      $logoMusicales .= '<li class="glide__slide" style="position: relative">';
            if($enlaceDec!= ''){
              $logoMusicales .= '<div class="infoDestacado" style="position:absolute; bottom: 0; right: 0; width: 80%">
                            <h3 style="font-size: 1.1rem">'. $tituloDest .'</h3>
                            <p style="font-size: 0.9rem; line-height: 1">'. $textoDesc.'</p><a href="'. $enlaceDec .'" class="btnDestacado" style="float: right; background-color: #fff; font-size: 0.9rem; text-align: center; border: solid 2px #bc1a80; text-transform: uppercase; display: block; width: fit-content; padding: 0 6px;">Leer más..</a></div>';
            }
      $logoMusicales .= '<img src="'. $subDestacados['url'] .'" width="100%" alt="'. $subDestacados['name'] .'">
                     </li>';


    }
  } */

?>

<div class="cont-destacados"><?php echo $logoMusicales; ?></div>

