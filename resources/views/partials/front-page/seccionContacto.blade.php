<h2 class="titSeccion">
  <?php
    the_field('tituloContacto','option')
  ?>
</h2>
<div class="container">
  <?php
 $nombreForm =  get_field('formContHome','option');
    gravity_form(
      $nombreForm,
      $display_title = false,
      $display_description = false,
      $display_inactive = false,
      $field_values = null,
      $ajax = false,
      $echo = false
    );


  ?>
</div>
