<h2 class="titSeccion"><?php the_field('tituloSituacion','option') ?></h2>
<div class="container-fluid">
  <div class="row align-items-center" >
    <div class="col-12 col-md-6 " style="text-align: center">
      <?php
      $imgPlano = get_field('imageSituacion','option');
      $imgMapa = get_field('imageMapaSituacion','option');
      $textoHorario = get_field('textoHorarios','option');
      ?>

      <h3 class="textoSituacion">
        <?php the_field('textoSituacion','option') ?>
      </h3>
        <?php echo $textoHorario; ?>

        <img src="{!! $imgPlano['url'] !!}" width="100%" class="metroImg" alt="{!! $imgPlano['name'] !!}">
      <h3 class="textoAutobuses"><br />Autobuses <br />2, 26, 61, C1, C2</h3>
    </div>
    <div class="col-12 col-md-6 sinPadding">
      <img src="{!! $imgMapa['url'] !!}" width="100%" alt="{!! $imgMapa['name'] !!}">
    </div>
  </div>
</div>
