<div class="container">
  <div class="row">
    <div class="col-12">
      <h2 class="txtDestacadoHome">
        <?php
          the_field('textoSuperior','option');
        ?>

      </h2>
    </div>
  </div>
</div>
<div class="container">

    <?php
      // Check rows exists.
      if( have_rows('logosMusicales','options') ):

        $logoMusicales = '<div class="div-logos">';

          // Loop through rows.
          while( have_rows('logosMusicales','options') ) : the_row();

              // Load sub field value.
            $subImagen = get_sub_field('imgLogos');
            $tipoCol = get_sub_field('tipoCol');
              // Do something...
            $logoMusicales .= '<div class="col-tipo col-tipo-'.$tipoCol.'"><img src="'. $subImagen['url'] .'" width="100%" alt="'. $subImagen['name'] .'"></div>';
            
          // End loop.
          endwhile;

      // No value.
      else :
          // Do something...
      endif;
      $logoMusicales .= '</div>';
      echo $logoMusicales;
      ?>


</div>
<div class="container">
  <div class="row">
    <div class="col-12 text-center texto-inferior">
        <?php
        the_field('textoInferior','option');
        ?>
    </div>
  </div>
</div>
