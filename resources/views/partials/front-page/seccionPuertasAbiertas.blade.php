<h2 class="titSeccion"><?php the_field('tituloSeccion','option') ?></h2>
<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="contFormulario">
        <div class="titFormulario">
          <?php the_field('introSeccion','option') ?>
        </div>
        <div class="avisoFormulario">
        <?php the_field('avisoSeccion','option') ?>
        </div>
        <br />

        <?php
        $nombreForm =  get_field('formSeccion','option');

        gravity_form($nombreForm, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $echo = true );

        ?>
      </div>
    </div>
  </div>
</div>
