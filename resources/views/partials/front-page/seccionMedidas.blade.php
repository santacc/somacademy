<?php
$fondoMedidas = get_field('fondoEscuela','option');
$logoMedidad = get_field('logoEscuelaSegura','option');
?>
<div class="container-fluid" style="background: black">
  <div class="row">

    <div class="col-12" >

      <div class="container">
      <div class="row align-items-center justify-content-center imgMedidas" style="background-image: url({!! $fondoMedidas['url'] !!});">
        <a href="/la-escuela/escuela-segura/" style="width: 100%; height: 100%; display: block; position: absolute; z-index: 999"></a>
        <div class="col-12" style="padding: 9%;">
<h3 style="font-size: 3rem; color: #fff">ESCUELA <br /> SEGURA</h3>
          <!-- <img src="{!! $logoMedidad['url'] !!}"> -->
          <h2 style="color: #fff; text-transform: uppercase"><?php the_field('textoescuela','option') ?></h2>
        </div>
      </div>
      </div>
    </div>
    <!-- <div class="col-12 col-md-6 p-5">

    </div> -->
  </div>
</div>
