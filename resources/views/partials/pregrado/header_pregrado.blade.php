<?php 
$imgHeader = get_field('imgHeader');
$contenedorDestacados = ' ';
if( have_rows('puntosDestacados') ):
    while( have_rows('puntosDestacados') ) : the_row();
        $textDestacados = get_sub_field('textDestacados');
        $contenedorDestacados .= '<div class="itemsDestacados">'.$textDestacados.'</div>';
    endwhile;
else :
endif;
$contenedorInfoVariable = ' ';
if( have_rows('infoVariable') ):
    while( have_rows('infoVariable') ) : the_row();
        $txtInfoVariable = get_sub_field('txtInfoVariable');
        $contenedorInfoVariable .= '<div class="itemsVariables">'.$txtInfoVariable.'</div>';
    endwhile;
else :
endif;
?>

<section class="headerPregrado" style="background-image: url(<?php echo $imgHeader["url"]; ?>);">
    <div class="page-header">
        <h1><?php echo the_title(); ?></h1>
    </div>
</section>
<section class="contenedorDestacados">
<?php echo $contenedorDestacados; ?>
</section>

<section class="contInfoVariable">
        <?php echo $contenedorInfoVariable; ?>
</section>