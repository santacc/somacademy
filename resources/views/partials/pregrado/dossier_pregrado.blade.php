<?php
  $mostrarDossier = '';
  $tituloDosier = get_field('tituloDossier');
  $codigoDossier = get_field('codeDossier');
  $descargarDossier = get_field('archivoDossier');

  if($tituloDosier != '') {
    $mostrarDossier .= '<a name="dossier"></a><div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <h3>'.$tituloDosier.'</h3>
                                        <hr class="wp-block-separator">
                                        '.do_shortcode( $codigoDossier ).'
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12" style="text-align: center">
                                    <a href="'. $descargarDossier['url'] .'"  class="btnDescargaDossier">Descargar el Dossier</a>
                                </div>
                                </div>
                            </div>';
    echo $mostrarDossier;
  }


  //echo do_shortcode( $codigoDossier );

  ?>