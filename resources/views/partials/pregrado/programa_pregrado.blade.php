<?php 



$contenedorIntroPrograma = '';
if( have_rows('introPrograma') ):
  while( have_rows('introPrograma') ) : the_row();
      $txtIntroPrograma = get_sub_field('txtIntroPrograma');
      $contenedorIntroPrograma .= '<div class="itemsIntroPrograma">'.$txtIntroPrograma.'</div>';
  endwhile;
else :
endif;

$contenedorEspecialidadesPrograma = '';
$i = 0;
if( have_rows('especialidadesPrograma') ):
  while( have_rows('especialidadesPrograma') ) : the_row();
      $titEspecialdiad = get_sub_field('titEspecialdiad');
      $imgEspecialdiad = get_sub_field('imgEspecialdiad');
      $guiaDocente = get_sub_field('guiaDocente');
      $descEspecialdiad = substr(get_sub_field('descEspecialdiad'),0, 250);
      if($guiaDocente != '') {
        $contenedorEspecialidadesPrograma .= '<a href="'.$guiaDocente["url"].'" class="itemsAsignaturas" style="background-image: url('.$imgEspecialdiad["url"].');"><div class="textDescripcion"><span class="titulo">'.$titEspecialdiad.'</span>'.$descEspecialdiad.'<div class="descargar-guia">guía docente</div></div></a>';
      } else {
        $contenedorEspecialidadesPrograma .= '<div class="itemsAsignaturas" style="background-image: url('.$imgEspecialdiad["url"].');"><div class="textDescripcion"><span class="titulo">'.$titEspecialdiad.'</span>'.$descEspecialdiad.'</div></div>';
      }
      
      $i++;
  endwhile;
else :
endif;
?>

<section class="seccionPrograma">
  <h2 class="tituloSecPrograma">Programa</h2>
  <div class="conteTitulos">
   <?php echo $contenedorIntroPrograma; ?>
  </div>
<div class="contAsignaturas">
<?php echo $contenedorEspecialidadesPrograma; ?>
</div>
</section>