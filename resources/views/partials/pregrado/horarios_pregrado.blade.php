<?php

$contenidoMostrar = '';
$tituloHorarios = get_field('titHorarios');
$contenidoMostrar = '<div class="container"><div class="row"><div class="col-12"><h3>'. $tituloHorarios .'</h3></div></div></div>';

if( have_rows('horario') ):

while( have_rows('horario') ) : the_row();
$contenidoMostrar .=  '<div class="container horarioClase"><div class="row"><div class="col-12"><div class="tablaTitulos">';
$nomHorario = get_sub_field('nombreHorario');
$cicHorario = get_sub_field('cicloHorario');
$turnHorario = get_sub_field('turnoHorario');
$horasHorario = get_sub_field('horasHorario');
// $contenidoMostrar .= '<div class="titulosHorario nomHorario">'.$nomHorario.'</div>';
// $contenidoMostrar .= '<div class="titulosHorario cicloHorario">'.$cicHorario.'</div>';
$contenidoMostrar .= '<div class="titulosHorario turnoHorario">'. $turnHorario .'  '. $horasHorario .'</div>';
$contenidoMostrar .= '</div></div></div>';
if( have_rows('diasLectivos') ):
  $contenidoMostrar .= ' <div class="row"><div class="col-12"><div class="tablaHorarios">';
  while( have_rows('diasLectivos') ) : the_row();
   $nomDia = get_sub_field('nombreDia');
    $contenidoMostrar .=  '<div class="columnaDia">';
    $contenidoMostrar .= '<div class="diaSemana">'. $nomDia .'</div>';
    if( have_rows('horasClases') ):
      while( have_rows('horasClases') ) : the_row();
        $horaClase = get_sub_field('horaClase');
        $nomClase = get_sub_field('nombreClase');
        $contenidoMostrar .= '<div class="claseHora">'.  $horaClase . ' ' .  $nomClase . '</div>';
      endwhile;
      $contenidoMostrar .= '</div>';
    else :
    endif;
  endwhile;
  $contenidoMostrar .= '</div></div></div></div>';
else :
endif;
endwhile;
$contenidoMostrar .= '</div></div></div>';
else :
endif;
echo $contenidoMostrar;
?>