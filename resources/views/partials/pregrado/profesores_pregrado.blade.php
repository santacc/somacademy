<?php
    
      $contDestacadoDoecente = '<div class="container"><div class="row">';
        if( have_rows('destacadoDocente') ):
            while( have_rows('destacadoDocente') ): the_row();
                // Get sub field values.
                $image = get_sub_field('fotoDoecenteDestacado');
              $nomDocenteDestacado = get_sub_field('nombreProfeDestacado');
              $titDocenteDestacado = get_sub_field('puestoProfeDestacado');
              $descProfeDestacado = get_sub_field('descProfeDestacado');
                //$link = get_sub_field('link');
              $contDestacadoDoecente .= '<div class="col-3">
                                            <img src="'. $image["url"]  .'" alt="'. $image["alt"]  .'" width="100%"/>
                                            </div>';
              $contDestacadoDoecente .= '<div class="col-9" style="align-content: center; align-self: center; align-items: center"><h3 style="width: fit-content">'.$nomDocenteDestacado.' <hr class="wp-block-separator"></h3><h4>'.$titDocenteDestacado.'</h4><p>'.$descProfeDestacado.'</p></div>';
         endwhile;
       endif;
      $contDestacadoDoecente .= '</div></div>';


  $contenedorGenralDocentes = '';
  if( have_rows('disciplinas') ):
    $contProfesores = '';
    $contProfesoresInterior = '';
    $contJefeEstudios = '';
    $contJefeEstudiosInterior = '';
    while( have_rows('disciplinas') ) : the_row();
      $titDisciplina = get_sub_field('titDisciplina');
      $etiquetaProfesores = get_sub_field('etiquetaProfesores');
      $profesores = get_sub_field('profesorGrado');
      $etiquetaJefeEstudios = get_sub_field('etiquetaJefeEstudios');
      $jefeEstudiosDisciplina = get_sub_field('jefeEstudiosDisciplina');
      $contenedorGenralDocentes .= '<div class="container mt-5">
                                        <div class="row">
                                        <div class="col-12">
                                        <h3>'. $titDisciplina .'</h3>
                                        <hr class="wp-block-separator"></div></div>';
      if( $jefeEstudiosDisciplina ):
        $contJefeEstudios .='<div class="row"><div class="col-12 col-md-6">  <h4 style="width: fit-content">'. $etiquetaJefeEstudios .'<hr class="wp-block-separator"></h4>
                                      </div></div><div class="row">';
                                      foreach( $jefeEstudiosDisciplina as $jefeEstudio ):
                                        $idJefe = $jefeEstudio->ID;
                                        $fotoProfesor = get_field('imageProfesor', $idJefe);
                                        setup_postdata($jefeEstudio);
                                        $contJefeEstudiosInterior.= '<div class="col-3 mb-4 fichaProf">

                                                                                                <a href="'. $jefeEstudio->guid .'" class="linkFichaProf">
                                                                                                  <img src="'. $fotoProfesor["url"] .'" alt="'. $fotoProfesor["alt"] .'" width="100%" class="imgProfe">
                                                                                                    <h3 class="titProf"> '.  $jefeEstudio->post_title.'</h3>
                                                                                                </a>
                                                                                            </div>';
                                      endforeach;
                                      $contJefeEstudios .= $contJefeEstudiosInterior. '</div>';
                                      wp_reset_postdata();
      endif;
      $contenedorGenralDocentes .= $contJefeEstudios;
      $contJefeEstudiosInterior = '';
      $contJefeEstudios= '';


        $etiquetaProfesores = get_sub_field('etiquetaProfesores');
        $profesores = get_sub_field('profesorGrado');
        if( $profesores ):
          $contProfesores .='<div class="row">
                                    <div class="col-12 col-md-6">
                                      <h4  style="width: fit-content">'. $etiquetaProfesores .'<hr class="wp-block-separator"></h4>
                                      </div></div><div class="row">';
                                     foreach( $profesores as $profesor ):
                                    $idProf = $profesor->ID;
                                    $fotoProfesor = get_field('imageProfesor', $idProf);
                                    setup_postdata($profesor);
                                    $contProfesoresInterior.= '<div class="col-3 mb-4 fichaProf">
                                                                  <a href="'. $profesor->guid .'" class="linkFichaProf">
                                                                    <img src="'. $fotoProfesor["url"] .'" alt="'. $fotoProfesor["alt"] .'" width="100%" class="imgProfe">
                                                                      <h3 class="titProf"> '.  $profesor->post_title.'</h3>
                                                                  </a>
                                                              </div>';
                                    endforeach;
                                  $contProfesores .= $contProfesoresInterior. '</div></div>';
                                  wp_reset_postdata();
                                  endif;
          $contenedorGenralDocentes .= $contProfesores;
          $contProfesoresInterior = '';
      $contProfesores = '';



    endwhile;
  else:

  endif;
  $contenedorGenralDocentes .= '</div></div>';

   echo  $contDestacadoDoecente;
    echo $contenedorGenralDocentes;
