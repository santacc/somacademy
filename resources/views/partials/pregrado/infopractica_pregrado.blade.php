<?php


$contenedorInfoPractica = ' ';
if( have_rows('infPractica') ):
    while( have_rows('infPractica') ) : the_row();
        $txtInfPractica = get_sub_field('txtInfPractica');
        $contenedorInfoPractica .= '<div class="itemsInfoPractica">'.$txtInfPractica.'</div>';
    endwhile;
else :
endif;
?>
<section class="contInfoPractica">
  <div class="intContInfoFractica">
    <h2 class="titInfoPractica">Información Práctica</h2>
    <div class="conItemsParcaticos"><?php echo $contenedorInfoPractica; ?></div>
  </div>
</section>