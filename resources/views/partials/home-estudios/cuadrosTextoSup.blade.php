<?php
$contenedorTextoSup = '';
$contenedorTextoSup .= '<div class="container"><div class="row">';
$i = 0;
if( have_rows('textoSuperiores','options') ):
  while( have_rows('textoSuperiores','options') ) : the_row();

    // Get parent value.
    $tiTextoSuperiores = get_sub_field('tiTextoSuperiores');
    $conTextoSuperiores = get_sub_field('conTextoSuperiores');
    $contenedorTextoSup .=  '<div class="col-12 col-md-4" style="min-height: 250px">
                                <div class="capaTexto capa'.$i.'">
                                    <h3>'.$tiTextoSuperiores.'</h3>
                                    <div class="separadorTitulo"></div>
                                    <p class="parrafoSuperior">'.$conTextoSuperiores.'</p>
                                </div>
                              </div>';
    $i++;
  endwhile;
endif;
$contenedorTextoSup .='</div></div>';

echo $contenedorTextoSup;
  ?>




