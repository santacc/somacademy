<?php
$i = 0;
$contenedorGrid = '';
$contenedorGridLeft = '';
$contenedorGridRight = '';
$contenedorGrid .= '<div class="container my-4 py-5"><div class="row" style="display: flex;"><div class="col-12 col-md-6">';
if( have_rows('contenidoGrid','options') ):
  while( have_rows('contenidoGrid','options') ) : the_row();

    // Get parent value.
    $imgenGrid = get_sub_field('imgenGrid');
    $tiGridImagenes = get_sub_field('tiGridImagenes');
    if($i==0) {
      $contenedorGridLeft .= '<div class="col-12 mb-3">';
      $contenedorGridLeft .= '<img src="'.$imgenGrid["url"].'" width="100%">';
      $contenedorGridLeft .= '<div class="recuadroTxt recuadro'.$i.'">
                                <h4 class="tituloGrid">'.$tiGridImagenes.'</h4>
                            </div>';
      $contenedorGridLeft .= '</div>';
    }

    if($i>0) {
      $contenedorGridRight .= '<div class="col-12 col-md-6 mb-4">';
      $contenedorGridRight .= '<img src="'.$imgenGrid["url"].'" width="100%">';
      $contenedorGridRight .= '<div class="recuadroTxt">
                                <h4 class="tituloGrid">'.$tiGridImagenes.'</h4>
                            </div>';
      $contenedorGridRight .= '</div>';
    }
    $i++;
  endwhile;

endif;
$contenedorGrid .= '<h3 style="text-align: center; font-weight: bold; text-transform: uppercase; font-size: 1.3rem">campus</h3><div class="row mt-4">';
$contenedorGrid .= $contenedorGridLeft;
$contenedorGrid .= '</div></div>';
$contenedorGrid .= '<div class="col-12 col-md-6">';
$contenedorGrid .= '<h3 style="text-align: center; font-weight: bold; text-transform: uppercase; font-size: 1.3rem">teatros y salas de ensayo</h3><div class="row mt-4">';
$contenedorGrid .= $contenedorGridRight;
$contenedorGrid .= '</div>';
$contenedorGrid .='</div></div>';

echo $contenedorGrid;
?>







