<?php



if( have_rows('contenidoRazones','option') ){

  $contRazonesSup = '';
  $contRazonesInf = '';
  $i = 0;
  while( have_rows('contenidoRazones' , 'option') ) {

    the_row();

    // Load sub field value.
    $numeroRazon = get_sub_field('numeroRazon');
    $textRazon = get_sub_field('textRazon');
    switch ($i) {
      case 0:
        $contRazonesSup .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                           <div class="separadorSupUno"></div>
                          <div class="numCapa">'.$numeroRazon.'</div>
                          <div class="textCapa">'.$textRazon.'</div>
                          <div class="separadorInfUno"></div>
                        </div>';
        break;
      case 1:
        $contRazonesSup .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                            <div class="numCapa">'.$numeroRazon.'</div>
                            <div class="textCapa">'.$textRazon.'</div>
                            </div>';
        break;
      case 2:
        $contRazonesSup .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                            <div class="numCapa">'.$numeroRazon.'</div>
                            <div class="textCapa">'.$textRazon.'</div>
                            </div>';
        break;
      case 3:
        $contRazonesSup .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                            <div class="separadorSupCuatro"></div>
                            <div class="numCapa">'.$numeroRazon.'</div>
                            <div class="textCapa">'.$textRazon.'</div>
                            <div class="separadorInfCuatro"></div>
                            </div>';
        break;
      case 4:
        $contRazonesSup .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                            <div class="numCapa">'.$numeroRazon.'</div>
                            <div class="textCapa">'.$textRazon.'</div>
                            </div>';
        break;
      case 5:
        $contRazonesInf .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                            <div class="separadorSupSeis"></div>
                            <div class="numCapa">'.$numeroRazon.'</div>
                            <div class="textCapa">'.$textRazon.'</div>
                            </div>';
        break;
      case 6:
        $contRazonesInf .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                            <div class="numCapa">'.$numeroRazon.'</div>
                            <div class="textCapa">'.$textRazon.'</div>
                            <div class="separadorInfSiete"></div>
                            </div>';
        break;
      case 7:
        $contRazonesInf .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                            <div class="numCapa">'.$numeroRazon.'</div>
                            <div class="textCapa">'.$textRazon.'</div>
                            </div>';
        break;
      case 8:
        $contRazonesInf .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                            <div class="numCapa">'.$numeroRazon.'</div>
                            <div class="textCapa">'.$textRazon.'</div>
                            <div class="separadorInfNueve"></div>
                            </div>';
        break;
      case 9:
        $contRazonesInf .= '<div class="col-6 col-md-5 capaNumeros capa'.$i.'">
                            <div class="numCapa">'.$numeroRazon.'</div>
                            <div class="textCapa">'.$textRazon.'</div>
                            <div class="separadorInfDiez"></div>
                            </div>';
        break;
    }

    $i++;
  }

  } else{
      // Do something...
    }
?>
<section id="razones">
  <h3 class="titSeccionRazones">10 RAZONES PARA ESTUDIAR EN SOM ACADEMY Y HACER DE TU PASIÓN TU PROFESIÓN</h3>
  <div class="fondoOscuro">
    <div class="container px-5">
      <div class="row justify-content-center">
      <?php echo $contRazonesSup; ?>
      </div>
    </div>
  </div>

  <div class="fondoClarito">
    <div class="container px-5">
      <div class="row justify-content-center">
        <?php echo $contRazonesInf; ?>
      </div>
    </div>
  </div>
</section>




