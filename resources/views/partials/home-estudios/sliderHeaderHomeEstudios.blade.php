<?php
$sliderTimer = get_field('velocidadTranscionHomeEstudios','option');

?>
<div class="container-fluid sinPadding">
  <div class="row">
    <div class="col-12">
      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel" data-interval="<?php echo intval($sliderTimer); ?>">
        <?php
        if( have_rows('sliderHeaderHomeEstudios','option') ):
          $i = 0;
          $indicadores = '';
          $carrousel = '';
          while( have_rows('sliderHeaderHomeEstudios','option') ) : the_row();
            $imgSlide = get_sub_field('imageSlideHomeEstudios');
            $textoSlide = get_sub_field('textoSliderHomeEstudios');
            $linkSlide = get_sub_field('linkSlideHomeEstudios');
            if ($i==0) {
              $indicadores .= '<li data-target="#carouselExampleCaptions" data-slide-to="'. $i .'" class="active"></li>';
              $carrousel .= '<div class="carousel-item active"><a href="'. $linkSlide .'"><img src="'. $imgSlide['url'] .'" class="d-block w-100" alt="'. $imgSlide['name'] .'"></a><a href="'. $linkSlide .'" class="carousel-caption d-md-block">';
              if($textoSlide != ''){
                $carrousel .= '<h3>'. $textoSlide .'</h3></a>';
              }
              $carrousel .= '</a></div>';
            } else {
              $indicadores .= '<li data-target="#carouselExampleCaptions" data-slide-to="'. $i .'"></li>';
              $carrousel .= '<div class="carousel-item"><a href="'. $linkSlide .'"><img src="'. $imgSlide['url'] .'" class="d-block w-100" alt="'. $imgSlide['name'] .'"></a><a href="'. $linkSlide .'" class="carousel-caption d-md-block">';
              if($textoSlide != ''){
                $carrousel .= '<h3>'. $textoSlide .'</h3></a>';
              }
              $carrousel .= '</a></div>';
            }
            $i++;
          endwhile;
        // No value.
        else :
          // Do something...
        endif;
        ?>
        <ol class="carousel-indicators">
          <?php echo $indicadores;?>
        </ol>
        <div class="carousel-inner">
          <?php echo $carrousel; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="iconos-especificaciones">
  <div class="icono-duracion"><div class="label-icono">Duración</div> 4 años</div>
  <div class="icono-duracion"><div class="label-icono">Idioma</div> Castellano</div>
  <div class="icono-duracion"><div class="label-icono">Modalidad</div> Presencial</div>
   <div class="icono-duracion"><div class="label-icono">Inicio</div> Septiembre 2025</div>
</div>