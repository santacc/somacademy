<?php
$profesorContenedor = '';
$profesorListado = get_field('profesorListado','options');

if( $profesorListado ):
  foreach( $profesorListado as $profesor ):

  // Setup this post for WP functions (variable must be named $post).
    $permalink = get_permalink( $profesor->ID );
    $title = get_the_title( $profesor->ID );
    $campoCargo = get_field( 'cargo', $profesor->ID );
    $imageProfesor = get_field( 'imageProfesor', $profesor->ID );
    setup_postdata($profesor);

    $profesorContenedor .= '<a class="col-11 col-md-5 mb-3 linkProfesores" href="'. $permalink .'">';
    $profesorContenedor .= '<img src="'.$imageProfesor["url"].'" width="100%">';
    $profesorContenedor .= '<div class="capaInfo"><div class="nombreProfesor">'. $title .'</div><div class="separadorLine"></div>';
    $profesorContenedor .= '<div class="puestoProfesor">'. $campoCargo.'</div></div>';
    $profesorContenedor .= '</a>';


  endforeach;
  wp_reset_postdata();

  endif;


?>





  <div class="container">
    <div class="row justify-content-center">
      <?php echo $profesorContenedor;?>
    </div>

  </div>




