<?php



?>



<?php
$alumnosContenedor = '';
$alumnosListado = get_field('alumnosListado','options');

if( $alumnosListado ):
  foreach( $alumnosListado as $alumno ):

    // Setup this post for WP functions (variable must be named $post).
    $imagenAlumno = get_the_post_thumbnail_url($alumno->ID );
    $title = get_the_title( $alumno->ID );
    $comentarioAlumno = get_field( 'comentarioAlumno', $alumno->ID );
    setup_postdata($alumno);

    $alumnosContenedor .= '<li style="position: relative">';
    $alumnosContenedor .= '<div class="divImgAlumno"><div class="capaImagen"><img src="'.$imagenAlumno.'" width="100%" class="imgAlumno"></div>';
    $alumnosContenedor .= '<div class="capaTexto" ><div class="nomAlumno">'.$title.' </div>'. $comentarioAlumno .'</div></div>';
    $alumnosContenedor .= '</li>';


  endforeach;
  wp_reset_postdata();

endif;

?>

  <div class="glide">
  <div data-glide-el="track" class="glide__track">
    <ul class="glide__slides list-unstyled justified-gallery">
      <?php echo $alumnosContenedor; ?>
    </ul>
    <div class="glide__arrows" data-glide-el="controls">
      <button class="glide__arrow glide__arrow--left" data-glide-dir="<"> < </button>
      <button class="glide__arrow glide__arrow--right"  data-glide-dir=">"> > </button>
    </div>
  </div>
</div>


