<?php $imagenEmpresa = get_the_post_thumbnail_url(); 

$urlEmpresa = get_field('urlEmpresa');

?>
  <a href="<?php echo $urlEmpresa; ?>" class="cont-empresa">
      <div class="logo-empresa">
        <img src="<?php echo $imagenEmpresa; ?>" width="100%">
      </div>
      <div class="txt-empresa">
       {!! get_the_title() !!}
       
       
      </div>
    </a>
