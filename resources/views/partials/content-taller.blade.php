<?php

  global $post;
  $imagenTaller = get_field('imageTaller');
  $strDataTaller = get_field('disciplinas');


$varIDPOST = $post->ID;
$listadoTaxonomis = '';
$terms = get_the_terms( $post->ID,'disciplina', '', ' ');
foreach ( $terms as $term ) {
  $pasoAminusculas = $term->slug;
  $listadoTaxonomis  .= $pasoAminusculas;
  $listadoTaxonomis .= ' ';
}


  ?>

<a href="<?php the_permalink(); ?>" class="col-10 col-md-5 itemTaller <?php  echo $listadoTaxonomis; ?>" >
  <div class="row">

    <div class="col-12 col-sm-6 col-md-5 imgItemTaller">
      <img src="<?php echo $imagenTaller['url']; ?>" width="100%">
    </div>
    <div class="col-12 col-sm-6 col-md-7 txtItemTaller">
      <h2>{!! get_the_title() !!}</h2>

      <?php

      $tipoTaller = get_field('tipoAlumnos');

     /* $featured_posts = get_field('profesoresTaller');
      if ($featured_posts) :
        foreach ($featured_posts as $featured_post) :
          $title = get_the_title($featured_post->ID);
           echo esc_html($title);
           echo '<br />';
         endforeach;
         endif; */
         echo '<br />';
         ?>

      <?php the_field('disciplinas') ?> <br />
      <?php if($tipoTaller != 'junior') {
          the_field('fechaGridTaller');
          echo '<br />';
      }
       ?>
      <div class="btnVermas">ver más</div>
    </div>

  </div>
</a>



