
<div class="page-header" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" >
 <div class="container">
   <div class="row">
     <div class="col-12">

       <h1>{!! App::title() !!}</h1>

       <?php
       $introHeader = get_field('intro');

       if($introHeader != '') {
         echo '<h2 class="subtituloHeader">'. $introHeader .' </h2>';
       }

       ?>

     </div>
   </div>
 </div>
  <div class="capaTransparencia"></div>
</div>
