<footer class="content-info">
  <div class="container-fluid">
    <div class="row align-items-center py-2">
      <div class="col-xs-12 col-sm-6 col-lg-3 footerUno">
        @php
          dynamic_sidebar('footer-primero')
        @endphp
      </div>
      <div class="col-xs-12 col-sm-6 col-lg-3 footerDos">
        @php
          dynamic_sidebar('footer-segundo')
        @endphp
      </div>
      <div class="col-xs-12 col-sm-6 col-lg-3 footerTres">
        @php
          dynamic_sidebar('footer-tercero')
        @endphp
      </div>
      <div class="col-xs-12 col-sm-6 col-lg-3 footerCuatro">
        @php
          dynamic_sidebar('footer-cuarto')
        @endphp
      </div>
    </div>
  </div>
  <div class="container-fluid menuFooter">
    @php
      dynamic_sidebar('sidebar-footer')
    @endphp
  </div>
</footer>
