<?php $imagenAlumno = get_the_post_thumbnail_url(); ?>
  <div class="contAlumno">
      <div class="imgAlumno">
        <img src="<?php echo $imagenAlumno; ?>" width="100%">
      </div>
      <div class=" txtAlumno">
        <h2>{!! get_the_title() !!}</h2>
        <hr />
        @php the_content() @endphp

      </div>
</div>
