  <?php
    $imagenProfesor = get_field('imageProfesor');
    $disciplinas = get_field('disciplinas');
    $tipoFormacion = get_field('tipoFormacion');
    $claseFormacion = '';

    $varCont = 0;
    $varCont = count($tipoFormacion);
   

    for($i = 0; $i<$varCont; $i++) {
      $claseFormacion .= $tipoFormacion[$i].' ';
    }
  ?>
  <a href="<?php the_permalink(); ?>" class="col-6 col-md-3 p-2 itemTaller <?php echo $claseFormacion; ?>">
    <div class="imgItemTaller">
      <img src="<?php echo $imagenProfesor['url']; ?>" width="100%">
      <h3 class="titProf"> {!! get_the_title() !!}</h3>
       <?php
          if ($disciplinas != '') {
            echo $disciplinas;
            echo '<br />';
          }
        ?>
    </div>
  </a>

