<?php 

$post_id = get_the_ID();

$fecha =  get_the_term_list( $post_id, 'fecha', '', ', ' ); ;
$imagenAlumno = get_the_post_thumbnail_url(); ?>


<div class="fila-encuentro">
      <div class="img-encuentro">
        <div class="caja-fecha"><?php echo $fecha; ?></div>
        <img src="<?php echo $imagenAlumno; ?>" width="100%">
      </div>
      <div class="txt-encuentro">
        <h2>{!! get_the_title() !!}</h2>
        <hr />
        <div class="desc-encuentro"><?php echo get_field('descripcionEncuentro'); ?></div>
        @php the_content() @endphp
      </div>

</div>

