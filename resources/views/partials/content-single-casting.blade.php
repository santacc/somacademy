<?php
$imagenCastingInterior = get_field('imagenCastingInterior');
$imagenCasting = get_field('imagenCasting');
$descCasting = get_field('descCasting');
$descCorta = get_field('descCorta');

/* para URL externa */
$urlExterna = get_field('urlExterna');
$urlCasting = get_field('urlCasting');
$textBotonExterno = get_field('textBotonExterno');

/* para agregar alguna descarga */
$urlDescarga = get_field('urlDescarga');
$archivoInscripcion = get_field('archivoInscripcion');
$textBotonDescarga = get_field('textBotonDescarga');

/* si tiene una ficha interior */
$paginaInterior = get_field('paginaInterior');
$textBotonInterior = get_field('textBotonInterior');
$codigoFormulario = get_field('codigoFormulario');

/* dfescargar autorizaciones */
$urlAutorizacion = get_field('urlAutorizacion');
$archivoAutorizacion = get_field('archivoAutorizacion');
$textBotonAutorizacion = get_field('textBotonAutorizacion');

?>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-12 p-1 col-md-10 mb-3">
         <img src="<?php echo $imagenCastingInterior["url"]; ?>" width="100%" style="background-color: white">
    </div>
  </div>
      <div class="row justify-content-center">
        <div class="col-12 col-md-10 mb-3 p-1">
      <div class="textoCastingInt">
       <?php echo $descCasting; ?>
      </div>

    </div>
  </div>
  <div class="row justify-content-center">
    <?php if($urlDescarga == 1) { ?>
    <div class="col-6 col-md-5 p-1 mb-1"><a class="btn btn-light" href="<?php echo $archivoInscripcion["url"]; ?>" target="_blank" style="display: block; text-transform: uppercase; border-radius: 0"><?php echo $textBotonDescarga; ?></a></div>
    <?php } ?>

      <?php if($urlAutorizacion == 1) { ?>
      <div class="col-6 col-md-5 p-1 mb-1"><a class="btn btn-light" href="<?php echo $archivoAutorizacion["url"]; ?>" target="_blank" style="display: block; text-transform: uppercase; border-radius: 0"><?php echo $textBotonAutorizacion; ?></a></div>
      <?php } ?>
  </div>

</div>
<?php if($codigoFormulario != '' || $codigoFormulario != 'null') { ?>
  <section class="px-5 py-0 pb-5 mt-0 formularioInscripcion">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md-10">
        <?php

        gravity_form( $codigoFormulario , $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $echo = true );
        ?>



      </div>
    </div>
  </div>
</section>

<?php } ?>
