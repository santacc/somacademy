<?php
  $imgTaller = get_field('imageTaller');
  $disciplinasTaller = get_field('disciplinas');
  $nivelesTaller = get_field('nivelesTaller');
  $descripcionTaller = get_field('descripcionTaller');
  $lugarTaller = get_field('lugarTaller');
  $duracionTaller = get_field('duracion');
  $perfilTaller = get_field('perfil');
  $oyentesTaller = get_field('oyentes');
  $precioTaller = get_field('precio');
  $OtrosTaller = get_field('Otros');
  $listadoProfesores = '';
  $botonParaMostrar = '';

  // Check rows exists.
  if( have_rows('botonesComprar') ):

    // Loop through rows.
    while( have_rows('botonesComprar') ) : the_row();
      // Load sub field value.
      $textoBoton = get_sub_field('txtBtnComprar');
      $linkBoton = get_sub_field('urlBtnComparar');
      // Do something...
      $botonParaMostrar .= '<a href='. $linkBoton .' target="_blank" class="btndifCompras" rel=“nofollow”>'. $textoBoton .'</a>';
      // End loop.
    endwhile;
  // No value.
  else :
    // Do something...
  endif;



  $featured_posts = get_field('profesoresTaller');
  if( $featured_posts ):
    foreach( $featured_posts as $featured_post ):
      $permalink = get_permalink( $featured_post->ID );
      $title = get_the_title( $featured_post->ID );
      $listadoProfesores .= '<a href="'. esc_url( $permalink ) .'">'. esc_html( $title ) .'</a><br />';
    endforeach;
 endif;

?>
<div class="col-12 col-md-4 py-5">
  <img src="{!! $imgTaller['url'] !!}" width="100%" alt="{!! $imgTaller['alt'] !!}">
</div>
<div class="col-12 col-md-8 py-5">
  <?php if($descripcionTaller != '') { ?>
  <h4>Descripción</h4>
  <div class="txtContenido"> {!! $descripcionTaller !!} </div>
  <?php } ?>
  <?php if($disciplinasTaller != '') { ?>
      <h4>Disciplinas</h4>
      <div class="txtContenido"> {!! $disciplinasTaller !!} </div>
    <?php } ?>

    <?php if($nivelesTaller != '') { ?>
      <h4>Niveles</h4>
      <div class="txtContenido"> {!! $nivelesTaller !!} </div>
    <?php } ?>
    <?php if($listadoProfesores != '') { ?>
      <h4>Profesores que lo imparten</h4>
      <div class="txtContenido">{!! $listadoProfesores !!}  </div>
    <?php } ?>

    <?php if($lugarTaller != '') { ?>
  <h4>Lugar</h4>
  <div class="txtContenido"> {!! $lugarTaller !!} </div>
    <?php } ?>
    <?php if($duracionTaller != '') { ?>
  <h4>Duración / horarios</h4>
  <div class="txtContenido"> {!! $duracionTaller !!} </div>
    <?php } ?>
    <?php if($perfilTaller != '') { ?>
  <h4>Perfil profesional requerido</h4>
  <div class="txtContenido"> {!! $perfilTaller !!} </div>
    <?php } ?>

    <?php if($oyentesTaller != '') { ?>
        <h4>Permite oyentes o no</h4>
        <div class="txtContenido"> {!! $oyentesTaller !!} </div>
       <?php if($precioTaller != '') { ?>
        <h4>Precio alumno / precio oyente</h4>
        <div class="txtContenido"> {!! $precioTaller !!} </div>
  <?php }
    } else if($precioTaller != '') { ?>
    <h4>Precio alumno</h4>
    <div class="txtContenido"> {!! $precioTaller !!} </div>
      <?php } ?>

    <?php if($OtrosTaller != '') { ?>
  <h4>Otras</h4>
  <div class="txtContenido"> {!! $OtrosTaller !!}</div>
    <?php } ?>
    <?php if($botonParaMostrar != '') { ?>
  <h4>Inscríbete aquí</h4><br />
  <div class="listado-botones">
  <?php
    echo $botonParaMostrar;
  ?>
  </div>
    <?php } ?>


  <?php

    $verForm = get_field('verFormulario');
    $codForm = get_field('formularioCV');
  if($verForm == 1) {

  ?>
    <hr >
  <h4>Formulario</h4>
    <?php
    $codigoNews = '[gravityform id="'.$codForm.'" title="false" description="false" ajax="true"]';

    echo do_shortcode( $codigoNews );
    }
    ?>
</div>
