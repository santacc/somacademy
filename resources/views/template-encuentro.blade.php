{{--
  Template Name: Grid encuentro
--}}


@extends('layouts.app')

@section('content')
@include('partials.page-header-sencillas')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
  @php
    $args = array(
      'post_type'=>'encuentro',
      'posts_per_page'=>'-1',
      'orderby_taxonomy' => 'fecha',
      'order' => 'ASC',

      );
    $loop = new WP_Query($args);
  @endphp
<div class="contenedor-encuentros">

    @while ($loop->have_posts()) @php $loop->the_post() @endphp
      @include('partials.content-'.get_post_type())
    @endwhile

</div>






@endsection
