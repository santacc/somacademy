{{--
  Template Name: Grid encuentro por fechas
--}}
<?php
  $fechas_encuentros = get_terms( 'fecha' , 
  array(
 	  'orderby'    => 'name',
     'order' => 'DESC',
    ) 
  );


$itemsMenuSup = '';

?>

@extends('layouts.app')

@section('content')
@include('partials.page-header-sencillas')

<?php
    $args = array(
      'post_type'=>'encuentro',
      'posts_per_page'=>'-1',
      );
    $loop = new WP_Query($args);
    $i = 0;
    foreach ( $fechas_encuentros as $grupo_encuentro ) {
      $fechas_query = new WP_Query( array(
          'post_type' => 'encuentro',
          'posts_per_page' => -1,
          'tax_query' => array(
            'relation' => 'AND',
              array(
                  'taxonomy' => 'fecha',
                  'field' => 'slug',
                  'terms' => array( $grupo_encuentro->slug ),
                  'operator' => 'IN'
              ),
          )
      ) );

      
      
     
      if ($i == 0){
        $itemsMenuSup .= '<li class="nav-item" role="presentation">
        <button class="nav-link active" id="encuentro'.$i.'-tab" data-toggle="tab" data-target="#encuentro'.$i.'" type="button" role="tab" aria-controls="encuentro'.$i.'" aria-selected="true">'.$grupo_encuentro->name.'</button>
        </li>';
        $contTabsCompleto .= '<div class="tab-pane fade show active" id="encuentro'.$i.'" role="tabpanel" aria-labelledby="encuentro'.$i.'-tab">
                <div class="contenedor-encuentros">';
      } else {
        $itemsMenuSup .= '<li class="nav-item" role="presentation">
        <button class="nav-link" id="encuentro'.$i.'-tab" data-toggle="tab" data-target="#encuentro'.$i.'" type="button" role="tab" aria-controls="encuentro'.$i.'" aria-selected="true">'.$grupo_encuentro->name.'</button>
        </li>';
        $contTabsCompleto .= '<div class="tab-pane fade" id="encuentro'.$i.'" role="tabpanel" aria-labelledby="encuentro'.$i.'-tab">
        <div class="contenedor-encuentros">';
                }
      if ( $fechas_query ->have_posts() ) {  
      
        $contItemsTab = '';
        while ( $fechas_query ->have_posts() ) { 
          $fechas_query ->the_post();  
          $imagenAlumno = get_the_post_thumbnail_url(); 
  
                
                $contItemsTab .= '<div class="fila-encuentro">
                                    <div class="img-encuentro">
                                      <img src="'.$imagenAlumno.'" width="100%">
                                    </div>
                                    <div class="txt-encuentro">
                                      <h2>'. get_the_title() .'</h2>
                                      <hr />
                                      <div class="desc-encuentro">'. get_field('descripcionEncuentro').'</div>
                                    </div>
                                </div>';
                              }
              $fechas_query  = null;
              wp_reset_postdata();
        }
      
          $contTabsCompleto .= $contItemsTab.'</div></div>';
          $i++;
        }

        

        ?>
      
      


  
      <div class="menu-nav">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <?php echo $itemsMenuSup;?>
        </ul>
    </div>
      <div class="tab-content" id="myTabContent">
       <?php echo $contTabsCompleto; ?>
      </div>
     
@endsection
