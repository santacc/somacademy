<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
  @php do_action('get_header') @endphp
  <?php
  if ( is_page_template( 'views/template-talleres.blade.php' ) ) {
    ?>
      @include('partials.header-talleres')
  <?php
  } else { ?>
  @include('partials.header')
 <?php
  }
  ?>

    <div class="wrap container-fluid" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "SOM Academy",
    "url": "https://somescuelademusicales.com",
    "address": "C/ Doctor Castelo, 7 ",
    "sameAs": [
      "https://www.facebook.com/somescuelademusicales/",
      "https://twitter.com/SomEscuela",
      "https://www.instagram.com/somescuelademusicales/"
    ]
  }
</script>
  </body>
</html>
